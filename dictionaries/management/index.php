<?php
session_start();

if ( isset($_SESSION['idRole']) && $_SESSION['idRole'] == 8 )
{ 

    require 'config.php';
    require 'sections.php';

    header("Content-Type: text/html; charset={$_CONFIG['encoding']};");   // первый раз используется config $_CONFIG['encoding']

    require_once($_CONFIG['root_path'] . 'jqGridLoader.php');             // используется config  $_CONFIG['root_path']

    // Создается объект для загрузки Grid'a
    $jq_loader = new jqGridLoader();

    $jq_loader->set('grid_path', 'grids' . DIRECTORY_SEPARATOR);

    // Устанавливаем настройки для Postres
    $jq_loader->set('db_pg_connect', $_CONFIG['db_pg_connect']);
    $jq_loader->set('db_driver', $_CONFIG['db_driver']);   

    //$jq_loader->set('pdo_dsn', $_CONFIG['pdo_dsn']);
    //$jq_loader->set('pdo_user', $_CONFIG['pdo_user']);
    //$jq_loader->set('pdo_pass', $_CONFIG['pdo_pass']);

    $jq_loader->set('debug_output', true);

    if(isset($_SERVER['HTTP_HOST']) and $_SERVER['HTTP_HOST'] == 'jqgrid-php.net')
    {
        $jq_loader->addInitQuery("SET NAMES 'utf8'");
    }

    $jq_loader->addInitQuery("SET NAMES 'utf8'");

    $jq_loader->autorun();

    //-----------
    // Get grid
    //-----------

    $grid = isset($_REQUEST['render']) ? $_REQUEST['render'] : 'jqSimple';
    $grid = preg_replace('#[^a-zA-Z0-9_-]#', '', $grid); //safe

    if ( $grid != 'jqSimple' )
    {
        $rendered_grid = $jq_loader->render($grid);
    }

    require 'templates/_layout.php';

}
else
{
    header("Location: http://".$_SERVER['HTTP_HOST']);
}