<?php
class jqObjectsZSHN extends jqGrid
{
    protected function init()
    {
        $this->nav = array(

            #Set common nav actions
            'add' => true,
            'edit' => true,
            'del' => true,
            'view' => true,

            #Set text labels. It's better to set them in defaults
            'addtext' => 'Add',
            'edittext' => 'Edit',
            'deltext' => 'Delete',
            'viewtext' => 'View',

            #Set common excel export
            'excel' => true,
            'exceltext' => 'Excel',

            #Set editing params
            'prmEdit' => array('width' => 400,
                'bottominfo' => 'Редактирование',
                'viewPagerButtons' => true), // отобразить/скрыть кнопки вперед/назад внизу окна редактирования
        );

        $this->table = 'data.contours_zshn_suchobuzimskoe_2015';  
        

        $this->cols = array(

            'gid' => array('label' => 'ID',
                'width' => 15,
                'align' => 'center',
            ),

            /*'__gid' => array('label' => 'Field',
                'width' => 10,
                'align' => 'center',
                'editable' => true,
                'editrules' => array('required' => false),
            ),*/

            'owner' => array('label' => 'С/х предприятие',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'culture_pl' => array('label' => 'План',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'culture_fa' => array('label' => 'Факт',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'area_giv' => array('label' => 'Площадь введенная',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'area_calc' => array('label' => 'Площадь рассчитанная',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'ndvi_mean' => array('label' => 'NDVI',
                'width' => 15,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
        );
        
        #Add filter toolbar
        $this->render_filter_toolbar = true;
    }

}
?>
