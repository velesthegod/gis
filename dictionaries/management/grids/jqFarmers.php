<?php
class jqFarmers extends jqGrid
{
    protected function init()
    {
        $select_OwnershipName = $this->getOwnershipName();
        $select_MunicipalRegion = $this->getMunicipalRegion();
        
        $this->nav = array(

            #Set common nav actions
            'add' => true,
            'edit' => true,
            'del' => true,
            'view' => true,

            #Set text labels. It's better to set them in defaults
            'addtext' => 'Add',
            'edittext' => 'Edit',
            'deltext' => 'Delete',
            'viewtext' => 'View',

            #Set common excel export
            'excel' => true,
            'exceltext' => 'Excel',

            #Set editing params
            'prmEdit' => array('width' => 400,
                'bottominfo' => 'Редактирование',
                'viewPagerButtons' => true), // отобразить/скрыть кнопки вперед/назад внизу окна редактирования
        );

        $this->table = 'data.d_farmers'; //'data.' . 
        
        
        $this->query = "
                SELECT {fields}
                    --f.id_farmer,
                    --f.f_name,
                    --f.f_legal_address,
                    --f.f_contact_preson,
                    --f.f_description,
                    --f.id_municipal_region,
                    --f.id_ownership_type,
                    --os.ot_name,
                    --os.ot_description,
                    --mr.r_name

                FROM data.d_farmers f 
                    JOIN data.d_ownership_types os ON (os.id_ownership_type = f.id_ownership_type)   
                    JOIN data.d_municipal_region mr ON (mr.id_municipal_region = f.id_municipal_region)
        ";
        
        //$this->do_sort = 'id_farmer';
        //$this->do_limit = '';
        
        $this->cols = array(

            'id_farmer' => array('label' => 'ID',
                'db' => 'f.id_farmer',
                'width' => 10,
                'align' => 'center',
            ),

            'f_name' => array('label' => 'Название хозяйства',
                'db' => 'f.f_name',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => true),
            ),

            'f_legal_address' => array('label' => 'Юридический адрес',
                'db' => 'f.f_legal_address',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'f_contact_preson' => array('label' => 'Контактное лицо',
                'db' => 'f.f_contact_preson',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'id_ownership_type' => array('label' => 'Форма собственности',
                //'db' => "CONCAT(os.ot_name, ' [', os.ot_description, ']')",
                'db' => "os.ot_name",
                'width' => 35,
                'edittype' => 'select',
                'editoptions' => array( 'value' => ':--;'.$select_OwnershipName ),
                'editable' => true,
                'editrules' => array('required' => true),
            ),
            
            'ot_description' => array('label' => 'Пояснения к форме собственности',
                'db' => 'os.ot_description',
                'width' => 35,
                'editable' => false,
                'editrules' => array('required' => false),
            ),
            
            'id_municipal_region' => array('label' => 'Муниципальный район',
                'db' => 'mr.r_name',
                'width' => 35,
                'edittype' => 'select',
                'editoptions' => array( 'value' => ':--;'.$select_MunicipalRegion ),
                'editable' => true,
                'editrules' => array('required' => true),
            ),
 
        );
        
        #Add filter toolbar
        $this->render_filter_toolbar = true;
    }
    
    protected function renderNav($nav)
    {
        #Disable 'del' depending on condition
        if(mt_rand(1, 10) > 5)
        {
            $nav['del'] = false;
        }

        return $nav;
    }
    
    protected function getOwnershipName()
    {
        $result = $this->DB->query("SELECT * FROM data.d_ownership_types");
        $r_count = $this->DB->query('SELECT count(id_ownership_type) as count FROM data.d_ownership_types;');
        while($c = $this->DB->fetch($r_count))
        {
            $count = $c['count'];
        }

        $selectNameValue = '';
        $i = 0;
        while($r = $this->DB->fetch($result))
        {
            $i++;
            if ( $i == $count ) {
                $delimiter = '';
            }
            else {
                $delimiter = ';';
            }

            $selectNameValue .= $r['id_ownership_type'].':'.$r['ot_name'].$delimiter;
        }
        
        return $selectNameValue;
    }
    
    protected function getMunicipalRegion()
    {
        $result = $this->DB->query("SELECT * FROM data.d_municipal_region");
        $r_count = $this->DB->query('SELECT count(id_municipal_region) as count FROM data.d_municipal_region');
        while($c = $this->DB->fetch($r_count))
        {
            $count = $c['count'];
        }

        $selectNameValue = '';
        $i = 0;
        while($r = $this->DB->fetch($result))
        {
            $i++;
            if ( $i == $count ) {
                $delimiter = '';
            }
            else {
                $delimiter = ';';
            }

            $selectNameValue .= $r['id_municipal_region'].':'.$r['r_name'].$delimiter;
        }
        
        return $selectNameValue;
    }

}
?>
