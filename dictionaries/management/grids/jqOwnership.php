<?php
class jqOwnership extends jqGrid
{
    protected function init()
    {
        $this->nav = array(

            #Set common nav actions
            'add' => true,
            'edit' => true,
            'del' => true,
            'view' => true,

            #Set text labels. It's better to set them in defaults
            'addtext' => 'Add',
            'edittext' => 'Edit',
            'deltext' => 'Delete',
            'viewtext' => 'View',

            #Set common excel export
            'excel' => true,
            'exceltext' => 'Excel',

            #Set editing params
            'prmEdit' => array('width' => 400,
                'bottominfo' => 'Редактирование',
                'viewPagerButtons' => true), // отобразить/скрыть кнопки вперед/назад внизу окна редактирования
        );

        $this->table = 'data.d_ownership_types'; 
        

        $this->cols = array(

            'id_ownership_type' => array('label' => 'ID',
                'width' => 10,
                'align' => 'center',
            ),

            'ot_name' => array('label' => 'Название',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => true),
            ),

            'ot_description' => array('label' => 'Описание',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
        );
        
        #Add filter toolbar
        $this->render_filter_toolbar = true;
    }

}
?>
