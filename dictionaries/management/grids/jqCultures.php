<?php
class jqCultures extends jqGrid
{
    
    protected function init()
    {
        $this->nav = array(

            #Set common nav actions
            'add' => true,
            'edit' => true,
            'del' => true,
            'view' => true,

            #Set text labels. It's better to set them in defaults
            'addtext' => 'Add',
            'edittext' => 'Edit',
            'deltext' => 'Delete',
            'viewtext' => 'View',

            #Set common excel export
            'excel' => true,
            'exceltext' => 'Excel',

            #Set editing params
            'prmEdit' => array('width' => 400,
                'bottominfo' => 'Редактирование',
                'viewPagerButtons' => true), // отобразить/скрыть кнопки вперед/назад внизу окна редактирования
        );

        $this->table = 'data.d_cultures'; //'data.' . 
        

        $this->cols = array(

            'id_culture' => array('label' => 'ID',
                'width' => 10,
                'align' => 'center',
            ),

            'c_name' => array('label' => 'Название',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => true),
            ),

            'c_sort' => array('label' => 'Сорт',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'c_kind' => array('label' => 'Вид',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'plan_seva_areas' => array('label' => 'Отчет',
                'width' => 10,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'c_removed' => array('label' => 'Статус',
                'width' => 10,
                'editable' => true,
                'editrules' => array('required' => false),
            ),

        );
        
        #Add filter toolbar
        $this->render_filter_toolbar = true;
    }

    protected function renderNav($nav)
    {
        #Disable 'del' depending on condition
        if(mt_rand(1, 10) > 5)
        {
            $nav['del'] = false;
        }

        return $nav;
    }
}
?>
