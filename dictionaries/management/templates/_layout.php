<?
include("../../header.php");
?>

<? 
    if ( isset($_SESSION['idRole']) && $_SESSION['idRole'] == 8 )
    {
?>

    <!--jQuery-->
    <!--<script src="jqgrid/yandex/jquery.min.js"></script>-->

    <!--jQuery UI-->
    <script src="jqgrid/yandex/jquery-ui.min.js"></script>
    <link href="jqgrid/yandex/jquery.ui.all.min.css" rel="stylesheet" type="text/css"/>

    <script src="jqgrid/plugins/ui.multiselect.js"></script>
    <link href="jqgrid/plugins/ui.multiselect.css" rel="stylesheet" type="text/css"/>

    <!--jqGrid-->
    <link href="jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
    <script src="jqgrid/js/i18n/grid.locale-ru.js"></script>
    <script src="jqgrid/js/jquery.jqGrid.min.js"></script>

    <!--jqGrid Extension-->
    <link href="client/jqgrid-ext.css" rel="stylesheet" type="text/css"/>
    <script src="client/jqgrid-ext.js"></script>

    <!-- Other plugins -->
    <!--<script src="jqgrid/yandex/jquery.form.min.js"></script>-->

    <!-- Code highlighter -->
    <!--<script src="jqgrid/yandex/highlight.min.js"></script>
    <link href="jqgrid/yandex/vs.css" rel="stylesheet" type="text/css"/>-->

    <link rel="icon" href="management/misc/favicon.png" type="image/png">

    <script>
        /*$(document).ready(function(){
            $( "#accordion" ).accordion({
                collapsible: false
            });
        });*/
        $.extend($.jgrid.defaults,
        {
            hidegrid:false,
            hoverrows:false,

            viewrecords:true,
            scrollOffset:21,

            width:800,
            height:290
        });

        //$.jgrid.defaults.height = '400px';
        $.jgrid.nav.refreshtext = 'Refresh';
        $.jgrid.formatter.date.newformat = 'ISO8601Short';

        $.jgrid.edit.closeAfterEdit = true;
        $.jgrid.edit.closeAfterAdd = true;

        $(function () {
            $('#tabs-info').html($('#descr_rus').html());

            $('#accordion').accordion({
                'animated':false,
                'navigation':true
            });

            $('#tabs').tabs();

            hljs.tabReplace = '    ';
            hljs.initHighlightingOnLoad();
        });
    </script>

    <style>
        <?php if(!isset($_REQUEST['iframe'])) : ?> body {background: #FFFFFF; font-size: 12px; padding: 10px;}<?php endif;?>
        #descr {
            display: none;
        }

        #descr_rus {
            display: none;
        }

        #accordion UL {
            padding: 0;
            margin: 0;
            list-style-type: circle;
        }

        #accordion UL A {
            text-decoration: none;
            font-size: 11px;
        }

        #accordion UL A:hover {
            text-decoration: underline;
        }

        #accordion UL LI.active {
            list-style-type: disc;
        }

        .ui-widget {
            font-family: verdana;
            font-size: 12px;
        }

        .ui-jqgrid {
            font-family: tahoma, arial;
        }

        .ui-jqgrid TR.jqgrow TD {
            font-size: 11px;
        }

        .ui-jqgrid TR.jqgrow TD {
            padding-left: 5px;
            padding-right: 5px;
        }

        .ui-jqgrid TR.jqgrow A {
            color: blue;
        }

        .ui-jqgrid INPUT,
        .ui-jqgrid SELECT,
        .ui-jqgrid TEXTAREA,
        .ui-jqgrid BUTTON {
            font-family: tahoma, arial;
        }
    </style>


<body>

<div class="container" style="margin-top: 40px;">
    <div class="row">
        <?php if(isset($_REQUEST['iframe'])) : ?>
        <?php require 'templates' . DIRECTORY_SEPARATOR . $grid . '.php'; ?>
        <?php else : ?>
        <table>
            <tr>
                <td valign="top"><?php require 'templates' . DIRECTORY_SEPARATOR . '_accordion.php'; ?></td>
                <td valign="top" style="padding-left: 10px;">
                    <?php require 'templates' . DIRECTORY_SEPARATOR . $grid . '.php'; ?>
                </td>
            </tr>
        </table>
        <?php endif; ?>
    </div>
</div>

<? } ?>

<?
include("../../footer.php");
?>