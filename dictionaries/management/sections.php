<?php

$_SECTIONS = array(
    'general' => array(
        'name' => array('en' => 'Dictionaries', 'ru' => 'Справочники'),
        'items' => array(
            'cultures' => array('en' => 'Cultures', 'ru' => 'С/х культуры'),
            'chemcomposition' => array('en' => 'Chemcomposition', 'ru' => 'Химсостав почв'),
            'farmers' => array('en' => 'Farmers', 'ru' => 'Хозяйства'),
            'ownership' => array('en' => 'Ownership types', 'ru' => 'Формы собственности'),
            'agricultural_works' => array('en' => 'Agricultural Works', 'ru' => 'Виды сельскохозяйственных работ'),
            'phenophases' => array('en' => 'Phenological Phases', 'ru' => 'Фенологические фазы'),
            'objects_zshn' => array('en' => 'Objects ZSHN', 'ru' => 'Объекты ЗСХН'),
            //'cols' => array('en' => 'New column options', 'ru' => 'Новые опции колонок'),
            //'renderData' => array('en' => 'Passing render data', 'ru' => 'Передача render data'),
            //'csv'     => 'Alternative data source',
        ),
    ),

    'output' => array(
        'name' => array('en' => 'Reports', 'ru' => 'Отчеты и документы'),
        'items' => array(
            'report1' => array('en' => 'Report1', 'ru' => 'Справка'),
            //'outSearch' => array('en' => 'Searching', 'ru' => 'Поиск'),
            //'outSort' => array('en' => 'Sorting', 'ru' => 'Сортировка'),
            //'outExcel' => array('en' => 'Export to Excel', 'ru' => 'Экспорт в Excel'),
            //'outComplex' => array('en' => 'Complex queries', 'ru' => 'Сложные SQL-запросы'),
            //'outTree' => array('en' => 'Tree grid', 'ru' => 'Вывод дерева по уровням'),
            //'outTreeFull' => array('en' => 'Tree grid (Full)', 'ru' => 'Вывод дерева сразу целиком'),
            //'outAdvancedSearch' => array('en' => 'Advanced Search', 'ru' => 'Продвинутый поиск'),
        ),
    ),

    /*'render' => array(
        'name' => array('en' => 'Rendering', 'ru' => 'Рендеринг'),
        'items' => array(
            'render1' => array('en' => 'Set grid options', 'ru' => 'Опции таблицы'),
            'render2' => array('en' => 'Set nav options', 'ru' => 'Опции навигатора'),
            //'render_alt' => 'Alternative render',
        ),
    ),

    'oper' => array(
        'name' => array('en' => 'Operations', 'ru' => 'Операции'),
        'items' => array(
            'operBasic' => array('en' => 'Extend basic oper', 'ru' => 'Расширение операций'),
            'operCustom' => array('en' => 'Custom oper', 'ru' => 'Пользовательские операции'),
            'operUpload' => array('en' => 'Upload files', 'ru' => 'Загрузка файлов'),
        ),
    ),

    'exception' => array(
        'name' => array('en' => 'Exceptions', 'ru' => 'Обработка ошибок'),
        'items' => array(
            'exceptionOper' => array('en' => 'Oper exception', 'ru' => 'Ошибки операций'),
            'exceptionOutput' => array('en' => 'Output exceptions', 'ru' => 'Ошибки вывода данных'),
            //'exceptionRender' => array('en' => 'Render exceptions', 'ru' => 'Ошибки рендеринга'),
        ),
    ),

    'primary_key' => array(
        'name' => array('en' => 'Primary key handling', 'ru' => 'Работа с primary key'),
        'items' => array(
            'primaryId' => array('en' => 'Edit primary key', 'ru' => 'Изменение primary key'),
            'primaryMulti' => array('en' => 'Multi-column key', 'ru' => 'Ключ на несколько колонок'),
        ),
    ),

    'other' => array(
        'name' => array('en' => 'Other', 'ru' => 'Прочее'),
        'items' => array(
            'miscGroupHeader' => array('en' => 'Grouping header', 'ru' => 'Группировка заголовков'),
            'miscGroupHeaderEx' => array('en' => 'Grouping header 2', 'ru' => 'Группировка заголовков 2'),
            'miscSubgrid' => array('en' => 'Grid as subgrid', 'ru' => 'Вложенные таблицы'),
            'miscDatepickers' => array('en' => 'Datepickers', 'ru' => 'Выбор даты'),
            'miscAjaxDialog' => array('en' => 'Grid in ajax dialog', 'ru' => 'Загрузка таблиц через AJAX'),
        ),
    ),*/
);
