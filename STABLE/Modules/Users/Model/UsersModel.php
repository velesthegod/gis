<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class UsersModel extends DB
{
    public $DB_NAME = 'rcku_region';
    
    public $SCHEME = 'users';
    
    public $TABLE_USERS = 'users';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    public function GetUser($login)
    {
        $query = "SELECT * FROM " . $this->SCHEME . "." . $this->TABLE_USERS . " ";
        $query .= "WHERE login = '" . $login . "'";
        
        $query .= ";";
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
}
?>
