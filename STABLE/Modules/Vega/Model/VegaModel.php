<?php
error_reporting( E_ERROR );

require_once('HTTPQueryModel.php');

class VegaModel extends HTTPQueryModel
{
    public function __construct() {}
    
    /**
    * return array
    */
    public function GetVegaNDVI($gid, $year) 
    {
        // $year = "2016,2015,2014";
		// Это старый URL, который работал до 2018-05-17
        //$response = self::call('export/data.pl?field_id=JU_' . $__gid . '&ndvi=' . $year . '&ukey=53616c7465645f5fea9ab642fbf8101c2cc4e0e44f672536f057b92d37317bd50b90beb954ee3ed2');
        // Это новый URL, который работает с 2018-05-17
        $response = self::call('export/data.pl?field_id=' . $gid . '&ndvi=' . $year . '&ukey=53616c7465645f5fea9ab642fbf8101c2cc4e0e44f672536f057b92d37317bd50b90beb954ee3ed2');
        // Запись результатов в файл ndvi.xml, если нужно
        // $content = file_get_contents($response);
        // $bytes_count = file_put_contents("ndvi.xml", $response);
        $xml = simplexml_load_string($response);
        
        // $xml = new SimpleXMLElement($response);
        
        //if ( $response != "Wrong 'field_id' !" ) 
        if ( stripos($response, 'Wrong') === false ) {
            for ( $i=0; $i<$xml->fields->field->ndvi->count(); $i++ )
            {
                for ( $j=0; $j<$xml->fields->field->ndvi[$i]->data->count(); $j++ )
                {
                    foreach( $xml->fields->field->ndvi[$i]->data[$j]->attributes() as $k => $v )
                    {
                        if ( $k == 'date' ) {
                            $datesArray[] = $v;
                        }
                        if ( $k == 'value' ) {
                            $valuesArray[] = $v;
                        }
                    }
                }
            }
            $vegaNDVI_Array = array_combine($datesArray, $valuesArray);
            return $vegaNDVI_Array;
        }
        else {
            return false;
        }
        
        // Убираем преобразование в JSON.
        // $json = json_encode($xml);
        // echo $json;
    }
}
?>
