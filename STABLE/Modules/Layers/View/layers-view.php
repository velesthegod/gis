<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/vendor/Misc.php");
require_once("../Model/LayersModel.php");

$miscObject = new Misc();

$layerName = $miscObject->CleanFormData($_REQUEST['layer']);

// ПОЛУЧИТЬ АТРИБУТЫ ВЫБРАННОГО СЛОЯ ИЗ БД
if ( isset($layerName) && !empty($layerName) ) {
    $layersModelObject = new LayersModel();
    
    $layerName = str_replace("_vw", "", $layerName);
    $layerName = str_replace("workspace:", "", $layerName);
    
    $result = $layersModelObject->GetAttribs($layerName);
    
    $attribs = pg_fetch_array($result, null, PGSQL_ASSOC);
    
    $array = array();
    $i = 0;
    foreach ( $attribs as $key => $val ) {
        $array[$key] = pg_field_type($result, $i);
        $i++;
    }
    echo json_encode($array);
}

// ПОЛУЧИТЬ АТРИБУТЫ ВЫБРАННОГО СЛОЯ ЧЕРЕЗ REST
// EXAMPLE: GET /layers/199/attributes?token=mbs90lon2a8
if ( !empty($_POST['layers']) && !empty($_POST['token']) ) {
        $layerId = $miscObject->CleanFormData($_POST['layers']);
        $token = $miscObject->CleanFormData($_POST['token']);
        
        // URL, по которому будет выполнен REST запрос
        $url = 'http://edu.ikit.sfu-kras.ru/layers/'.$layerId.'/attributes?token='.$token;
        
        $mycurl = curl_init($url);
        // Данные, которые будут отправлены в запросе
        $encodedData = json_encode($data);
        curl_setopt($mycurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($mycurl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($mycurl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($mycurl, CURLOPT_POSTFIELDS, $encodedData);
        echo curl_exec($mycurl);
}

?>
