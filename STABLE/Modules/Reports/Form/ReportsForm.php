<?php
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Map/Form/MapForm.php");
require_once("../Model/ReportsModel.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Cultures/Model/CulturesModel.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/History/Model/HistoryModel.php");

class ReportsForms extends MapForm
{
    public $c_kind = array(
        0 => 'Озимые зерновые',
        1 => 'Яровые зерновые',
        2 => 'Зернобобовые',
        3 => 'Картофель',
        4 => 'Овощи',
        5 => 'Технические культуры',
        6 => 'Кормовые',
        7 => 'Подпокровный посев многолетних трав',
    );
    
    public $reportsModelObject;
    public $culturesModelObject;
    public $historyModelObject;
    
    public function __construct()
    {
        $this->reportsModelObject = new ReportsModel();
        $this->culturesModelObject = new CulturesModel();
        $this->historyModelObject = new HistoryModel();
    }
    
    /**
    * @description ReportAboutSevAreas() - отображает отчет "Справка о посевных площадях"
    * @param $farmer string Название хозяйства
    * @return Form
    */
    public function ReportAboutSevAreas($farmer)
    {
        $result = $this->reportsModelObject->GetCulturesReport1();
    
        ?>
        <div id="report1">
            <table style="width:100%; margin-top: 30px; border-collapse: collapse;">
            <tr>
                <td colspan="5" style="border: 0px;">&nbsp;</td>
                <td style="border: 0px;" align="left" style="width:40%;">
                    <p>
                    Министру сельского хозяйства<br>
                    и продовольственной политики<br>
                    Красноярского края<br>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="border: 0px;">&nbsp;</td>
                <td style="border:0px; border-bottom: 1px solid #000000;">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="5" style="border: 0px;">&nbsp;</td>
                <td style="border: 0px;" align="center">(И.О. Фамилия)</td>
            </tr>
            <tr>
                <td colspan="6" style="height: 160px; border: 0px;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border: 0px;" colspan="6" align="center">
                    <p>
                        Справка<br>
                        о размерах посевных площадей и объемах валового производства сельскохозяйственных культур<br>
                        за 5 лет, предшествующих 20___году,<br>
                        а также планируемых площадях сельскохозяйственных культур<br>
                        в 20___году<br>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="height: 30px; border: 0px;">&nbsp;</td>
            </tr>
            <tr>
                <td style="border: 0px; border-bottom: 1px solid #000000;" colspan="6" align="center">
                    <p style="font-style: italic;"><b><?=$farmer?>, Сухобузимский район</b></p>
                </td>
            </tr>
            <tr>
                <td style="border: 0px;" colspan="6" align="center">
                    <p>(наименование товаропроизводителя, муниципальный район)</p>
                </td>
            </tr>
            
            <tr>
                <td colspan="6" style="height: 50px; border: 0px;">&nbsp;</td>
            </tr>
            
            <tr>
                <td colspan="6" align="center" style="border: 1px solid black;">Вся посевная площадь сельскохозяйственных культур, га</td>
            </tr>
            <tr>
                <td colspan="5" align="center" style="border: 1px solid black;">Факт</td>
                <td colspan="1" align="center" style="border: 1px solid black;">Планируемая</td>
            </tr>
            <tr>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
            </tr>
            <tr>
                <td style="border: 1px solid black;">&nbsp;</td>
                <td style="border: 1px solid black;">&nbsp;</td>
                <td style="border: 1px solid black;">&nbsp;</td>
                <td style="border: 1px solid black;">&nbsp;</td>
                <td style="border: 1px solid black;">&nbsp;</td>
                <td style="border: 1px solid black;">&nbsp;</td>
            </tr>
            </table>
            
            <br>
            
            <table style="width:100%; border: 1px solid black; border-collapse: collapse;">
            <tr>
                <td rowspan="2" align="center" style="border: 1px solid black;">Сельскохозяйственные культуры</td>
                <td colspan="5" align="center" style="border: 1px solid black;">План посева сельскохозяйственных площадей, Га</td>
            </tr>
            <tr>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
                <td align="center" style="border: 1px solid black;">20___г.</td>
            </tr>
            <?
            
            $dataset['farmer']       = 'owner';
            $dataset['farmer_val']   = $farmer;
            $dataset['culture']      = 'culture_fa';
                          
            while ( $row = pg_fetch_array($result, null, PGSQL_ASSOC) ) { ?>
                <tr>
                    <td style="border: 1px solid black;"><p class="culture-name"><?=$row['c_name']?></p></td>
                    <?
                    $dataset['culture_val'] = $row['c_name']; 
                    $result_area = $this->reportsModelObject->GetSumAreaOfCulture($dataset); 
                    $area = pg_fetch_array($result_area, null, PGSQL_ASSOC);
                    ?>
                    <td style="border: 1px solid black;"></td>
                    <td style="border: 1px solid black;"></td>
                    <td style="border: 1px solid black;"></td>
                    <td style="border: 1px solid black;"></td>
                    <td align="center" style="border: 1px solid black;"><b><i><?=$area['area']?></i></b></td>
                </tr>
            <?
            }
            ?>
            </table>
            
            <br>
            
            <table style="width:100%; border-collapse: collapse; border:0px;" class="report1-footer">
            <tr>
                <td style="border:0px;" align="left" style="width:50%;">
                    <p>
                        Руководитель товаропроизводителя<br>
                        М.П.<br>
                        Главный бухгалтер товаропроизводителя<br>
                        «___» __________20___г.
                    </p>
                </td>
                <td style="border:0px;" colspan="4">&nbsp;</td>
                <td style="border:0px;" align="left" valign="top">
                    <p>
                    И.О. Фамилия<br><br>
                    И.О. Фамилия
                    </p>
                </td>
            </tr>
            <tr>
                <td style="border:0px;" colspan="6" height="70px">&nbsp;</td>
            </tr>
            <tr>
                <td style="border:0px;" align="left">
                    <p>
                        Уполномоченное лицо<br>
                        Органа местного самоуправления<br>
                        М.П.<br>
                        «___» __________20___г.
                    </p>
                </td>
                <td style="border:0px;" colspan="4">&nbsp;</td>
                <td style="border:0px;" align="left" valign="top">
                    <p>
                        <br>И.О. Фамилия
                    </p>
                </td>
            </tr>
            </table>
        </div>
        <?
    }
    
    /**
    * @description ReportPlanSevAreas() - отображает форму отчета "План посевных площадей"
    * @param $farmer string Название хозяйства
    * @return Form
    */
    public function ReportPlanSevAreas($farmer)
    {
        ?>
        <!-- В данном отчете стили для таблицы дублируются с целью корректного отображения при печати -->
        <div id="report2">
        <center>
            <p><?=$farmer?></p>
        </center>
        <table style="width:100%; height: auto; border: 1px solid #000000; border-collapse: collapse;">
        <tr>
            <th style="border: 1px solid #000000;">Вид культуры</th>
            <th style="border: 1px solid #000000;">План, Га</th>
            <th style="border: 1px solid #000000;">Результаты посевной, Га</th>
            <th style="border: 1px solid #000000;">К возврату субсидии, Га</th>
        </tr>
        <?
        // формируем массив данных для получения суммы площадей плановых культур
        $dataset['farmer']       = 'owner';
        $dataset['farmer_val']   = $farmer;
        
        foreach($this->c_kind as $key=>$val)
        {
            
        ?>
        <tr>
            <td valign="middle" style="width:40%; height: 40px; background: #DAE7EC; font:bold 12pt sans-serif; border: 1px solid #000000;" class="c-kind"><?=$val?></td>
            <td style="width:20%; border: 1px solid #000000;">&nbsp;</td>
            <td style="width:20%; border: 1px solid #000000;">&nbsp;</td>
            <td style="width:20%; border: 1px solid #000000;">&nbsp;</td>
        </tr>
        <?    
            $result = $this->reportsModelObject->GetCulturesByKind($val);
            while ( $row = pg_fetch_array($result, null, PGSQL_ASSOC) ) { ?>
        <tr>
            <td style="padding:0px 0px 0px 30px; border: 1px solid #000000; font:normal 12pt;" class="c-name"><?=$row['c_name']?></td>
            <?
            // Сначала считаем сумму площадей для плановых культур
            $dataset['culture']      = 'culture_pl';
            $dataset['culture_val']  = $row['c_name'];
            
            $result_sum_plan = $this->reportsModelObject->GetSumAreaOfCulture($dataset);
            $area_plan = pg_fetch_array($result_sum_plan, null, PGSQL_ASSOC);
            
            // Затем считаем сумму для фактических культур
            $dataset['culture'] = 'culture_fa';
            $result_sum_fact = $this->reportsModelObject->GetSumAreaOfCulture($dataset);
            $area_fact = pg_fetch_array($result_sum_fact, null, PGSQL_ASSOC);
            ?>
            <td align="right" style="font: italic 11pt sans-serif; padding-right: 15px; border: 1px solid #000000;" class="sum-area"><?=$area_plan['area']?></td>
            <td align="right" style="font: italic 11pt sans-serif; padding-right: 15px; border: 1px solid #000000;" class="sum-area"><?=$area_fact['area']?></td>
            <td align="right" style="font: italic 11pt sans-serif; padding-right: 15px; border: 1px solid #000000;" class="sum-area">
                <? if ( $area_plan['area'] > $area_fact['area'] ) {
                    $difference = $area_plan['area'] - $area_fact['area'];
                    echo $difference;
                } ?>
            </td>
        </tr>
        <?
            }
        }
        ?>
        </table>
        </div>
        <?
    }
    
    /**
    * @description SpringfieldWork() - отображает форму отчета "План весенне-полевых работ"
    * @param $idFarmer integer Идентификатор хозяйства
    * @param $farmer string Название хозяйства
    * @return Form
    */
    public function SpringfieldWork($idFarmer, $farmer)
    {
        $result = $this->historyModelObject->GetHistoryByFarmerId($idFarmer);
        ?>
        <div id="report3">
            <center>
                <p><?=$farmer?></p>
            </center>
            <table style="width:100%; height: auto; border: 1px solid #000000; border-collapse: collapse;">
            <tr>
                <th style="border: 1px solid #000000; width:7%;">SID</th>
                <th style="border: 1px solid #000000; width:10%;">№ поля</th>
                <th style="border: 1px solid #000000;">Культура</th>
                <th style="border: 1px solid #000000; width:20%;">Дата планирования</th>
            </tr>
            <?
            while ( $row = pg_fetch_array($result, null, PGSQL_ASSOC) ) { ?>
                <tr>
                    <td style="border: 1px solid #000000;" align="center"><?=$row['id_field']?></td>
                    <?
                    $r_history = $this->historyModelObject->GetHistoryById($row['id_plan_fact']);
                    $history = pg_fetch_array($r_history, null, PGSQL_ASSOC);
                    $r_cultures = $this->culturesModelObject->GetCultureInfoById($history['plan_id_culture']);
                    $cultures = pg_fetch_array($r_cultures, null, PGSQL_ASSOC);
                    ?>
                    <td style="border: 1px solid #000000;" align="center"><?=$row['field_number']?></td>
                    <td style="border: 1px solid #000000;" align="right"><p style="margin: 2px 7px 2px 7px;"><?=$cultures['c_name']?></p></td>
                    <td style="border: 1px solid #000000;" align="right"><p style="margin: 2px 7px 2px 7px;"><?=$history['plan_date']?></p></td>
                </tr>
            <?
            }
            ?>
            </table>
        </div>
        <?
    }
    
    /**
    * @description ExportTools() - отображает инструменты экспорта отчета
    * @param $divElement string - название id или class элемента DIV, содержащего отчет, например, #report2
    * @return Form
    */
    public function ExportTools($divElement)
    {
        ?>
    
        <script type="text/javascript">
        $(document).ready(function(){
            $(".print").click(function(){
                
                var divToPrint = $('<?=$divElement?>').html();
                //alert(divToPrint);
                var iframe=$('<iframe id="print_frame">'); // создаем iframe в переменную
                $('body').append(iframe); //добавляем эту переменную с iframe в наш body (в самый конец)
                
                // теперь получим объекты document и window новосозданного iFrame
                var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
                var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
                
                var reportStyle = '<link rel="stylesheet" type="text/css" href="/public/css/s_reports.css">';
                
                doc.getElementsByTagName('body')[0].innerHTML = reportStyle + divToPrint;
                
                //setTimeout("win.print()", 500);
                //delay(2000);
                win.print();
                $('iframe').remove();
                //doc.close();
                return false;
            });
            
            $(".to-excel").click(function(){
                // Сужаем таблицу для сохранения в Excel.
                $('<?=$divElement?> table').css({"width":"490px"});
                var url='data:application/vnd.ms-excel,' + encodeURIComponent($('<?=$divElement?>').html());
                // Возвращаем исходную ширину для web.
                $('<?=$divElement?> table').css({"width":"100%"});
                location.href=url;
                return false;
            });
            
        });
        </script>
        
        <div id="export-box">
            <button type="button" name="print" class="print">Печать</button>
            <button type="button" name="to-excel" class="to-excel">Excel</button>
        </div>
        
        <?
    }
}
?>
