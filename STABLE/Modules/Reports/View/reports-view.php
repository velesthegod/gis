<?
session_start();
?>
<link rel="stylesheet" type="text/css" href="/public/css/s_reports.css">
<?
require_once('../Form/ReportsForm.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/Modules/Popup/Model/PopupModel.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/Modules/Farmers/Model/FarmersModel.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Misc.php');

$reportsFormsObject = new ReportsForms();
$popupModelObject = new PopupModel();
$farmersModelObject = new FarmersModel();
$miscObject = new Misc();

$report = $_REQUEST['report'];
//$idFarmer = $_REQUEST['idf'];
$idFarmer = $_SESSION['idFarmer'];

// Если прошли авторизацию и существует пользователь, закрепленный за определенным хозяйством, то
if ( isset($idFarmer) && !empty($idFarmer) )
{
    // проверяем корректность переменной ID хозяйства
    $idFarmer = $miscObject->CleanFormData($idFarmer);
    
    // и получаем информацию о хозяйстве.
    //$data['id_farmer'] = $idFarmer;
    $resultFarmer = $farmersModelObject->GetFarmerInfoById($idFarmer);
    
    $rowFarmer = pg_fetch_array($resultFarmer, null, PGSQL_ASSOC);
    
    $farmer = trim($rowFarmer['f_name']);
}
    
?>

<div class="dialog-content">
<?
if ( $report == 'report1' )
{
    $reportsFormsObject->ReportAboutSevAreas($farmer);
    
    $reportsFormsObject->ExportTools('#report1');
}
?>

<?
if ( $report == 'report2' )
{
    $reportsFormsObject->ReportPlanSevAreas($farmer);
    
    $reportsFormsObject->ExportTools('#report2');
}
?>

<?
if ( $report == 'report3' )
{
    $reportsFormsObject->SpringfieldWork($idFarmer, $farmer);
    
    $reportsFormsObject->ExportTools('#report3');
}
?>
</div>