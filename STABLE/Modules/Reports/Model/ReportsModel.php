<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class ReportsModel extends DB
{
    public $DB_NAME = 'inf_region';
    public $SCHEME = 'data';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    public function GetCulturesReport1()
    {
        $query = "SELECT * FROM " . $this->SCHEME . "." . $this->CULTURES_TABLE . " ";
        $query .= "WHERE plan_seva_areas = 1 ";
        $query .= "ORDER BY id_culture ASC ";
        
        $query .= ";";
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
        
    public function GetSumAreaOfCulture($data)
    {
        $query = "SELECT SUM(ZSHN.area_calc) area ";
        $query .= "FROM " . $this->SCHEME . "." . $this->FIELDS_TABLE . " ZSHN ";
        $query .= "WHERE ZSHN." . $data['culture'] . " = '" . $data['culture_val'] . "' AND ZSHN." . $data['farmer'] . " = '" . $data['farmer_val'] . "' ";
        
        $query .= ";";
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
        
        /*SELECT DC.c_name,
               SUM(ZSHN.area_calc)
            
            FROM data.contours_zshn_suchobuzimskoe_2015 ZSHN

            INNER JOIN data.d_cultures DC ON ZSHN.culture_pl = DC.c_name
            
        WHERE ZSHN.owner = 'ЗАО АПХ «АгроЯрск» ОСП «Шилинское»' AND ZSHN.culture_pl IS NOT NULL AND DC.plan_seva_areas = 1
        GROUP BY ZSHN.culture_pl, DC.c_name;*/
    }
    
    public function GetCulturesByKind($kind)
    {
        $query = "SELECT * ";
        $query .= "FROM data.d_cultures ";
        $query .= "WHERE c_kind = '".$kind."'";
        $query .= ";";
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
}
?>
