<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class FarmersModel extends DB 
{
    protected $SCHEME  = 'data';
    protected $DB_NAME = 'inf_region';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    /**
     * @description Метод SetFarmer() закрепляет контуры выделенных полей за хозяйствами
     * @param $table string название таблицы
     * @param $id int Идентификатор контура поля
     * @param $data string Название хозяйства
     * @return Object
     */
     public function SetFarmer($gid, $owner, $idOwner)
     {
         $query = "UPDATE " . $this->SCHEME . "." . $this->FIELDS_TABLE . " ";  
         $query .= "SET ";
         
         $query .= " owner = '". $owner . "',";
         $query .= " id_owner = ". $idOwner;
         
         $query .= " WHERE gid = " .$gid. "; ";
         //echo $query;
         $result = pg_query($this->id_connect, $query);
        
         return $result;
     }
     
     public function GetFarmerInfoById($idFarmer)
     {
         $query = "SELECT * FROM " . $this->SCHEME . "." . $this->FARMERS_TABLE . " ";
         
         $query .= "WHERE id_farmer = " . $idFarmer . ";";
         
         $result = pg_query($this->id_connect, $query);
        
         //echo $query;
         return $result;
     }
     
     /**
     * @description Метод GetFieldOwner() получить название хозяйства по ID поля
     * @param $idField integer Идентификатор поля (gid)
     * @return string Наименование хозяйства
     */
     public function GetFieldOwner($idField)
     {
         $query = "SELECT * FROM " . $this->SCHEME . "." . $this->FIELDS_TABLE . " ";
         
         $query .= "WHERE gid = " . $idField . ";";
         
         $result = pg_query($this->id_connect, $query);
         
         $rowField = pg_fetch_array($result, null, PGSQL_ASSOC);
         
         return $rowField['owner'];
     }
     
     public function GetFarmerBbox($idFarmer)
     {
         $query = "SELECT min(minlat) as minlat, min(minlng) as minlng, max(maxlat) as maxlat, max(maxlng) as maxlng " . 
                  "FROM " . $this->SCHEME . "." . $this->FARMERS_FIELDS_TABLE . " " . 
                  "WHERE id_farmer = " . $idFarmer . ";";
         $result = pg_query($this->id_connect, $query);
         return $result;
     }
}
?>
