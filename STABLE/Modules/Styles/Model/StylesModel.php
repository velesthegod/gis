<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class StylesModel extends DB
{
    public $DB_NAME = 'inf_region';
    
    public $SCHEME = 'data';
    
    public $TABLE_USERS_STYLES = 'users_styles';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    public function GetUserStyles($id_user)
    {
        $query = "SELECT * FROM " . $this->SCHEME . "." . $this->TABLE_USERS_STYLES . " ";
        $query .= "WHERE id_user = " . $id_user . "";
        
        $query .= ";";
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
}
?>
