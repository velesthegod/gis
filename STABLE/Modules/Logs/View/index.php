<?php
    session_start();

    if ( !isset($_SESSION['idRole']) && $_SESSION['idRole'] != 8 ) {
        header("Location: http://".$_SERVER['HTTP_HOST']);
    }
    
    include($_SERVER['DOCUMENT_ROOT']."/header.php");
    
    ?>
    
    <script type="text/javascript">      
        $(function() {
            $( "#datepicker" ).datepicker({
                monthNames:["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                firstDay:1,
                dateFormat:"dd.mm.yy"                                                     
            });
            
            $( "#datepicker-from" ).datepicker({
                monthNames:["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                firstDay:1,
                dateFormat:"dd.mm.yy"                                                     
            });
            
            $( "#datepicker-to" ).datepicker({
                monthNames:["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                firstDay:1,
                dateFormat:"dd.mm.yy"                                                     
            });
            
            $("#show-stat").click(function(){
                $.post('logs-view.php', {date:$("#datepicker").val()}, function(result)
                {
                    $("#block-stat").replaceWith(result);
                });
            });
            
            $("#show-stat-range").click(function(){
                $.post('logs-view.php', {dateFrom:$("#datepicker-from").val(), dateTo:$("#datepicker-to").val()}, function(result)
                {
                    $("#block-stat").replaceWith(result);
                });
            });
            
        });
    </script>
    
    <div class="container" style="margin-top: 40px;">
        <div class="row">
            <div id="statistic-header">
                <div style="margin: 5% 0px 3% 26%;">
                    <p style="font-size: 20pt; font-family: Arial, sans-serif; color: #4E8BA7;">Статистика активности пользователей</p>
                </div>
                <div class="horizontal-line"></div>
                <div class="span2">
                    <div><p>Просмотр статистики за дату</p></div>
                    <div><input type="text" id="datepicker"></div>
                    <button type="button" id="show-stat">Показать</button>
                </div>
                <div class="span2">
                    <div><p>Просмотр статистики за период</p></div> 
                    <div>
                        <input type="text" id="datepicker-from">
                        <div class="defis"></div>
                        <input type="text" id="datepicker-to">
                    </div>
                    <button type="button" id="show-stat-range">Показать</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="span12" style="margin: 40px 0 0 0;">
                <div id="block-stat"></div>
            </div>    
            
            
            <script type="text/javascript" src="/public/google-charts/canvasjs.min.js"></script>
        </div>
    </div>