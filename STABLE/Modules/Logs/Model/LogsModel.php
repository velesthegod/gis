<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");
require_once($_SERVER['DOCUMENT_ROOT']."/vendor/Misc.php");

class LogsModel extends DB
{
	protected $SCHEME  = 'data';
    protected $DB_NAME = 'inf_region';
    
    public $id_connect;
    public $miscObject;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
        $this->miscObject = new Misc();
    }
	
	public function SaveLog($type, $id_user)
	{
		$description = self::LogType($type);
        
        $date_action = $this->miscObject->getCurrentDate();
		
         $query = "INSERT INTO " . $this->SCHEME . "." . $this->A_LOGS_TABLE . 
                  " ( l_type, id_user, l_description, date_action ) " . 
                  "VALUES ( " . $type . ", " . $id_user . ", '" . $description . "', '" . $date_action . "');";
         $result = pg_query($this->id_connect, $query);
         return $result;
	}
	
	protected function LogType($type)
	{
		switch ($type) {
		 	case 1:
                $description = "Вход пользователя в систему";
                break;
            case 2:
                $description = "Закрепление поля за хозяйством";
                break;
            case 3:
                $description = "Удаление поля из хозяйства";
                break;
            case 4:
                $description = "Указание плановой культуры";
                break;
            case 5:
                $description = "Указание фактической культуры";
                break;
		}
		return $description;
	}
    
    public function GetUserEntriesStatistic($date, $id_user)
    {
        $query = "SELECT count(logs.id_user) user_entries
                  FROM " . $this->SCHEME . "." . $this->A_LOGS_TABLE . " logs
                  INNER JOIN " . $this->SCHEME . "." . $this->FARMERS_TABLE . " farmer ON (logs.id_user = farmer.id_farmer)
                  WHERE date_action = '" . $date . "' and l_type = 1 and id_user = " . $id_user . ";";
        $result = pg_query($this->id_connect, $query);
        return $result;
    }
    
    public function GetUserEntriesStatisticByRange($date_from, $date_to, $id_user)
    {
        $query = "SELECT id_user, date_action, count(id_log) user_entries 
                    FROM " . $this->SCHEME . "." . $this->A_LOGS_TABLE . "  
                  WHERE date_action <= '".$date_to."' and date_action >= '".$date_from."' and id_user = ".$id_user."  
                  GROUP BY id_user, date_action 
                  ORDER BY date_action DESC;";
        $result = pg_query($this->id_connect, $query);
        return $result;
    }
}
?>