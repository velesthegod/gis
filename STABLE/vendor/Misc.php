<?php
class Misc 
{
    public function IdsStringToArray($string)
    {
        $idsArray = explode(",", $string);
        $idsArray = array_filter($idsArray);
        //var_dump($idsArray);

        return $idsArray;
    }
     
    public function getCurrentDate()
    {
        //1000-01-01 00:00:00
        date_default_timezone_set('asia/krasnoyarsk');
        //$currentDate = date('Y-m-d H:m:s');
        $currentDate = date('Y-m-d');
        return $currentDate;
    }
    
    public function CleanFormData($value)
    {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        
        return $value;
    }
}
?>
