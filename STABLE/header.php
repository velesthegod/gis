<?
session_start();
Error_Reporting(E_ALL & ~E_NOTICE);
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="/public/images/favicon.ico" type="image/x-icon">
<meta name="DESCRIPTION" content="Программно-технологический геоинформационный комплекс агромониторинга для агромониторинга Красноярского края">
<meta http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Lang" content="en">
<meta name="author" content="ИКИТ">
<title>ГИС Агромониторинга</title>

<link rel="shortcut icon" type="image/png" href="http://krasnoyarsk-region.rekod.ru/statics/favicon/faviconRekod.png"/>
<link rel="stylesheet" type="text/css" href="/public/css/demo-examples.css">

<!--jQuery-->
<link rel="stylesheet" type="text/css" href="/public/jquery/jquery-ui.css">
<script src="/public/jquery/jquery-1.10.2.js" type="text/javascript"></script>
<script src="/public/jquery/jquery-ui-1.9.2.js" type="text/javascript"></script>

<!--GEOPORTAL API-->
<script src="http://edu.ikit.sfu-kras.ru/public/javascripts/geoportal/geoportal-api.min.js" type="text/javascript"></script>

<!--Рисование -->
<script src="/public/js/PolygonDraw.js" type="text/javascript"></script>
<script src="/public/js/RectangleDraw.js" type="text/javascript"></script>

<!--BOOTSTRAP-->
<link rel="stylesheet" type="text/css" href="/public/bootstrap2/css/bootstrap.css">
<script src="/public/bootstrap2/js/bootstrap.js" type="text/javascript"></script>

<!--ACTIVEMAP MANAGER-->
<script src="/public/js/activemap-manager.js" type="text/javascript"></script>
<script src="/public/js/geojson.ndvi.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/css/s_activemap_ui.css">
<link rel="stylesheet" href="/public/css/ui.slider.css">
<link rel="stylesheet" href="/public/css/ui.horizontal.loader.css">
<link rel="stylesheet" href="/public/css/ui.filter.css">

<script type="text/javascript">
var ieStyle = BrowserIs({'ie':'/public/css/ie.css', 'firefox':'/public/css/ie.css', 'chrome':''});
document.write('<link rel="stylesheet" type="text/css" href="' + ieStyle + '">');
var zoomControlShift = BrowserIs({'ie':'440px', 'chrome':'420px', 'firefox':'440px'});
</script>

<!--LIGHT TABS-->
<script src="/public/simple-tabs/js/ion.tabs.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/simple-tabs/css/ion.tabs.css">
<link rel="stylesheet" type="text/css" href="/public/simple-tabs/css/ion.tabs.skinBordered.css">

<!--Stickers-->
<script src="/public/stickers/jquery.stickr.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/stickers/stickr.css">

<script src="/public/js/ObjectInfo.js" type="text/javascript"></script>
<script src="/public/js/ObjectInfoHandler.js" type="text/javascript"></script>
<script src="/public/js/AMLayers.js" type="text/javascript"></script>
<script src="/public/js/Filter.js" type="text/javascript"></script>
<script src="/public/js/FilterHandler.js" type="text/javascript"></script>

<?
// Подключаем необходимые библиотеки
include("Modules/Users/Model/UsersModel.php");
include("vendor/Misc.php");
include("Modules/Popup/Model/PopupModel.php");
include("Modules/Styles/Model/StylesModel.php");
include("Modules/Farmers/Model/FarmersModel.php");
include("Modules/Logs/Model/LogsModel.php");

$authButton = $_POST['auth'];
$authExitButton = $_POST['auth-exit'];
$z = 0;
if ( $authButton )
{
    $userModelObject = new UsersModel();
    $miscObject = new Misc();
    
    $login = $miscObject->CleanFormData($_POST['login']);
    $password = $miscObject->CleanFormData($_POST['password']);
    
    $encodePassword = md5($password);
    
    // Проверяем существует ли такой пользователь в БД, прежде чем записать его в сессию
    $result = $userModelObject->GetUser($login);
    $row = pg_fetch_array($result, null, PGSQL_ASSOC);
    
    if ( $row['login'] == $login && $row['password'] == $encodePassword )
    {
        $_SESSION['uLogin'] = $login;
        $_SESSION['uPassword'] = $password;
        $_SESSION['idRole'] = trim($row['role_id']);
        $_SESSION['idUser'] = trim($row['id']);
    }
}
if ( $authExitButton )
{
    session_unset();
    session_destroy();
}
if ( isset($_SESSION['uLogin']) && !empty($_SESSION['uLogin']) )
{
    $stylesModelObject = new StylesModel();
    // ПОЛУЧАЕМ ID ХОЗЯЙСТВА ПО ID ПОЛЬЗОВАТЕЛЯ. TODO: переделать на таблицу связи пользователей и хозяйств.
    $resultStyles = $stylesModelObject->GetUserStyles($_SESSION['idUser']);
    if ( isset($resultStyles) && !empty($resultStyles) ) {
        $rowStyles = pg_fetch_array($resultStyles, null, PGSQL_ASSOC);
        
        // ПОЛУЧАЕМ НАЗВАНИЕ ХОЗЯЙСТВА ИЗ СПРАВОЧНИКА ПО ID ХОЗЯЙСТВА.
        if ( isset($rowStyles['id_farmer']) ) {
            // Запоминаем ID хозяйства.
            $_SESSION['idFarmer'] = $rowStyles['id_farmer'];
			// Записываем в лог вход пользователя.
            $logsModelObject = new LogsModel();
            $logsModelObject->SaveLog(1, $rowStyles['id_farmer']);
            $popupModelObject = new PopupModel();
            $farmersModelObject = new FarmersModel();
            $resultFarmers = $popupModelObject->SelectDictionaryData($popupModelObject->FARMERS_TABLE, 'f_name', $rowStyles['id_farmer']);
            if ( isset($resultFarmers) && !empty($resultFarmers) ) {
                $rowFarmers = pg_fetch_array($resultFarmers, null, PGSQL_ASSOC);
                // Запоминаем название хозяйства.
                $_SESSION['farmerName'] = trim($rowFarmers['f_name']);
            }
            // ПОЛУЧАЕМ BBOX ХОЗЯЙСТВА, ЕСЛИ ОН ЕСТЬ.
            $result = $farmersModelObject->GetFarmerBbox($rowStyles['id_farmer']);
            if ( isset($result) && !empty($result) ) {
                $farmerBbox = pg_fetch_array($result, null, PGSQL_ASSOC);
                $_SESSION['minLat'] = $farmerBbox['minlat'];
                $_SESSION['minLng'] = $farmerBbox['minlng'];
                $_SESSION['maxLat'] = $farmerBbox['maxlat'];
                $_SESSION['maxLng'] = $farmerBbox['maxlng'];
            }
        }
    }
}
?>

<script type="text/javascript">
/****************************** ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ ******************************/

var login = "<?=$_SESSION['uLogin']?>";
var password = "<?=$_SESSION['uPassword']?>";
var idFarmer = "<?=$_SESSION['idFarmer']?>";
var farmerName = "<?=$_SESSION['farmerName']?>";
var role = "<?=$_SESSION['idRole']?>";
var adminRole = 8;

var _minLat = "<? if ( !empty($_SESSION['minLat']) ) echo $_SESSION['minLat']; else echo $z; ?>";
var _minLng = "<? if ( !empty($_SESSION['minLng']) ) echo $_SESSION['minLng']; else echo $z; ?>";
var _maxLat = "<? if ( !empty($_SESSION['maxLat']) ) echo $_SESSION['maxLat']; else echo $z; ?>";
var _maxLng = "<? if ( !empty($_SESSION['maxLng']) ) echo $_SESSION['maxLng']; else echo $z; ?>";

var hostName = 'edu.ikit.sfu-kras.ru';

var currentUserName;
var currentUserLogin;

var farmerUniversalStyleName = 'contours_zshn_suchobuzimskoe_2015_universal_style';
var culturesPlanUniversalStyleName = 'cultures_plan_universal_style';
var culturesFactUniversalStyleName = 'cultures_fact_universal_style';
var NDVIUniversalStyle = 'ndvi_universal_style';

var filterCQLFarmer = new GeoPortal.Filter.CQL([
    {
        field: "owner",
        type: "string",
        value: farmerName
    }
]);

var queryArray = new Array();
var selectorLayer = new Array();

var idsString = '';

var mapObject;
var layersStore = new Array();

var _currentToken;

var agriculturalLayer;
var layerOfFarmer;
var layerCultures;
var layerCulturesFact;

var randmz = Math.random();

var gid;
var someString = '';
var groups_count;
var _compare_bbox = {minLat: 90, maxLat: 0, minLng: 180, maxLng: 0};

var date = new Date();
currentYear = date.getFullYear();

var editingLayerId;

/***********************************************************************************/

$(function() {
    var clickLatLn;
        
    var userPanelDiv = ObjectInfo._userPanel = $("#user-panel"), 
        rollUserPanelDiv = ObjectInfo._rollUserPanel = $(".roll-user-panel");
    
    // ACCORDION ДЛЯ МЕНЮ СПРАВОЧНИКОВ.    
    $( "#accordion" ).accordion({
        collapsible: false
    });
    
    // АВТОРИЗАЦИЯ.
    GeoPortal.authenticate(login,password,
        function(data){
            // ПОЛУЧАЕМ ВСЕ ДОСТУПНЫЕ ГРУППЫ СЛОЕВ. 
            // ПАРАМЕТР true ОЗНАЧАЕТ, ЧТО ВСЕ ГРУППЫ ВЕРНУТЬСЯ СО СЛОЯМИ.
            // callback(groups) – функция выполняется при получении групп слоев с сервера. Принимает на вход один параметр - массив экземпляров класса GeoPortal.LayerGroup.
            // callErrorBack(status,error) – функция, которая будет выполняться при ошибке во время запроса, на вход должна принимать статус и описание ошибки.
            GeoPortal.requestGroups(true,
                function(groups) {
                    // Получаем массив слоев, чтобы запомнить в глобальную переменную agriculturalLayer рабочий слой полей.
                    var layers = groups[0].layers();
                    agriculturalLayer = layers[0];
                    
                    EnableWorkingLayer(agriculturalLayer);                

                    // Если группы слоев существуют, то оформляем графически КОНТЕНТ ПРАВОЙ ПАНЕЛИ с группами.
                    if(groups.length >= 1) {
                        var i;
                        groups_count = (groups.length+1) * 30;
                        var groupsHeight = $("#map").height() - 150;
                        
                        for ( i=0; i<groups.length; i++ ) {
                            drawGroup(groups[i]);
                        }
                        
                        $("#groups").height(groupsHeight);
                        // Changing DEFAULT Settings for jQuery accordion
                        $("#groups").accordion();
                        $(".ui-accordion-content").height(groupsHeight - groups_count);
                        $(".ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br").css({"border-bottom-right-radius":"0px", "border-bottom-left-radius":"0px"});
                        $(".ui-widget-content").css({"border":"0px solid #aaaaaa", "color":"#000000"});
                        $(".ui-accordion .ui-accordion-header").css({"display":"block", "cursor":"pointer", "position":"relative", "margin":"0px 0px 1px 0px", "padding":"8px 0px 0px 30px", "height":"26px", "min-height":"26px", "max-height":"26px", "font":"bold 10pt sans-serif", "font-style":"normal", "font-variant":"normal", "font-stretch":"normal"});
                        $(".ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default").css({"color":"#FFFFFF", "background":"#9EB5BF", "border":"0px solid #D3D3D3"});
                        // background for active h3 div header
                        $(".ui-accordion-header-active").css({"color":"#FFFFFF", "background":"#6D8692", "border":"0px solid #6D8692"});
                        $(".ui-accordion-header").click(function(){
                            $(".ui-accordion-header").css({"color":"#FFFFFF", "background":"#9EB5BF", "border":"0px solid #6D8692"});
                            $(".ui-accordion-header-active").css({"color":"#FFFFFF", "background":"#6D8692", "border":"0px solid #6D8692"});
                        });
                        // radius for h3 div header
                        $(".ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl").css({"border-top-left-radius":"0px"});
                        $(".ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr").css({"border-top-right-radius":"0px"});
                        $(".ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-tl").css({"border-bottom-left-radius":"0px"});
                        $(".ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-tr").css({"border-bottom-right-radius":"0px"});
                        // accordion content
                        $(".ui-accordion .ui-accordion-content").css({"padding":"0", "color":"#000000", "font":"normal 10pt sans-serif"});
                    }
                    
                    // Оформляем САМУ ПРАВУЮ ПАНЕЛЬ с группами слоев.
                    var groupsDiv = $("#groups");
                    groupsDiv.before('<div id="roll-up"><p>ОБЩИЕ СЛОИ</p></div>');
                    groupsDiv.addClass('groups-hide');
                    // Сворачиваем и разворачиваем правую панель со слоями.
                    $("#roll-up").click(function(){
                        if ( groupsDiv.hasClass('groups-hide') == true )
                        {
                            groupsDiv.removeClass('groups-hide');
                            groupsDiv.addClass('groups-show');
                            groupsDiv.slideUp(500);
                            $(this).css({"opacity":"0.5", "-webkit-opacity":"0.5", "-moz-opacity":"0.5", "-o-opacity":"0.5"});
                        }
                        else
                        {
                            groupsDiv.removeClass('groups-show');
                            groupsDiv.addClass('groups-hide');
                            groupsDiv.slideDown(500);
                            $(this).css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
                        }
                    });
                    
                    // ОТРАБАТЫВАЕМ СОБЫТИЯ, СВЯЗАННЫЕ С ОБЩИМИ СЛОЯМИ.
                    $("#groups").find(".layer").find("input").on("click",function() { 
                        var id = $(this).val(), 
                        layer = layersStore[id];

                        // ВКЛЮЧАЕМ КАРТИНКУ ПЕРЕЛЕТА К СЛОЮ И ОТРАБАТЫВАЕМ САМ ПЕРЕЛЕТ.
                        if ( document.getElementById('checkbox-layer-' + id).checked == true ) 
                        { 
                            if ( role == adminRole ) {
                                $("#checkbox-layer-" + id).after('<span class="create-layer-object" id="create-layer-object-' + id + '"><img src="public/images/img-add-object.png" title="Создать объект в этом слое" /></span>');
                                $("#checkbox-layer-" + id).after('<span class="download-layer" id="download-layer-' + id + '"><img src="public/images/download-layer.png" title="Скачать слой" /></span>');
                            }
                            $("#checkbox-layer-" + id).after('<span class="fly" id="fly-' + id + '"><img src="public/images/fly-to-layer.png" title="Перелет к слою" /></span>'); 
                            
                            $("#fly-" + id).click(function(){ 
                                layer.requestBbox( 
                                    function(bbox){ 
                                        mapObject.fitBounds(bbox.minx, bbox.miny, bbox.maxx, bbox.maxy);
                                    }, 
                                    function(status, error){ 
                                        console.log(error); 
                                }); 
                            });
                            
                            $("#download-layer-" + id).click(function(){
                                var layer_name = layer.name();

                                if ( layer._model._values.info.style == "raster" )
                                {
                                    var url = "http://"+hostName+"/geoserver/workspace/wms/kml?"+"layers="+layer_name;
                                }
                                else
                                {
                                    var url = "http://"+hostName+"/geoserver/workspace/ows?"+
                                              "service=WFS&"+
                                              "version=1.1.0&"+
                                              "request=GetFeature&"+
                                              "typeName="+layer_name+"&"+
                                              "outputFormat=SHAPE-ZIP&"+
                                              "format_options=charset:cp1251";
                                }
                                
                                window.open(url, '_blank');
                            });
                            
                            $("#create-layer-object-" + id).click(function(){
                                // Если в левой панеле во вкладке Свойства уже была открыта какая то форма, то удаляем ее:
                                $('#user-panel #object-info-form').remove();
                                // Активируем контрол для рисования:
                                PolygonDraw._activateControl();
                                // Разворачиваем левую панель
                                userPanelDiv.slideDown(500);
                                userPanelDiv.attr('class', 'up-hide');
                                rollUserPanelDiv.css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
                                // Делаем активной нужную вкладку таба.
                                $.ionTabs.setTab("Tabs_Group_name", "Tab_1_name");
                                // Перемещаем контрол zoom вправо
                                $(".zoom-control").css({"left":zoomControlShift}); 
                                // Создаем форму для ввода атрибутов объекта
                                ObjectInfo._layer = layersStore[id];
                                var createForm = ObjectInfo.CreateForm();
                                $("div[data-name='Tab_1_name']").prepend(createForm);
                            });
                        } 
                        else 
                        { 
                            $("#create-layer-object-" + id).remove();
                            $("#download-layer-" + id).remove();
                            $("#fly-" + id).remove();
                        } 

                        if(typeof layer != 'undefined') { 
                            layer.turn(mapObject); 
                        } 
                    });
                    // ОТОБРАЖАЕМ КАРТИНКУ ИНФОРМАЦИИ О СЛОЕ И ОТРАБАТЫВАЕМ ВКЛЮЧЕНИЕ ЛЕГЕНДЫ СЛОЯ.
                    $(".info").click(function(){ 
                        $("#layer-legend").remove(); 
                        
                        var id = this.id, 
                            layer = layersStore[id], 
                            legend, 
                            legend = layer.legend(); 
                        
                        userPanelDiv.slideDown(500); 
                        userPanelDiv.attr('class', 'up-hide'); 
                        rollUserPanelDiv.css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
                        // Делаем активной нужную вкладку таба.
                        $.ionTabs.setTab("Tabs_Group_name", "Tab_4_name");
                        $.ionTabs.setTab("SubTabs_Group_name", "SubTab_1_legend");
                        $(".zoom-control").css({"left":zoomControlShift}); 
                        $("div[data-name='SubTab_1_legend']").prepend('<div id="layer-legend" class="tab-content"><div class="legend"><img src="' + legend + '" /></div></div>');
                        Filter._layer = layer;
                        var filter = Filter.Form();
                        $("div[data-name='SubTab_2_filter'] #layer-filter-form").replaceWith(filter);
                    });
                },
                
                function(status,error) {
                    console.log(status);
                    console.log(error);
                }
            );
            
            function drawGroup(group) { 

                if(typeof group == "undefined") 
                    return; 

                var groupsDiv = $("#groups"), 
                groupDiv, layersDiv, layers, key; 

                groupsDiv.append('<h3>' + group.name() + '</h3>' + 
                        '<div><div class="group"><div class="layers"></div></div></div>'); 
                groupDiv = groupsDiv.last(); 
                layersDiv = groupDiv.find(".layers:last"); 
                layers = group.layers();

                for(key in layers) {

                    layersStore[layers[key].id()] = layers[key]; 

                    var layerName = layers[key].rusName(); 
                    
                    shortLayerName = GetShortLayerName(layerName); 

                    layersDiv.append('<div class="layer"><input type="checkbox" class="checkbox-layer" id="checkbox-layer-'+ layers[key].id() +'" value="'+ layers[key].id() +'">' + '<span title="' + layerName + '">' + shortLayerName + '</span>' + '<span class="info" id="' + layers[key].id() + '"><img src="public/images/info-icon.png" title="Информация о слое"></span></div>');
                } 
            };
            
            // Получаем текущего пользователя
            GeoPortal.currentUser(
                function(user){
                    if (user != null){
                        currentUserLogin = user.login;
                        currentUserName = user.name;
                        $(".hello-user").html('Добро пожаловать, '+user.name);
                    }
                },
                function(status,error){
                    console.log("Error to request authentication. Status = " +
                                status + ". Error text: " + error);
                }
            );
            // end
        },
        function(status,error){
            console.log("Error to request authentication. Status = " +
                        status + ". Error text: " + error);
        }
    );
    // end
    
    // Отображаем элементы геопортала, когда объект GeoPortal загружен
    GeoPortal.on("ready",function() {

        
        var schemas     = GeoPortal.baseLayers.schemas,
            schemasLen  = schemas.length,
            spaces      = GeoPortal.baseLayers.spaces,
            spacesLen   =  spaces.length,
            currentBaseLayer,
            i=0,
            selected = "",
            baseLayer,
            baseLayerArray = new Array();
        
        _currentToken = GeoPortal._accessToken;
        
        // ЕСЛИ ПОЛЬЗОВАТЕЛЬ АДМИНИСТРАТОР, ТО ОН ДОЛЖЕН УВИДЕТЬ ВСЕ ХОЗЯЙСТВА, ВСЕ КУЛЬТУРЫ ПЛАНИРУЕМЫЕ И ФАКТИЧЕСКИЕ.
        if ( farmerName == '' ) {
            farmerUniversalStyleName = 'all_farmers_color_admin_style';
        }

        // формируем слой хозяйства, чтобы он был доступен везде
        layerOfFarmer = new GeoPortal.Layer.WMS("http://"+hostName+"/service/wms", {
            service: 'WMS',
            request: 'GetMap',
            version: '1.1.1',
            layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
            styles: farmerUniversalStyleName,
            format: 'image/png',
            transparent: true,
            cql_filter: filterCQLFarmer.filterString(),
            token: _currentToken,
            random: randmz
        });
        
        layerCultures = new GeoPortal.Layer.WMS("http://"+hostName+"/service/wms", {
            service: 'WMS',
            request: 'GetMap',
            version: '1.1.1',
            layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
            styles: culturesPlanUniversalStyleName,
            format: 'image/png',
            transparent: true,
            cql_filter: filterCQLFarmer.filterString(),
            token: _currentToken,
            random: randmz
        });
        
        layerCulturesFact = new GeoPortal.Layer.WMS("http://"+hostName+"/service/wms", {
            service: 'WMS',
            request: 'GetMap',
            version: '1.1.1',
            layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
            styles: culturesFactUniversalStyleName,
            format: 'image/png',
            transparent: true,
            cql_filter: filterCQLFarmer.filterString(),
            token: _currentToken,
            random: randmz
        });
        
        mapObject = new GeoPortal.Map('map');
        
        // Добавляем элемент Zoom
        zoom = new GeoPortal.Control.Zoom();
        zoom.on("handClick", function(){
            console.log("icon hand click");
        },this);
        // Позиционируем карту. Это позиционирование по умолчанию.
        mapObject.on("ready",function(){
                mapObject.setView(92.93111444,56.0641667,9);
        },this);

        mapObject.addControl(zoom);
        // end 
        
        // Добавляем элемент Измерение дистанции
        distance = new GeoPortal.Control.Distance();
        distance.on("control:distance:enable", function(data){},this);
        distance.on("control:distance:disable", function(data){},this);
        mapObject.addControl(distance);
        $('div#distanceButton img').prop("title", "Измерение расстояния");
        // end
        
        // Добавляем элемент Рисования объекта
        PolygonDraw = new GeoPortal.Control.PolygonDraw;
        PolygonDraw.on("control:DrawPolygon:enable", function(data){},this);
        PolygonDraw.on("control:DrawPolygon:disable", function(data){},this);
        mapObject.addControl(PolygonDraw);
        $("#polygondraw").hide();
        // end
        
        RectangleDraw = new GeoPortal.Control.RectangleDraw();
        RectangleDraw.on("control:RectangleDraw:enable", function(data){},this);
        RectangleDraw.on("control:RectangleDraw:created", function(data){
            Filter._filter_bbox = new GeoPortal.LatLngBounds(data.latLngs[0],data.latLngs[2]);
            $(Filter.selectors['spanCoordinates']).prepend('('+Filter._filter_bbox._northEast.lng+'), ('+Filter._filter_bbox._northEast.lat+'), ('+Filter._filter_bbox._southWest.lng+'), ('+Filter._filter_bbox._southWest.lat+')');
        },this);
        RectangleDraw.on("control:RectangleDraw:disable", function(data){
             $(Filter.selectors['spanCoordinates']).empty();
        },this);
        mapObject.addControl(RectangleDraw);
        $("#rectangleDrawButton").hide();
        
        // Добавляем элементы select для выбора карт-подложек
        currentBaseLayer = mapObject.baseLayer();
        
        $("body").find("#map").after('<div id="baseLayerContainer"/>');
            $("#baseLayerContainer").append('<div class="schemasContainer"><select class="selectBaseLayer"><option value="" selected>'+GPMessages("default.select")+'...</option></select></div>');
            $("#baseLayerContainer").append('<div class="spacesContainer"><select class="selectBaseLayer"><option value="" selected>'+GPMessages("default.select")+'...</option></select></div>');
        
        if(schemasLen >0) {
            for(i=0; i<schemasLen; i++) {
                baseLayer = schemas[i];
                baseLayerArray[baseLayer.id()] = baseLayer;

                if(baseLayer.id() == currentBaseLayer.id())
                    selected = "selected";
                else
                    selected = "";

                $("#baseLayerContainer>.schemasContainer>select").append('<option value="'+baseLayer.id()+'" '+selected+'>'+baseLayer.name()+'</option>');
            }
        }
        if(spacesLen >0) {
            for(i=0;i<spacesLen;i++) {
                baseLayer = spaces[i];
                baseLayerArray[baseLayer.id()] = baseLayer;

                if(baseLayer.id() == currentBaseLayer.id())
                    selected = "selected";
                else
                    selected = "";

                $("#baseLayerContainer>.spacesContainer>select").append('<option value="'+baseLayer.id()+'" '+selected+'>'+baseLayer.name()+'</option>');
            }
        }
        // Поключаемся к событию "change" у каждого селектора. Когда новый базовый слой выбран, устанавливаем карте.
        $(".selectBaseLayer").change(function(){
            var id = $(this).val(),
                baseLayer = baseLayerArray[id];

            if(typeof baseLayer != 'undefined') {
                mapObject.setBaseLayer(baseLayer);
                currentBaseLayer = baseLayer;
            }
            if($(this).parent("div").hasClass("schemasContainer")){
                $("#baseLayerContainer>.spacesContainer>select").children("option:first").attr("selected","selected");
            }
            else{
                $("#baseLayerContainer>.schemasContainer>select").children("option:first").attr("selected","selected");
            }

        });
        // end
        
        // ПОЛУЧИТЬ КООРДИНАТЫ КЛИКА ПО КАРТЕ
        mapObject.on("click",function(e) {
            clickLatLng = e.latlng;
            $("#click").remove();
            $(".wrap").append('<div id="click"><p class="latLng">Широта|Долгота: ' + clickLatLng.lat + " | " + clickLatLng.lng + '</p></div>');
        },this);
        
        // ОБРАБОТКА КЛИКА ПО ОБЪЕКТУ НА КАРТЕ
        mapObject.on("featureClicked",function(data) {
            
            if(typeof data.features == 'undefined') {
                // console.log("Request features error. Status = " + status + ". Error text: " + error);
                return;
            }
            // Делаем деселект редактируемого контура
            if (ObjectInfo._layerEditor != null) {
                ObjectInfo._layerEditor.editing.disable();
                mapObject.removeLayer(ObjectInfo._layerEditor);
                ObjectInfo._layerEditor = null;
                mapObject.removeLayer(ObjectInfo._layerSelector);
                ObjectInfo._layerSelector = null;
            }

            var features = data.features;
            ObjectInfo._features = features;

            // закраска объекта
            var objGeometry = ObjectInfo._geom = JSON.parse(features[0]._model._values.data.geom);
            geoJson = objGeometry;

            var layer1 = new GeoPortal.Layer.GeoJSON(
                geoJson, {color: '#32A4D2', /*weight: 5, opacity: 0.65,*/ editable:true,fillOpacity:0.65} //#32A4D2
            );
            
            ObjectInfo._layerSelector = layer1;
            selectorLayer.push(layer1);
            //alert(selectorLayer.length);
            mapObject.addLayer(layer1);
            
            // убираем выделение объекта одинарным щелчком мыши по нему
            layer1.on("click",function(e) {
                // когда делаем деселект контура, то убираем ID этого контура из строки idsString
                mapObject.removeLayer(layer1);
                layer1 = null;
                idsString = idsString.replace(new RegExp(objFID.toString()+',','g'),"");
                someString = someString.replace(new RegExp(objFID.toString(),'g'),"");
                // и очищаем массив координат.
                objGeometry = null;
            },this);
            
            var objFID = JSON.parse(features[0]._model._values.data.fid);
            // строка ID выбранных контуров полей
            idsString += objFID + ',';
            
            var objGeom = JSON.parse(features[0]._model._values.data.geom);
            
            var _object_bbox = GetBbox(objGeom);
            
            someString += "|fid:"+objFID+", minLat:"+_object_bbox['minLat']+", minLng:"+_object_bbox['minLng']+", maxLat:"+_object_bbox['maxLat']+", maxLng:"+_object_bbox['maxLng']+"|";
		
            // При клике по объекту, имеющему координаты, выполняем следующие действия:
            if(clickLatLng instanceof GeoPortal.LatLng) {
                // ПРОВЕРЯЕМ НА РЕЖИМ ПЕЧАТИ КАРТЫ. ЕСЛИ НЕ В РЕЖИМЕ ПЕЧАТИ, ТО МОЖНО ВСЕ.
                if ( $("#print-mode-hidden").val() == 0 ) {
                    // убираем элемент object-info в левой панеле, если оно было создано,
                    $("#object-info-form").remove();
                    // разворачиваем левую панель,
                    userPanelDiv.slideDown(500); 
                    userPanelDiv.attr('class', 'up-hide'); 
                    rollUserPanelDiv.css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
                    // Делаем активной нужную вкладку таба.
                    $.ionTabs.setTab("Tabs_Group_name", "Tab_2_name");
                    // Перемещаем контрол zoom в видимую область.
                    $(".zoom-control").css({"left":zoomControlShift});
                    
                    // Выводим информацию о выбранных объектах, создаем элемент object-info в левой панеле.
                    var selected = ObjectInfo._selected(mapObject, layersStore, features);
                    $("div[data-name='Tab_1_name']").prepend(selected);
                }
            }
            
            /*$('#user-panel').on('click', 'button.temp', function(){
                console.log('Работает');
            });*/
            
        },this);
        // end
    },this);
    // END Geoportal ready
});
</script>
<script src="/public/js/activemap-index.js" type="text/javascript"></script>

</head>

<body>

<div class="container">
    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="navbar-inner">
            <div class="nav-collapse">
                 <ul class="nav">
                    <li class="active">
                        <a class="brand" style="margin:0px;" href="/">
                           Система агромониторинга
                        </a>
                    </li>
                    <? if ( isset($_SESSION['idRole']) && $_SESSION['idRole'] == 8 ) { ?>
                    <li class="dropdown">
                        <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/public/images/dictionary.png" alt="Справочники" title="Справочники" style="width:24px; height:18px;">
                            <b class="caret"></b>
                        </a>
                     
                        <ul class="dropdown-menu" style="width: 235px;">
                            <li class="nav-header">Управление справочниками
                            <ul style="list-style: none;">
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php">Главная</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqCultures">С/Х Культуры</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqAgriculturalWorks">Виды сельскохозяйственных работ</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqChemcomposition">Химический состав почв</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqOwnership">Формы собственности</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqFarmers">Хозяйства</a></li>
                            </ul>
                            </li>
                        </ul>
                    </li>
					<li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/public/images/stat.png" alt="Статистика" title="Статистика" style="height: 24px; width: 24px; margin: -6px 0px -3px 0px;">
                            <b class="caret"></b>
                        </a>
                     
                        <ul class="dropdown-menu" style="width: 235px;">
                            <li class="nav-header">Статистика
                            <ul style="list-style: none;">
                                <li><a href="<?=$_SERVER['REMOTE_SERVER']?>/Modules/Logs/View/index.php">Входы пользователей</a></li>
                            </ul>
                            </li>
                        </ul>
                    </li>
                    <? } ?>
                    <li class="divider-vertical"></li>
                    <li>
                        <div class="printMode">
                            <img src="/public/images/print.png" alt="Печать карты" title="Печать карты" style="margin-left: 12px; margin-right: 12px;">
                            <input type="hidden" id="print-mode-hidden" value="0" />
                        </div>
                    </li>
                    <li class="divider-vertical"></li>
                </ul>
                <? if ( isset($_SESSION['uLogin']) && !empty($_SESSION['uLogin']) ) { ?>
                    <div id="hello-box">
                        <p class="hello-user"></p>
                    </div>
                    <div class="menu-authorization-exit">
                        <form action="<?=$_SERVER['PHP_SELF']?>" method="post">
                            <input type="submit" name="auth-exit" value="  Выйти">
                        </form>
                    </div>
                <? } else { ?>
                    <div class="menu-authorization">
                        <div></div>
                    </div>
                <? } ?>
                
            </div>
        </div>
    </div>
    <!--  АВТОРИЗАЦИЯ  -->
    <? if ( empty($_SESSION['uLogin']) ) { ?>
    <div id="authorization-box">
        <p class="authorization-title">Авторизация</p>
        <div class="authorization-form">
            <form action="<?=$_SERVER['PHP_SELF']?>" method="post">
                <table>
                <tr>
                    <td align="right" style="width: 100px;"><p class="authorization-login">Логин:</p></td>
                    <td align="right"><input class="authorization-login-element" type="text" id="login" name="login"></td>
                </tr>
                <tr>
                    <td align="right"><p class="authorization-password">Пароль:</p></td>
                    <td align="right"><input class="authorization-password-element" type="password" id="password" name="password"></td>
                </tr>
                </table>
                <div class="button-block">
                    <input type="submit" id="authorization-enter" name="auth" value="Вход">
                    <button type="button" id="authorization-cancel">Отмена</button>
                </div>
            </form>
        </div>
    </div>
    <? } ?>
    <!--  END  -->
</div>
