<?php
/**
 * Sample PostgreSQL driver
 * It's just an example - use PDO if you can
 */

class jqGrid_DB_Pgsql extends jqGrid_DB
{
    protected $db_type = 'postgresql';

    public function link()
    {
        static $link = null;

        if(!$link)
        {
            $connect = $this->Loader->get('db_pg_connect');
            $link = pg_connect($connect);
            //$link = pg_connect('pgsql:host=193.218.136.151;port=5432;dbname=inf_region;user=region_user;password=1QaZfdt67M2');
            //echo '<br>Postres connect: '.$link . "<br>";

            if(!$link)
            {
                throw new jqGrid_Exception_DB(pg_last_error());
            }
        }

        return $link;
    }

    /**
     * @param $query
     * @return resource
     * @throws jqGrid_Exception_DB
     */
    public function query($query)
    {
        //echo "Query: " . $query;
        $result = pg_query($this->link(), $query);

        if(!$result)
        {
            throw new jqGrid_Exception_DB(pg_last_error(), array('query' => $query));
        }

        return $result;
    }

    public function fetch($result)
    {
        return pg_fetch_assoc($result);
    }

    public function quote($val)
    {
        if(is_null($val))
        {
            return $val;
        }

        return "'" . pg_escape_string($this->link(), $val) . "'";
    }

    public function rowCount($result)
    {
        return pg_affected_rows($result);
    }
    
    /*blic function lastInsertId()
    {
        return pg_last_oid($this->link());
    }*/
}