<?php
class jqAgriculturalWorks extends jqGrid
{
    protected function init()
    {
        $this->nav = array(

            #Set common nav actions
            'add' => true,
            'edit' => true,
            'del' => true,
            'view' => true,

            #Set text labels. It's better to set them in defaults
            'addtext' => 'Add',
            'edittext' => 'Edit',
            'deltext' => 'Delete',
            'viewtext' => 'View',

            #Set common excel export
            'excel' => true,
            'exceltext' => 'Excel',

            #Set editing params
            'prmEdit' => array('width' => 400,
                'bottominfo' => 'Редактирование',
                'viewPagerButtons' => true), // отобразить/скрыть кнопки вперед/назад внизу окна редактирования
        );

        $this->table = 'data.d_agricultural_works_type';  
        

        $this->cols = array(

            'id_awt' => array('label' => 'ID',
                'width' => 10,
                'align' => 'center',
            ),

            'awt_name' => array('label' => 'Название',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => true),
            ),

            'awt_recomended_date_start' => array('label' => 'Рекомендуемая дата начала',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'awt_recomended_date_end' => array('label' => 'Рекомендуемая дата завершения',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'awt_description' => array('label' => 'Пояснения',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
        );
        
        #Add filter toolbar
        $this->render_filter_toolbar = true;
    }

}
?>
