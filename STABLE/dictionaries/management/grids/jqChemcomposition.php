<?php
class jqChemcomposition extends jqGrid
{
    protected function init()
    {
        $this->nav = array(

            #Set common nav actions
            'add' => true,
            'edit' => true,
            'del' => true,
            'view' => true,

            #Set text labels. It's better to set them in defaults
            'addtext' => 'Add',
            'edittext' => 'Edit',
            'deltext' => 'Delete',
            'viewtext' => 'View',

            #Set common excel export
            'excel' => true,
            'exceltext' => 'Excel',

            #Set editing params
            'prmEdit' => array('width' => 400,
                'bottominfo' => 'Редактирование',
                'viewPagerButtons' => true), // отобразить/скрыть кнопки вперед/назад внизу окна редактирования
        );

        $this->table = 'data.d_chemcomposition'; //'data.' . 
        

        $this->cols = array(

            'id_chemcomposition' => array('label' => 'ID',
                'width' => 10,
                'align' => 'center',
            ),

            'ch_name' => array('label' => 'Название',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => true),
            ),

            'ch_range_name' => array('label' => 'Диапазон значений',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
        );
        
        #Add filter toolbar
        $this->render_filter_toolbar = true;
    }

}
?>
