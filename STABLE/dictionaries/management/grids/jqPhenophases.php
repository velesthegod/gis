<?php
class jqPhenophases extends jqGrid
{
    protected function init()
    {
        $this->nav = array(

            #Set common nav actions
            'add' => true,
            'edit' => true,
            'del' => true,
            'view' => true,

            #Set text labels. It's better to set them in defaults
            'addtext' => 'Add',
            'edittext' => 'Edit',
            'deltext' => 'Delete',
            'viewtext' => 'View',

            #Set common excel export
            'excel' => true,
            'exceltext' => 'Excel',

            #Set editing params
            'prmEdit' => array('width' => 400,
                'bottominfo' => 'Редактирование',
                'viewPagerButtons' => true), // отобразить/скрыть кнопки вперед/назад внизу окна редактирования
        );

        $this->table = 'data.d_phenophases';  
        

        $this->cols = array(

            'id_pp' => array('label' => 'ID',
                'width' => 10,
                'align' => 'center',
            ),

            'pp_name' => array('label' => 'Название',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => true),
            ),

            'pp_date_start' => array('label' => 'Дата начала',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'pp_date_end' => array('label' => 'Дата завершения',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
            'pp_description' => array('label' => 'Пояснения',
                'width' => 35,
                'editable' => true,
                'editrules' => array('required' => false),
            ),
            
        );
        
        #Add filter toolbar
        $this->render_filter_toolbar = true;
    }

}
?>
