
$(document).ready(function(){
    
    var authorizationBox = $("#authorization-box");
    authorizationBox.hide();
    
    $(".menu-authorization").click(function(){
        authorizationBox.slideDown(300);
    });
    
    $("#authorization-cancel").click(function(){
        authorizationBox.slideUp(300);
    });
    
    
    
    $(".printMap").click(function(){
        var baselayer = mapObject.baseLayer();
        var currentzoom = mapObject.zoom();
        var centercoords = mapObject.center();
        var windowheight = $(window).height();
        var openedLayers = mapObject.layers();
        var layersArray = openedLayers.getArray();
        var URLString = "http://edu.ikit.sfu-kras.ru/print?";
        if ( layersArray.length > 0 ) {
            URLString += "layers=";
            var zpt = ",";
            for(var i=0; i<layersArray.length; i++) {
                //alert(layersArray[i]._model._pkValue);
                if ( i == layersArray.length-1 ) {
                    zpt = "";
                }
                URLString += layersArray[i]._model._pkValue + zpt;
            }
            URLString += "&";
        }
        URLString += "lat="+centercoords.lat+"&lon="+centercoords.lng+"&zoom="+currentzoom+"&baselayer="+baselayer.id();
        window.open(URLString, "a", "resizable=yes,scrollbars=yes,width=1003,height="+windowheight+",left=10px, top=100px");
    });
    
    //{"layers":[{"id":226,"filters":null,"geomFilter":null}],"lat":56.55077850339641,"lon":93.23822021484375,"zoom":10,"baseLayer":-1780345091}
    
    // расчитываем высоту панели
    var windowHeight = $(window).height();
    var userPanelHeight = windowHeight-150;
    var userPanelDiv = $("#user-panel");
    userPanelDiv.height(userPanelHeight);
    
    // Скрываем панель по умолчанию
    userPanelDiv.hide();
    $(".roll-user-panel").click(function(){
        
        if ( userPanelDiv.attr('class') == 'up-show' )
        {
            userPanelDiv.slideDown(500);
            userPanelDiv.attr('class', 'up-hide');
            $(this).css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
            $(".zoom-control").css({"left":zoomControlShift});
        }
        else
        {
            userPanelDiv.slideUp(500);
            userPanelDiv.attr('class', 'up-show');
            $(this).css({"opacity":"0.5", "-webkit-opacity":"0.5", "-moz-opacity":"0.5", "-o-opacity":"0.5"});
            $(".zoom-control").removeAttr("style");
        }
    });
    
    var modes = '<div class="tab-content">' +
                    '<div id="div-popup-form">' +
                        '<button type="button" onclick="ShowFarmers()" id="set-farm">Закрепить за хозяйством</button>' + 
                        '<button type="button" onclick="RemoveFieldFromFarmer()" id="unset-farm">Убрать из хозяйства</button>' + 
                        '<button type="button" onclick="ShowCultures()" id="set-cultures">Указать культуру</button>' + 
                    '</div>' +
                '</div>';
                
    $("div[data-name='Tab_2_name']").prepend(modes);
    
    // ОТОБРАЖАЕМ ОТЧЕТЫ.
    var reports = '<div class="tab-content">' +
                        '<div id="my-reports">' +
                            '<button type="button" class="report1">Справка о посевных площадях</button>' +
                            '<button type="button" class="report2">План посевных площадей</button>' +
                            '<button type="button" class="report3">План весенне-полевых работ</button>' +
                        '</div>' +
                  '</div>';
    $("div[data-name='Tab_3_name']").prepend(reports);
    
    var reportButton = $("button[class*='report']");
    var reportType;
    // ОТОБРАЖАЕМ МОДАЛЬНОЕ ОКНО С ОТЧЕТАМИ.
    reportButton.click(function(){
        reportType = $(this).attr("class");
        title = $(this).text();
        $('span[class="ui-dialog-title"]').text(title);
        $("#dialog-box").attr('title', title);
        //var send = "report="+reportType+"&idf="+"<?//=$_SESSION['idFarmer']?>";
        var send = "report="+reportType;
        var dialogHeight = $("#map").height() - 50;
    
        $.ajax({
            url:"Modules/Reports/View/reports-view.php",
            data:send,
            success:function(result){
                $("div[class='dialog-content']").replaceWith(result);
            }
        });
        
        $("#dialog-box").dialog({
            minWidth: 980,
            height: dialogHeight,
            position: ['top',50],
            resizable: false,
            draggable: true
        });
    });
    // ВРЕМЕННО: ПРОВЕРКА ЗАПУСКА СКРИПТА НА LINUX ИЗ WEB-ИНТЕРФЕЙСА
    $("#start-inhomogeneity-algoritm").click(function(){
        $.ajax({
            url:"/test.php",
            
            success:function(result){
                $("div[id='object-info-form']").replaceWith(result);
            }
        });
    });
    
    // ОТОБРАЖАЕМ ДИНАМИЧЕСКИЕ СЛОИ ХОЗЯЙСТВА.
    var dynamicLayers = '<form id="my-contours">' +
                            '<fieldset>' +
                                '<legend>Мои сельхоз контуры</legend>' +
                                '<div class="chbks">' +
                                    '<div>' +
                                        '<span class="dynamic-info" id="farmer">' +
                                            '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                        '</span>' +
                                        '<input type="checkbox" class="show-saved-layer" id="my-farmer" ><label for="my-farmer">Контуры хозяйства</label>' +
                                    '</div>' +
                                    '<div>' +
                                        '<span class="dynamic-info" id="plan">' +
                                            '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                        '</span>' +
                                        '<input type="checkbox" class="show-saved-layer" id="my-plan" ><label for="my-plan">Культуры плановые</label>' +
                                    '</div>' +
                                    '<div>' +
                                        '<span class="dynamic-info" id="fact">' +
                                            '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                        '</span>' +
                                        '<input type="checkbox" class="show-saved-layer" id="my-fact" ><label for="my-fact">Культуры фактические</label>' +
                                    '</div>' +
                                    /*'<div>' +
                                        '<span class="dynamic-info" id="ndvi-mean">' +
                                            '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                        '</span>' +
                                        '<input type="checkbox" class="show-saved-layer" id="my-ndvi-mean" ><label for="my-ndvi-mean">Среднее значение NDVI</label>' +
                                    '</div>' +*/
                                    '<div>' +
                                        '<span class="dynamic-info" id="ndvi-add">' +
                                            '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                        '</span>' +
                                        '<input type="checkbox" class="show-saved-layer" id="my-ndvi-add" ><label for="my-ndvi-add">История изменения растительности</label>' +
                                    '</div>' +
                                '</div>' +
                            '</fieldset>' +
                        '</form>';
    
    var subtabs = '<div class="ionTabs" id="subtabs_1" data-name="SubTabs_Group_name">' +
                        '<ul class="ionTabs__head">' +
                            '<li class="ionTabs__tab" data-target="SubTab_1_legend">Легенда</li>' +
                            '<li class="ionTabs__tab" data-target="SubTab_2_filter">Фильтры</li>' +
                        '</ul>' +
                        '<div class="ionTabs__body">' +
                            '<div class="ionTabs__item" data-name="SubTab_1_legend" style="padding: 20px 10px;"></div>' +
                            '<div class="ionTabs__item" data-name="SubTab_2_filter"><div id="layer-filter-form"></div></div>' +
                            '<div class="ionTabs__preloader"></div>' +
                        '</div>' +
                  '</div>';
    
    $("div[data-name='Tab_4_name']").prepend(dynamicLayers + subtabs);
    
    // ОТОБРАЖАЕМ ЛЕГЕНДУ ДИНАМИЧЕСКОГО СЛОЯ.
    $(".dynamic-info").click(function(){
        var id = this.id;
        // Включаем subtab с легендой
        $.ionTabs.setTab("SubTabs_Group_name", "SubTab_1_legend");
        // Удаляем предыдущую легенду
        $("#layer-legend").remove();
        if ( id == 'farmer' ) {
            //$("#my-contours").after('<form id="layer-legend"><fieldset><legend>Легенда</legend><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + farmerUniversalStyleName + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></fieldset></form>'); 
            $("div[data-name='SubTab_1_legend']").prepend('<div id="layer-legend" class="tab-content"><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + farmerUniversalStyleName + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></div>'); 
        }
        if ( id == 'plan' ) {
            //$("#my-contours").after('<form id="layer-legend"><fieldset><legend>Легенда</legend><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + culturesPlanUniversalStyleName + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></fieldset></form>'); 
            $("div[data-name='SubTab_1_legend']").prepend('<div id="layer-legend" class="tab-content"><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + culturesPlanUniversalStyleName + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></div>'); 
        }
        if ( id == 'fact' ) {
            //$("#my-contours").after('<form id="layer-legend"><fieldset><legend>Легенда</legend><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + culturesFactUniversalStyleName + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></fieldset></form>'); 
            $("div[data-name='SubTab_1_legend']").prepend('<div id="layer-legend" class="tab-content"><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + culturesFactUniversalStyleName + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></div>');
        }
        /*if ( id == 'ndvi-mean' ) {
            $("#my-contours").after('<form id="layer-legend"><fieldset><legend>Легенда</legend><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + NDVIUniversalStyle + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></fieldset></form>'); 
        }*/
		if ( id == 'ndvi-add' ) {
            //$("#my-contours").after('<form id="layer-legend"><fieldset><legend>Легенда</legend><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + NDVIUniversalStyle + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></fieldset></form>'); 
            $("div[data-name='SubTab_1_legend']").prepend('<div id="layer-legend" class="tab-content"><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + NDVIUniversalStyle + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></div>');
        }
    });
    
    // ОТОБРАЖАЕМ КАРТИНКУ ПЕРЕЛЕТА К СЛОЮ ПРИ КЛИКЕ НА CHECKBOX ДИНАМИЧЕСКОГО СЛОЯ.
    $('.chbks input[type="checkbox"]').click(function(){
        var id = this.id;
        if ( document.getElementById(id).checked == true ) {
            $(this).after('<span id="fly" class="fly fly-to-' + id + '"><img src="public/images/fly-to-layer.png" alt="Перелет к слою"></span>');
            // ВЫПОЛНЯЕМ ПЕРЕЛЕТ К СЛОЮ ПРИ КЛИКЕ НА КАРТИНКУ ПЕРЕЛЕТА.
            $('.fly-to-' + id).click(function(){
                // Позиционируем карту.
                if ( _minLat!=0 && _minLng!=0 && _maxLat!=0 && _maxLng!=0 ) {
                    mapObject.fitBounds(_minLng, _minLat, _maxLng, _maxLat);
                }
            }); 
        }
        else {
            $(".fly-to-"+id).remove();
        }
    }); 
    
    // ОТОБРАЖАЕМ ДИНАМИЧЕСКИЕ СЛОИ НА КАРТЕ ПРИ КЛИКЕ НА СООТВЕТСТВУЩИЙ CHECKBOX
    $("#my-farmer").click(function(){
        ShowMyLayer('farmer', this);
    });
    $("#my-plan").click(function(){
        ShowMyLayer('plan', this);
    });
    $("#my-fact").click(function(){
        ShowMyLayer('fact', this);
    });
    /*$("#my-ndvi-mean").click(function(){
        ShowMyLayer('ndvi-mean', this);
    });*/
    $("#my-ndvi-add").click(function(){
		NDVI.ShowNdviGeojsonLayer(this/*, '2016-05-16'*/);
    });

    // ИНИЦИАЛИЗИРУЕМ ИНТЕРФЕЙС TABS. ПРИМЕР: http://ionden.com/a/plugins/ion.tabs/#tabs|Product:Info|Company:History
    $.ionTabs("#tabs_1, #subtabs_1", {
        type: "storage",                    // hash, storage или none
        onChange: function(obj){         // функция обратного вызова
            //console.log(obj);
        }
    });
    // ИНИЦИАЛИЗИРУЕМ ИНТЕРФЕЙС TABS для SUBTUBS. ПРИМЕР: http://ionden.com/a/plugins/ion.tabs/#tabs|Product:Info|Company:History
    //$.ionTabs("#subtabs_1");
    /*$.ionTabs("#subtabs_1", {
        type: "storage",                    // hash, storage или none
        onChange: function(obj){         // функция обратного вызова
            console.log(obj);
        }
    });*/
    
    // ПРИ ЗМЕНЕНИИ ВЫСОТЫ ОКНА БРАУЗЕРА ПЕРЕСЧИТЫВАЕМ ВЫСОТУ ПРАВОЙ ПАНЕЛИ С ОБЩИМИ СЛОЯМИ.
    $(window).resize( function(){
        // Высота правой панели.
        var groupsHeight = $("#map").height() - 150;
        $("#groups").height(groupsHeight);
        // groups_count - глобальная переменная
        $(".ui-accordion-content").height(groupsHeight - groups_count);
        
        // Высота левой панели.
        var windowHeight = $(window).height();
        var userPanelHeight = windowHeight-150;
        var userPanelDiv = $("#user-panel");
        userPanelDiv.height(userPanelHeight);
    } );
    
    //----------------------------НАЧАЛО-----------------------------------------
    //-----------------КАСИКОВ А.О. грКИ13-15Б-----------------------------------
    //---------------------------------------------------------------------------
    
    // Сразу добавляем кнопки, чтобы обработчик на них сработал 
    $("body").append('<div class="adding_button"><input id="printButton" class="printMode" value="Печать" type="submit"></input></div>');
    $("body").append('<div class="adding_button"><input id="exitMode" class="printMode" value="Выйти из режима печати" type="submit"></input></div>');
    
    // Изначально делаем их невидимыми
    $("#printButton").css("display", "none");
    $("#exitMode").css("display", "none");
    
    var choose = true;
   
    $(".printMode").on("click", function(){
        if(choose == true){ 
            $("#print-mode-hidden").val(1);
            $("head").append('<link rel="stylesheet" type="text/css" href="public/css/print.css">');
            $("body").append('<div class="textarea_class"><textarea placeholder="Комментарии:" id="comments" rows="6" name="comment"></textarea></div>');
            
            // Делаем кнопки видимыми
            $("#printButton").css("display", "block");
            $("#exitMode").css("display", "block");
            
            $("#printButton").css("color", "black");
            $("#exitMode").css("color", "black");
            
            // Убираем во время печати колонку слева (Хозяйства)
            $("#user-panel").css("display", "none");
            $("#user-panel").css("height", "0px");
            $(".zoom-control").css("left", "10px");
            
            choose = false;
        }
    });
    
    // Печать и выход из режима печати
    $("input#printButton").on("click", function(){           
        $("#printButton").css("display", "none");
        $("#exitMode").css("display", "none");
        
        $("#comments").attr("placeholder", "");
        
        window.print();
        
        $('link[href~="public/css/print.css"]').remove();
        $('textarea[id=comments]').remove();
        
        // Восстанавливаем прежний стиль для левой колонки (Хозяйства)
        $("#print-mode-hidden").val(0);
        $("#user-panel").css("display", "block");
        $("#user-panel").height(userPanelHeight);
        choose = true;
    });
    
    // Выход из режима печати
    $("input#exitMode").on("click", function(){           
        $('link[href~="public/css/print.css"]').remove();
        $('textarea[id=comments]').remove();
        
        $("#printButton").css("display", "none");
        $("#exitMode").css("display", "none");
        
        // Восстанавливаем прежний стиль для левой колонки (Хозяйства)
        $("#print-mode-hidden").val(0);
        $("#user-panel").css("display", "block");
        $("#user-panel").height(userPanelHeight);;
        
        choose = true;
    });
    
    //---------------------------------------------------------------------------
    //-----------------КАСИКОВ А.О. грКИ13-15Б-----------------------------------
    //------------------------------КОНЕЦ----------------------------------------
});
