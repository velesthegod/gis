
$(document).ready(function(){
    
    $('#user-panel').on('click', 'button.button-show', function(){
        var attrJson = Filter.FormToJSON($(Filter.selectors['input']));
        Filter.ExecuteCQL(attrJson);
    });
    
    $('#user-panel').on('click', 'button.button-clear', function(){
        var attrJson = Filter.FormToJSON($(Filter.selectors['input']));
        Filter.ClearForm(attrJson);
    });
    
    $('#user-panel').on('click', 'button.button-boundary', function(){
        RectangleDraw._activateControl();
    });
    
});