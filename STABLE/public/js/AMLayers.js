var AMLayers = {
    
    _getLayer: function(layersStore, layerID) {
        var currentLayer = layersStore[layerID];
        return currentLayer;
    },
    
    _isRaster: function(style){
        var isRaster;
        if ( style == "raster" ) {
            isRaster = true;
        }
        else {
            isRaster = false;
        }
        return isRaster;
    },
    
    reload: function(layer) {
        mapObject.removeLayer(layer);
        layer._layersForMaps.map[20].wmsParams.random = Math.random();
        var reloadLayer = layer;
        mapObject.addLayer(reloadLayer);
    },
};