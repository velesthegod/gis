/**
* ВЫВОД GEOJSON СЛОЕВ ДЛЯ ИСТОРИИ ИЗМЕНЕНИЯ NDVI
*/

var NDVI = {
    
    ndviJsonArray: new Array(),
    
    /**
    * @description Метод _setStyle() стилизация объектов Geojson слоя NDVI.
    * @returns void
    */
    _setStyle: function(ndvi)
    {
        if ( role != adminRole ) {
            for (var l=0; l<ndvi.length; l++)
            {
                if ( ndvi[l].properties.id_owner == idFarmer ) {
                    
                    var layerNDVIAdd = new GeoPortal.Layer.GeoJSON(ndvi[l]);
                    
                    if ( ndvi[l].properties.ndvi_value > 0 && ndvi[l].properties.ndvi_value <= 0.1 ) {
                        layerNDVIAdd.setStyle({fillColor :'#92723F', fillOpacity: 1, opacity: 0});
                    }
                    if ( ndvi[l].properties.ndvi_value > 0.1 && ndvi[l].properties.ndvi_value <= 0.26 ) {
                        layerNDVIAdd.setStyle({fillColor :'#94B614', fillOpacity: 1, opacity: 0});
                    }
                    if ( ndvi[l].properties.ndvi_value > 0.26 && ndvi[l].properties.ndvi_value <= 0.43 ) {
                        layerNDVIAdd.setStyle({fillColor :'#63A104', fillOpacity: 1, opacity: 0});
                    }
                    if ( ndvi[l].properties.ndvi_value > 0.43 && ndvi[l].properties.ndvi_value <= 0.57 ) {
                        layerNDVIAdd.setStyle({fillColor :'#3C8604', fillOpacity: 1, opacity: 0});
                    }
                    if ( ndvi[l].properties.ndvi_value > 0.57 && ndvi[l].properties.ndvi_value <= 0.65 ) {
                        layerNDVIAdd.setStyle({fillColor :'#045F04', fillOpacity: 1, opacity: 0});
                    }
                    if ( ndvi[l].properties.ndvi_value > 0.65 && ndvi[l].properties.ndvi_value <= 1 ) {
                        layerNDVIAdd.setStyle({fillColor :'#043A04', fillOpacity: 1, opacity: 0});
                    }
                
                    NDVI.ndviJsonArray.push(layerNDVIAdd);
                    mapObject.addLayer(layerNDVIAdd);
                }
            }
        }
        else {
            for (var l=0; l<ndvi.length; l++)
            {
                var layerNDVIAdd = new GeoPortal.Layer.GeoJSON(ndvi[l]);
                
                if ( ndvi[l].properties.ndvi_value > 0 && ndvi[l].properties.ndvi_value <= 0.1 ) {
                    layerNDVIAdd.setStyle({fillColor :'#92723F', fillOpacity: 1, opacity: 0});
                }
                if ( ndvi[l].properties.ndvi_value > 0.1 && ndvi[l].properties.ndvi_value <= 0.26 ) {
                    layerNDVIAdd.setStyle({fillColor :'#94B614', fillOpacity: 1, opacity: 0});
                }
                if ( ndvi[l].properties.ndvi_value > 0.26 && ndvi[l].properties.ndvi_value <= 0.43 ) {
                    layerNDVIAdd.setStyle({fillColor :'#63A104', fillOpacity: 1, opacity: 0});
                }
                if ( ndvi[l].properties.ndvi_value > 0.43 && ndvi[l].properties.ndvi_value <= 0.57 ) {
                    layerNDVIAdd.setStyle({fillColor :'#3C8604', fillOpacity: 1, opacity: 0});
                }
                if ( ndvi[l].properties.ndvi_value > 0.57 && ndvi[l].properties.ndvi_value <= 0.65 ) {
                    layerNDVIAdd.setStyle({fillColor :'#045F04', fillOpacity: 1, opacity: 0});
                }
                if ( ndvi[l].properties.ndvi_value > 0.65 && ndvi[l].properties.ndvi_value <= 1 ) {
                    layerNDVIAdd.setStyle({fillColor :'#043A04', fillOpacity: 1, opacity: 0});
                }
            
                NDVI.ndviJsonArray.push(layerNDVIAdd);
                mapObject.addLayer(layerNDVIAdd);
            }
        }
    },

    /**
    * @description Метод ShowNdviGeojsonLayer() отобразить geojson слой NDVI.
    * @param element Object (html object checkbox)
    * @returns void
    */
    ShowNdviGeojsonLayer: function(element/*, ndvidate*/)
    {
        var currentElementId = element.id;

        if ( document.getElementById(currentElementId).checked == true )
        {
            var datesArray = new Array();
            var queryStatus = 1;
            var send = "ndvidate="+queryStatus;
            // Make query to select all NDVI dates from DB
            $.ajax({
                url:"Modules/Popup/View/popup-view.php",
                data:send,
                method: 'POST',
                success:function(result){
                    // Convert JSON format to objects array
                    var datesObj = JSON.parse(result);
                    // Convert objects array to dates array
                    for ( var i=0; i<datesObj.length; i++ ) {
                        datesArray.push(datesObj[i].ndvi_year);
                    }
                    
                    var currentDate = datesArray[datesArray.length - 1];
                   
                    NDVI._loadSlider(datesArray, currentDate);
                                      
                    // Show by default the last NDVI layer
                    NDVI._loadGeojson(datesArray[datesArray.length - 1]);
                    // Show label of current position
                    $("#slider span#date"+currentDate).css("display","inline-block");    
                },
                error: function() {
                    console.log('Error of get NDVI dates');
                }
            });
        }
        else
        {
            for (var i=0; i<NDVI.ndviJsonArray.length; i++)
            {
                mapObject.removeLayer(NDVI.ndviJsonArray[i]);
            }
            NDVI.ndviJsonArray = [];
            $("#slider-drag-box").remove();
        }
    },
    
    _loadGeojson: function(date){
        /*$.getScript( "public/geojson-layers/ndvi"+datesArray[ui.value]+".geojson", function( data, textStatus, jqxhr ) {});*/
        var disablingFrame = '<div id="wait-for-load"></div>';
        
        var circularG = '<div id="cssload-wrapper">' +
                            '<div id="cssload-border">' +
                            '<div id="cssload-whitespace">' +
                            '<div id="cssload-line">' +
                        '</div></div></div></div>';
        $.when(
            $.ajax({ 
                type: "GET", 
                url: "public/geojson-layers/ndvi"+date+".geojson", 
                dataType: "script", 
                cache: true,
                success: function(data, textStatus, jqxhr){
                    NDVI._setStyle(ndvi);
                }
            }),
            
            $("#slider").after(disablingFrame),
            
            $("#wait-for-load").after(circularG)
            
        ).done(function(){
            $("#wait-for-load").remove();
            $("#cssload-wrapper").remove();
        });
    },
    
    /**
    * @description Метод _loadSlider() создать и загрузить элемент slider.
    * @param labelsArray Array (массив меток на шкале)
    * @param currentLabel String (активная по умолчанию метка)
    */
    _loadSlider: function(labelsArray, currentLabel){
        var slider = '<div id="slider-drag-box">';
        slider += '<div class="slider-canvas">';
        slider += '<div class="arrow-left"></div>';
        slider += '<div id="slider" style="width: 320px;"></div>';
        slider += '<div class="arrow-right"></div>';
        slider += '</div></div>';
        
        // Show slider
        $(".wrap").after(slider);
        
        var sliderArLeft = $("#slider-drag-box .slider-canvas div.arrow-left");
        var sliderArRight = $("#slider-drag-box .slider-canvas div.arrow-right");
        
        var slider = $("#slider").slider({
            value: labelsArray.length-1,
            min: 0,
            max: labelsArray.length-1,
            step: 1,
            change: function(event, ui){
                if ( $(this).slider("value") == $(this).slider("option", "min") ) {
                    sliderArLeft.css("background-image", "url(/public/images/ar-left-gray.png)");
                    sliderArRight.css("background-image", "url(/public/images/ar-right-white.png)");
                }
                if ( $(this).slider("value") == $(this).slider("option", "max") ) {
                    sliderArRight.css("background-image", "url(/public/images/ar-right-gray.png)");
                    sliderArLeft.css("background-image", "url(/public/images/ar-left-white.png)");
                } else {
                    sliderArLeft.css("background-image", "url(/public/images/ar-left-white.png)");
                    sliderArRight.css("background-image", "url(/public/images/ar-right-white.png)");
                }
                
                // Unload already loaded geojson layer, when switching the slider
                if ( NDVI.ndviJsonArray.length > 0 ) 
                {
                    for (var i=0; i<NDVI.ndviJsonArray.length; i++)
                    {
                        mapObject.removeLayer(NDVI.ndviJsonArray[i]);
                    }
                    NDVI.ndviJsonArray = [];
                }
                
                // Hide label of previos position
                $("#slider span#date"+currentLabel).css("display","none");
                currentLabel = labelsArray[ui.value];
                // Show label of current position
                $("#slider span#date"+labelsArray[ui.value]).css("display","inline-block");
                
                // Get geojson layer
                NDVI._loadGeojson(labelsArray[ui.value]);
            }
        }).each(function(){
            //var opt = $(this).data().uiSlider.options;
            //var vals = opt.max - opt.min;
            
            $("#slider").attr("class", "ui-slider ui-slider-horizontal ui-widget ui-slider-content ui-corner-all");
            $("#slider a").removeAttr("href");
            $("#slider a").attr("class", "ui-slider-state-handle ui-slider-state-default");
            
            // Space out values
            for (var i = 0; i < labelsArray.length; i++) {
                var el2 = $('<span id="date'+labelsArray[i]+'" class="label">'+(labelsArray[i])+'</span>').css('left',(i/(labelsArray.length-1)*100)-3+'%');
                var el = $('<div id="date'+labelsArray[i]+'" class="scale"></div>').css({'left': (i/(labelsArray.length-1)*100)+'%'});
                $( "#slider" ).append(el);
                $( "#slider" ).append(el2);
            }
            
            $(".scale").hover(
                function(){
                    $("#slider span#"+this.id).css("display","inline-block");
                }, 
                function(){
                    $("#slider span#"+this.id).css("display","none");
            
            });
           
            
            // Обрабатываем нажатие на стрелку влево
            sliderArLeft.click(function(){
                var sv = slider.slider("value");
                var sv_min = slider.slider("option", "min");
                if ( sv != sv_min )
                {
                    sliderArRight.css("background-image", "url(/public/images/ar-right-white.png)");
                    slider.slider("value", sv - 1);
                }
                else
                {
                    $(this).css("background-image", "url(/public/images/ar-left-gray.png)");
                }
            });
            
            // Обрабатываем нажатие на стрелку вправо
            sliderArRight.click(function(){
                var sv = slider.slider("value");
                var sv_max = slider.slider("option", "max");
                if ( sv != sv_max )
                {
                    sliderArLeft.css("background-image", "url(/public/images/ar-left-white.png)");
                    slider.slider("value", sv + 1);
                }
                else
                {
                    $(this).css("background-image", "url(/public/images/ar-right-gray.png)");
                }
            });
           
            // Делаем элемент доступным для перемещения
            $("#slider-drag-box").draggable();
            
            // Обрабатываем наведение указателя на слайдер
            $("#slider-drag-box").hover(function(){
                $(this).css({"background": "rgba(48, 57, 62, 0.75)"});
                $("div[id^='date']").css('visibility', 'visible');
                $("#slider").css({'border': '1px solid #ffffff', 'background': 'linear-gradient(to bottom, #8499b5, #456A7F)'});
                $("#slider .ui-slider-state-handle").css({'border': '1px solid #ffffff', 'background': '#ededed'});
                sliderArLeft.css('visibility', 'visible');
                sliderArRight.css('visibility', 'visible');
            }, function(){
                $(this).css({"background": "rgba(48, 57, 62, 0)"});
                $("div[id^='date']").css('visibility', 'hidden');
                $("#slider").css({'border': '1px solid #ffffff', 'background': 'rgba(255, 255, 255, 0)'});
                $("#slider .ui-slider-state-handle").css({'border': '1px solid #ffffff', 'background': 'rgba(255, 255, 255, 0)'});
                sliderArLeft.css('visibility', 'hidden');
                sliderArRight.css('visibility', 'hidden');
            });
        });
    }
};
