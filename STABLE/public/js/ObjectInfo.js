var ObjectInfo = {
    
    _layer: new Object(),
    _features: new Object(),
    _geom: new Object(),
    _layerSelector: null,
    _layerEditor: null,
    _userPanel: null,
    _rollUserPanel: null,
    
    FormToJSON: function(elems) {
        var inputElems = elems;
        var attrJson = [];
        
        for (var i=0; i<inputElems.length; i++)
        {
            attrJson[i] = {
                "name": inputElems[i]['name'],
                "value": inputElems[i]['value'],
                "id": inputElems[i]['id'],
            };
        }
        return attrJson;
    },
    
    // Создаем массив координат, в котором только дырки в полигоне
    holesCoordinates: function([arr]) {
        var del = arr.splice(0,1);
        return arr; 
    },
    
    getStoredLayers: function(mapObject) {
        // получаем все открытые слои,
        var openedLayers = mapObject.layers();
        // преобразуем слои в массив,
        var layersArray = openedLayers.getArray();
        
        var storedLayers = [];
        
        if ( layersArray.length > 0 ) 
        {
            for( var i=0; i<layersArray.length; i++ ) 
            {
                // ЭТО УСЛОВИЕ НЕОБХОДИМО, ЧТОБЫ ОТОБРАЖАЛАСЬ И ОБНОВЛЯЛАСЬ ИНФОРМАЦИЯ О КАЖДОМ ИЗ ВКЛЮЧЕНЫЫХ КОНТУРОВ ОБЪЕКТОВ, 
                // Т.К. layersArray СОДЕРЖИТ НЕ ТОЛЬКО ОСНОВНЫЕ ВКЛЮЧЕННЫЕ СЛОИ, НО И КОНТУРЫ ДОБАВЛЕННЫХ ОБЪЕКТОВ ПОНИМАЕТ КАК СЛОИ. НО ЭТИ КОНТУРЫ НЕ ИМЕЮТ СВОЙСТВА _model И У НИХ НЕТ ID, ЧТО МОЖЕТ ПРИВЕСТИ К ОШИБКЕ.
                if ( "_model" in layersArray[i] )
                {
                    storedLayers[i] = layersArray[i];
                }
            }
        }
        
        return storedLayers;
    },
    
    _selected: function(mapObject, layersStore, features) {
        var layersArray = this.getStoredLayers(mapObject);
        // формируем HTML, в котором будет выводиться информация о объекте
        var objectInfoHTML = '';
            
        objectInfoHTML += '<div id="object-info-form">';
            
        var count = layersArray.length;
        
        for( var i=0; i<layersArray.length; i++ ) 
        {
            // Получаем ID открытых слоев.
            var currentLayerID = layersArray[i]._model._pkValue;
            // Получаем поля БД каждого из открытых слоев.
            var currentLayerFields = layersStore[currentLayerID].fields();
            
            if ( typeof features[i] != 'undefined' ) {
                // ID выделенного объекта
                var fieldId = features[i]._model._values.data['fid'];
                
                objectInfoHTML += '<div id="object-info">';
            
                if ( role == adminRole && count == 1 ) {
                    // Добавляем кнопку изменить контур
                    objectInfoHTML += '<span class="but-change"><button type="button" layerid="'+currentLayerID+'" fieldid="'+fieldId+'" class="changek object-change" id="object-change"></button></span>';
                    // Добавляем кнопку удалить контур и всю информацию о нем
                    objectInfoHTML += '<span class="but-delete"><button type="button" layerid="'+currentLayerID+'" fieldid="'+fieldId+'" class="object-delete" id="object-delete"></button></span>';
                    // Корректируем отображение списка атрибутов объекта
                    objectInfoHTML += '<div class="object-attribs">';
                }
                
                if ( count == 1 ) {
                    objectInfoHTML += '<div class="object-attribs-admin">';
                } else {
                    objectInfoHTML += '<div class="object-attribs-admin">';
                }
                
                objectInfoHTML += '<p style="text-decoration:underline; color:#2C87D2; font-size:12pt; font-weight:normal; letter-spacing:0.5px; cursor:pointer;"><b>' + layersStore[currentLayerID].rusName() + '</b></p>';
                
                for ( var j=0; j<currentLayerFields.length; j++ )
                {
                    if ( currentLayerFields[j].nameRu != '' )
                    {
                        // Получаем имя свойства на латинице.
                        var nameLat = currentLayerFields[j].name;

                        if ( features[i]._model._values.data[nameLat] != undefined )
                        {
                            objectInfoHTML += '<p id="'+nameLat+'" style="font-size:9pt;"><b>' + currentLayerFields[j].nameRu + ':</b> ';
                            objectInfoHTML += '' + features[i]._model._values.data[nameLat] + '</p>';
                        }
                    }
                }
                
                objectInfoHTML += '</div>';
                
                if ( currentLayerID == workingLayerId ) {
                    // Глобальная переменная, используется в activemap-manager.js
                    // __gidNDVI = features[i]._model._values.data['__gid']; // Значение поля __gid, а нужно gid, что равно fid.
                    gid = fieldId;
                    objectInfoHTML += this._ndviform();
                }
                
                objectInfoHTML += '</div>';
            }
            // проверочная кнопка
            //objectInfoHTML += '<br><br><br><button type="button" layerid="'+currentLayerID+'" fieldid="'+attribsArray['fid']+'" class="temp" id="temp">Проверка</button>';
        }
        objectInfoHTML += '</div>';
                
        return objectInfoHTML;
    },
    
    InfoForm: function() {
        
        // формируем HTML, в котором будет выводиться информация о объекте
        var objectInfoHTML = '';
        
        objectInfoHTML += '<div id="object-info-form">';

        // Получаем поля БД каждого из открытых слоев.
        var currentLayerFields = this._layer.fields();
        var nameRu = this._layer.rusName()
        var layerID = this._layer._model._pkValue;
        // ID выделенного объекта
        var fieldId = this._features[0]._model._values.data['fid'];
        
        objectInfoHTML += '<div id="object-info">';
                    
        if ( role == adminRole ) {
            // Добавляем кнопку изменить контур
            objectInfoHTML += '<span class="but-change"><button type="button" layerid="'+layerID+'" fieldid="'+fieldId+'" class="changek object-change" id="object-change"></button></span>';
            // Добавляем кнопку изменить удалить контур и всю информацию о нем
            objectInfoHTML += '<span class="but-delete"><button type="button" layerid="'+layerID+'" fieldid="'+fieldId+'" class="object-delete" id="object-delete"></button></span>';
        }
        
        objectInfoHTML += '<div class="object-attribs">';
        objectInfoHTML += '<p style="text-decoration:underline; color:#2C87D2; font-size:12pt; font-weight:normal; letter-spacing:0.5px; cursor:pointer;"><b>' + nameRu + '</b></p>';
        
        for ( var j=0; j<currentLayerFields.length; j++ )
        {
            if ( currentLayerFields[j].nameRu != '' )
            {
                // Получаем имя свойства на латинице.
                var nameLat = currentLayerFields[j].name;
                
                if ( this._features[0]._model._values.data[nameLat] != undefined )
                {
                    objectInfoHTML += '<p id="'+nameLat+'" style="font-size:9pt;"><b>' + currentLayerFields[j].nameRu + ':</b> ';
                    objectInfoHTML += '' + this._features[0]._model._values.data[nameLat] + '</p>';
                }
            }
        }
        
        objectInfoHTML += '</div>';
                    
        if ( layerID == workingLayerId ) {
            objectInfoHTML += this._ndviform();
        }
        objectInfoHTML += '</div>';
        
        objectInfoHTML += '</div>';
                
        return objectInfoHTML;
    },
    
    EditForm: function() {
        // формируем HTML, в котором будет выводиться информация о объекте
        var objectInfoHTML = '';
        
        objectInfoHTML += '<div id="object-info-form" style="margin-bottom:30px;">';

        // Получаем поля БД каждого из открытых слоев.
        var currentLayerFields = this._layer.fields();
        var nameRu = this._layer.rusName();
        var layerID = this._layer._model._pkValue;
        objectInfoHTML += '<p style="text-decoration:underline; color:#2C87D2; font-size:12pt; font-weight:normal; letter-spacing:0.5px; cursor:pointer;"><b>' + nameRu + '</b></p>';
        
        for ( var j=0; j<currentLayerFields.length; j++ )
        {
            if ( currentLayerFields[j].nameRu != '' )
            {
                // Получаем имя свойства на латинице.
                var nameLat = currentLayerFields[j].name;
                var fieldId = currentLayerFields[j].id;
                // TODO:
                if ( this._features[0]._model._values.data[nameLat] != undefined )
                {
                    objectInfoHTML += '<p style="font-size:9pt;"><b>' + currentLayerFields[j].nameRu + ':</b></p> ';
                    objectInfoHTML += '<input type="text" name="'+nameLat+'" id="'+fieldId+'" value="' + this._features[0]._model._values.data[nameLat] + '" />';
                } else {
                    this._features[0]._model._values.data[nameLat] = '';
                    objectInfoHTML += '<p style="font-size:9pt;"><b>' + currentLayerFields[j].nameRu + ':</b></p> ';
                    objectInfoHTML += '<input type="text" name="'+nameLat+'" id="'+fieldId+'" value="' + this._features[0]._model._values.data[nameLat] + '" />';
                }
            }
        }
        // ID редактируемого объекта
        var fieldId = this._features[0]._model._values.data['fid'];
        
        objectInfoHTML += '<p style="margin:20px 0;">';
        // Добавляем кнопку отмены редактирования контура
        objectInfoHTML += '<button type="button" layerid="'+layerID+'" fieldid="'+fieldId+'" class="change-object-cancel" id="change-object-cancel">Отменить</button >';
        // Добавляем кнопку для сохранения редактирования
        objectInfoHTML += '<button type="button" layerid="'+layerID+'" fieldid="'+fieldId+'" class="change-object-save" id="change-object-save">Сохранить</button>';
        objectInfoHTML += '</p>';
        
        objectInfoHTML += '</div>';
                
        return objectInfoHTML;
    },
    
    CreateForm: function() {
        // формируем HTML, в котором будет выводиться информация о объекте
        var objectInfoHTML = '';
        
        objectInfoHTML += '<div id="object-info-form" style="margin-bottom:30px;">';
        
        var layerFields = this._layer.fields();
        var layerID = this._layer._model._pkValue;
        
        objectInfoHTML += '<p style="text-decoration:underline; color:#2C87D2; font-size:12pt; font-weight:normal; letter-spacing:0.5px; cursor:pointer;"><b>' + this._layer.rusName() + '</b></p>';
        
        for ( var j=0; j<layerFields.length; j++ )
        {
            if ( layerFields[j].nameRu != '' )
            {
                // Получаем имя свойства на латинице.
                var nameLat = layerFields[j].name;
                var fieldId = layerFields[j].id;
                
                objectInfoHTML += '<p style="font-size:9pt;"><b>' + layerFields[j].nameRu + ':</b></p> ';
                objectInfoHTML += '<input type="text" name="'+nameLat+'" id="'+fieldId+'" value="" />';
            }
        }
        
        objectInfoHTML += '<p style="margin:20px 0;">';
        // Добавляем кнопку отмены создания контура
        objectInfoHTML += '<button type="button" layerid="'+layerID+'" class="create-object-cancel" id="create-object-cancel">Отменить</button >';
        // Добавляем кнопку для сохранения нового контура
        objectInfoHTML += '<button type="button" layerid="'+layerID+'" class="create-object-save" id="create-object-save">Сохранить</button>';
        objectInfoHTML += '</p>';
        
        objectInfoHTML += '</div>';
                
        return objectInfoHTML;
    },
    
    _ndviform: function(){
        // ДОБАВЛЯЕМ ПРОСМОТР СТАТИСТИКИ NDVI НА КОНТУР
        var objectInfoHTML = '';
        objectInfoHTML += '<form id="field-state">';
        objectInfoHTML += '<fieldset>';
        objectInfoHTML += '<legend>Оценка состояния поля</legend>';
        objectInfoHTML += '<div class="chbks">';
        objectInfoHTML += '<input type="checkbox" class="ndvi-src" value="Landsat-8" id="Landsat-8"><label for="Landsat-8">Данные Landsat-8</label>';
        objectInfoHTML += '<input type="checkbox" class="ndvi-src" value="Sentinel-2A" id="Sentinel-2A"><label for="Sentinel-2A">Данные Sentinel-2A</label>';
        objectInfoHTML += '<input type="checkbox" class="ndvi-src" value="Vega-PRO" id="Vega-PRO"><label for="Vega-PRO">Данные сервиса Vega-PRO</label>';
        objectInfoHTML += '</div>';
        objectInfoHTML += '<select class="select-ndvi-year" multiple="multiple" size="5" style="width:250px;">';
        for (i = 2000; i<=currentYear; i++){
            objectInfoHTML += '<option value="' + i + '">' + i + '</option>';
        }
        objectInfoHTML += '</select>';
        objectInfoHTML += '<button type="button" class="ndvi-button" onclick="ShowNDVIStats()" id="ndvi-show">Динамика вегетации</button>';
        objectInfoHTML += '</fieldset>';
        objectInfoHTML += '</form>';
        
        return objectInfoHTML;
    },
    
    Save: function(saveObject) {
        var url = 'Modules/Object/View/object-view.php';
        $.ajax({
            url:url,
            method:'POST',
            data:saveObject,
            headers:[{
                'Access-Control-Request-Headers': 'X-Requested-With'
            }],
            xhrFields: {
                withCredentials:true
            },
            success:function(data){
                // debug
                // $("#user-panel #object-info-form").prepend(data);
                
                var userPanelDiv = $("#user-panel"), 
                    rollUserPanelDiv = $(".roll-user-panel");
                
                if ( ObjectInfo._layerSelector != null ) {
                    //console.log('Слой селектор существует');
                    //console.log(ObjectInfo._layerSelector);
                    mapObject.removeLayer(ObjectInfo._layerSelector);
                }
                if ( ObjectInfo._layerEditor != null ) {
                    //console.log('Слой редактор существует');
                    //console.log(ObjectInfo._layerEditor);
                    mapObject.removeLayer(ObjectInfo._layerEditor);
                }
                
                $('#user-panel #object-info-form').remove();

                userPanelDiv.slideUp(500);
                userPanelDiv.attr('class', 'up-show');
                rollUserPanelDiv.css({"opacity":"0.5", "-webkit-opacity":"0.5", "-moz-opacity":"0.5", "-o-opacity":"0.5"});
                $(".zoom-control").removeAttr("style");
                
                if ( saveObject['_action'] == 1 ) {
                    $.stickr({note:'<p>Изменения для объекта <b>[№='+ saveObject['fieldid'] + ']</b> сохранены.</p>',className:'sticker sticker-success', time:3000, speed:2000, position:{right:320,top:98}});
                }
                if ( saveObject['_action'] == 2 ) {
                    $.stickr({note:'<p>Объект слоя <b>[№='+ saveObject['layerid'] + ']</b> создан.</p>',className:'sticker sticker-success', time:3000, speed:2000, position:{right:320,top:98}});
                }
                if ( saveObject['_action'] == 3 ) {
                    $.stickr({note:'<p>Объект <b>[№='+ saveObject['fieldid'] + ']</b> удален.</p>',className:'sticker sticker-success', time:3000, speed:2000, position:{right:320,top:98}});
                }
            },
            error:function(xhr, textStatus, errorTrown){
                console.log(xhr);
                $.stickr({note:'<p>Действие не выполнено.</p><p>Ошибка' + xhr + '</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
            },
            beforeSend: function(jqXHR, settings){}
        });
    }
};