<?php
class DB 
{
    /**
    * Основные настройки подключения к серверу БД
    */
    public $HOST = '10.3.3.208';

    public $PORT = 5432;
    
    public $USER = 'student';

    public $PASSWORD = 'EdUcAtIOn~123';

    /**
    * Таблицы БД, используемые в программе:
    */
    // Таблица основного слоя полей
    public $FIELDS_TABLE    = 'contours_zshn_suchobuzimskoe_2015';
    // Таблица справочника культур
    public $CULTURES_TABLE  = 'd_cultures';
    // Таблица справочника хозяйств
    public $FARMERS_TABLE   = 'd_farmers';
    // Таблица истории ввода плановых и фактических культур
    public $PLAN_FACT_TABLE = 'plan_fact';
    // Таблица bbox для слоев хозяйств
    public $LAYER_BBOX_TABLE = 'layer_bbox';
    // Таблица связи хозяйства с полями
    public $FARMERS_FIELDS_TABLE = 'farmers_fields';
	// Таблица логов.
    public $A_LOGS_TABLE = "a_logs";
    // Таблица статистики NDVI.
    public $A_NDVI_STATS = "a_ndvi_stats";
    
    public function _construct(){}
    
    /**
    * @description PG_GetConnect() - создает соединение с СУБД PostgreSQL
    * @param $db_name string - система предусматривает несколько БД
    * @return resource
    */
    public function PG_GetConnect($db_name)
    {
        $id_connect = pg_connect('host=' . $this->HOST . ' port=' . $this->PORT . ' dbname=' . $db_name . ' user=' . $this->USER . ' password=' . $this->PASSWORD);
        
        return $id_connect;
    }
}
?>
