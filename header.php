<?
session_start();
Error_Reporting(E_ALL & ~E_NOTICE);
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="/public/images/favicon.ico" type="image/x-icon">
<meta name="DESCRIPTION" content="Программно-технологический геоинформационный комплекс агромониторинга Красноярского края">
<meta http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Lang" content="en">
<meta name="author" content="ИКИТ">
<title>ГИС Агромониторинга</title>

<!--jQuery-->
<link rel="stylesheet" type="text/css" href="/public/jquery/jquery-ui.css">
<script src="/public/jquery/jquery-1.10.2.js" type="text/javascript"></script>
<script src="/public/jquery/jquery-ui-1.9.2.js" type="text/javascript"></script>

<!--GEOPORTAL API-->
<script src="http://edu.ikit.sfu-kras.ru/public/javascripts/geoportal/geoportal-api.min.js" type="text/javascript"></script>

<script src="/public/js/PortalGlobal/PortalGlobal.js" type="text/javascript"></script>
<script src="/public/js/RightPanel/RightPanelModel.js" type="text/javascript"></script>
<script src="/public/js/RightPanel/RightPanelView.js" type="text/javascript"></script>
<script src="/public/js/Groups/GroupsModel.js" type="text/javascript"></script>
<script src="/public/js/Groups/GroupsView.js" type="text/javascript"></script>
<script src="/public/js/Layers/LayersModel.js" type="text/javascript"></script>
<script src="/public/js/Layers/LayersView.js" type="text/javascript"></script>
<script src="/public/js/ErsData/ErsDataModel.js" type="text/javascript"></script>
<script src="/public/js/ErsData/ErsDataView.js" type="text/javascript"></script>

<script src="/public/js/Users/UsersController.js" type="text/javascript"></script>
<script src="/public/js/Users/UsersView.js" type="text/javascript"></script>
<script src="/public/js/Users/UsersModel.js" type="text/javascript"></script>

<script src="/public/js/Captcha/CaptchaView.js" type="text/javascript"></script>
<script src="/public/js/Captcha/CaptchaModel.js" type="text/javascript"></script>
<script src="/public/js/Captcha/CaptchaController.js" type="text/javascript"></script>

<script src="/public/js/vendor/jquery.mask.min.js" type="text/javascript"></script>
<script src="/public/js/vendor/jquery.minicolors.js" type="text/javascript"></script>

<!--Рисование -->
<script src="/public/js/PolygonDraw.js" type="text/javascript"></script>
<script src="/public/js/RectangleDraw.js" type="text/javascript"></script>

<!--BOOTSTRAP-->
<link rel="stylesheet" type="text/css" href="/public/bootstrap2/css/bootstrap.css">
<script src="/public/bootstrap2/js/bootstrap.js" type="text/javascript"></script>

<link rel="stylesheet" href="/public/css/jquery.minicolors.css">

<!--ACTIVEMAP MANAGER-->
<script src="/public/js/activemap-manager.js" type="text/javascript"></script>
<script src="/public/js/geojson.ndvi.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/css/s_activemap_ui.css">
<link rel="stylesheet" href="/public/css/ui.slider.css">
<link rel="stylesheet" href="/public/css/ui.horizontal.loader.css">
<link rel="stylesheet" href="/public/css/ui.filter.css">
<link rel="stylesheet" href="/public/css/ui.logo.css">
<link rel="stylesheet" href="/public/css/ui.right.panel.css">
<link rel="stylesheet" href="/public/css/ui.users.registration.css">

<script type="text/javascript">
var ieStyle = BrowserIs({'ie':'/public/css/ie.css', 'firefox':'/public/css/ie.css', 'chrome':''});
document.write('<link rel="stylesheet" href="' + ieStyle + '">');
var zoomControlShift = BrowserIs({'ie':'440px', 'chrome':'420px', 'firefox':'440px'});
</script>

<!--LIGHT TABS-->
<script src="/public/simple-tabs/js/ion.tabs.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/simple-tabs/css/ion.tabs.css">
<link rel="stylesheet" type="text/css" href="/public/simple-tabs/css/ion.tabs.skinBordered.css">

<!--Stickers-->
<script src="/public/stickers/jquery.stickr.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/public/stickers/stickr.css">

<script src="/public/js/ObjectInfo.js" type="text/javascript"></script>
<script src="/public/js/ObjectInfoHandler.js" type="text/javascript"></script>
<script src="/public/js/AMLayers.js" type="text/javascript"></script>
<script src="/public/js/Filter.js" type="text/javascript"></script>
<script src="/public/js/FilterHandler.js" type="text/javascript"></script>


<script src="/public/js/layer-uploader.js" type="text/javascript"></script>

<script src="/public/js/ErsData/result-data.js" type="text/javascript"></script>


<?
// Подключаем необходимые библиотеки
include("Modules/Users/Model/UsersModel.php");
include("vendor/Misc.php");
include("Modules/Popup/Model/PopupModel.php");
include("Modules/Styles/Model/StylesModel.php");
include("Modules/Farmers/Model/FarmersModel.php");
include("Modules/Logs/Model/LogsModel.php");

$authButton = $_POST['auth'];
$authExitButton = $_POST['auth-exit'];
$z = 0;
if ( $authButton )
{
    $usersModelObject = new UsersModel();
    $miscObject = new Misc();
    
    $login = $miscObject->CleanFormData($_POST['login']);
    $password = $miscObject->CleanFormData($_POST['password']);
    $encodedPassword = md5($password);
    
    $result = $usersModelObject->GetUser($login);
    $row = pg_fetch_array($result, null, PGSQL_ASSOC);
    
    // Проверяем существует ли такой пользователь в БД, прежде чем записать его в сессию
    if ( $row['login'] == $login && $row['password'] == $encodedPassword ) {
        // Проерить активирован ли пользователь
        if ( $row['activation_flag'] == 't' ) {
            $_SESSION['uLogin'] = $login;
            $_SESSION['uPassword'] = $password;
            $_SESSION['idRole'] = $row['role_id'];
            $_SESSION['idUser'] = $row['id'];
            if ( !empty($row['bbox']) ) {
                $_SESSION['uBbox'] = $row['bbox'];
            }
            if ( !empty($row['layer_to_disp']) ) {
                $_SESSION['uWLayer'] = $row['layer_to_disp'];
                
            }
        }
    }
}
if ( $authExitButton )
{
    session_unset();
    session_destroy();
}
if ( isset($_SESSION['uLogin']) && !empty($_SESSION['uLogin']) )
{
    $stylesModelObject = new StylesModel();
    // ПОЛУЧАЕМ ID ХОЗЯЙСТВА ПО ID ПОЛЬЗОВАТЕЛЯ. TODO: переделать на таблицу связи пользователей и хозяйств.
    $resultStyles = $stylesModelObject->GetUserStyles($_SESSION['idUser']);
    if ( isset($resultStyles) && !empty($resultStyles) ) {
        $rowStyles = pg_fetch_array($resultStyles, null, PGSQL_ASSOC);
        
        // ПОЛУЧАЕМ НАЗВАНИЕ ХОЗЯЙСТВА ИЗ СПРАВОЧНИКА ПО ID ХОЗЯЙСТВА.
        if ( isset($rowStyles['id_farmer']) ) {
            // Запоминаем ID хозяйства.
            $_SESSION['idFarmer'] = $rowStyles['id_farmer'];
			// Записываем в лог вход пользователя.
            $logsModelObject = new LogsModel();
            $logsModelObject->SaveLog(1, $rowStyles['id_farmer']);
            $popupModelObject = new PopupModel();
            $farmersModelObject = new FarmersModel();
            $resultFarmers = $popupModelObject->SelectDictionaryData($popupModelObject->FARMERS_TABLE, 'f_name', $rowStyles['id_farmer']);
            if ( isset($resultFarmers) && !empty($resultFarmers) ) {
                $rowFarmers = pg_fetch_array($resultFarmers, null, PGSQL_ASSOC);
                // Запоминаем название хозяйства.
                $_SESSION['farmerName'] = trim($rowFarmers['f_name']);
            }
            // ПОЛУЧАЕМ BBOX ХОЗЯЙСТВА, ЕСЛИ ОН ЕСТЬ.
            $result = $farmersModelObject->GetFarmerBbox($rowStyles['id_farmer']);
            if ( isset($result) && !empty($result) ) {
                $farmerBbox = pg_fetch_array($result, null, PGSQL_ASSOC);
                $_SESSION['minLat'] = $farmerBbox['minlat'];
                $_SESSION['minLng'] = $farmerBbox['minlng'];
                $_SESSION['maxLat'] = $farmerBbox['maxlat'];
                $_SESSION['maxLng'] = $farmerBbox['maxlng'];
            }
        }
    }
}
?>

<script type="text/javascript">
/****************************** ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ ******************************/

Users.login = "<?=$_SESSION['uLogin']?>";
Users.password = "<?=$_SESSION['uPassword']?>";
Users.workingLayerId = "<?=$_SESSION['uWLayer']?>";
Users.bboxWkt = "<?=$_SESSION['uBbox']?>";
Users.role = "<?=$_SESSION['idRole']?>";
var idFarmer = "<?=$_SESSION['idFarmer']?>";
var farmerName = "<?=$_SESSION['farmerName']?>";

var _minLat = "<? if ( !empty($_SESSION['minLat']) ) echo $_SESSION['minLat']; else echo $z; ?>";
var _minLng = "<? if ( !empty($_SESSION['minLng']) ) echo $_SESSION['minLng']; else echo $z; ?>";
var _maxLat = "<? if ( !empty($_SESSION['maxLat']) ) echo $_SESSION['maxLat']; else echo $z; ?>";
var _maxLng = "<? if ( !empty($_SESSION['maxLng']) ) echo $_SESSION['maxLng']; else echo $z; ?>";

var hostName = 'edu.ikit.sfu-kras.ru';

var farmerUniversalStyleName = 'contours_zshn_suchobuzimskoe_2015_universal_style';
var culturesPlanUniversalStyleName = 'cultures_plan_universal_style';
var culturesFactUniversalStyleName = 'cultures_fact_universal_style';
var NDVIUniversalStyle = 'ndvi_universal_style';

var filterCQLFarmer = new GeoPortal.Filter.CQL([
    {
        field: "owner",
        type: "string",
        value: farmerName
    }
]);

var queryArray = new Array();
var selectorLayer = new Array();

var idsString = '';

var mapObject;
var layersStore = new Array();

var _currentToken;

var layerOfFarmer;
var layerCultures;
var layerCulturesFact;

var randmz = Math.random();

var gid;
var someString = '';
var _compare_bbox = {minLat: 90, maxLat: 0, minLng: 180, maxLng: 0};

var date = new Date();
currentYear = date.getFullYear();

/** Кликнутый слой на правой панели */
var clickedLayerForInfoTab = { id: null, name: null };

var editingLayerId;

/***********************************************************************************/

$(function() {
    
    var clickLatLn;
        
    var userPanelDiv = $(ObjectInfo.selectors['divUserPanel']),
        rollUserPanelDiv = $(ObjectInfo.selectors['divRollUserPanel']);
    
    // ACCORDION ДЛЯ МЕНЮ СПРАВОЧНИКОВ.    
    $( "#accordion" ).accordion({
        collapsible: false
    });
    
    // АВТОРИЗАЦИЯ.
    GeoPortal.authenticate(Users.login, Users.password,
        function(data){
            $("body").prepend('<div id="waiting-mode"></div>');
            $("#waiting-mode").after(horizontalLoader);
            $(RegistrationForm.selectors['urMenuButton']).remove();
            $("script[src*='UsersController.js']").remove();
            $("script[src*='UsersView.js']").remove();
            $("script[src*='Captcha']").remove();
            GeoPortal.requestGroups(true,
                function(groups) {
                    $.when(
                        Groups.groups = groups,
                        RightPanelForm.display(),
                        LayersForm.display(),
                        ErsDataForm.display(),
                        GroupsForm.display()
                    ).done(function(){
                        $("body div#waiting-mode").remove();
                        $("#cssload-wrapper").remove();
                    });
                    
                    if ( Users.workingLayerId ) {
                        Layers.enableWorkingLayer(parseInt(Users.workingLayerId));
                    }
                    Users.position();
                },
                function(status,error) {
                    console.log(status);
                    console.log(error);
                }
            );
            
            // Получаем текущего пользователя
            GeoPortal.currentUser(
                function(user){
                    if (user != null){
                        $(".hello-user").html('Добро пожаловать, '+user.name);
                    }
                },
                function(status,error){
                    console.log("Error to request authentication. Status = " +
                                status + ". Error text: " + error);
                }
            );
            // end
        },
        function(status,error){
            console.log("Error to request authentication. Status = " +
                        status + ". Error text: " + error);
        }
    );
    // end
    
    
    // Отображаем элементы геопортала, когда объект GeoPortal загружен
    GeoPortal.on("ready",function() {

        
        var schemas     = GeoPortal.baseLayers.schemas,
            schemasLen  = schemas.length,
            spaces      = GeoPortal.baseLayers.spaces,
            spacesLen   =  spaces.length,
            currentBaseLayer,
            i=0,
            selected = "",
            baseLayer,
            baseLayerArray = new Array();
        
        _currentToken = GeoPortal._accessToken;
        
        // ЕСЛИ ПОЛЬЗОВАТЕЛЬ АДМИНИСТРАТОР, ТО ОН ДОЛЖЕН УВИДЕТЬ ВСЕ ХОЗЯЙСТВА, ВСЕ КУЛЬТУРЫ ПЛАНИРУЕМЫЕ И ФАКТИЧЕСКИЕ.
        if ( farmerName == '' ) {
            farmerUniversalStyleName = 'all_farmers_color_admin_style';
        }

        // формируем слой хозяйства, чтобы он был доступен везде
        layerOfFarmer = new GeoPortal.Layer.WMS("http://"+hostName+"/service/wms", {
            service: 'WMS',
            request: 'GetMap',
            version: '1.1.1',
            layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
            styles: farmerUniversalStyleName,
            format: 'image/png',
            transparent: true,
            cql_filter: filterCQLFarmer.filterString(),
            token: _currentToken,
            random: randmz
        });
        
        layerCultures = new GeoPortal.Layer.WMS("http://"+hostName+"/service/wms", {
            service: 'WMS',
            request: 'GetMap',
            version: '1.1.1',
            layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
            styles: culturesPlanUniversalStyleName,
            format: 'image/png',
            transparent: true,
            cql_filter: filterCQLFarmer.filterString(),
            token: _currentToken,
            random: randmz
        });
        
        layerCulturesFact = new GeoPortal.Layer.WMS("http://"+hostName+"/service/wms", {
            service: 'WMS',
            request: 'GetMap',
            version: '1.1.1',
            layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
            styles: culturesFactUniversalStyleName,
            format: 'image/png',
            transparent: true,
            cql_filter: filterCQLFarmer.filterString(),
            token: _currentToken,
            random: randmz
        });
        
        mapObject = new GeoPortal.Map('map');
        
        // Добавляем элемент Zoom
        zoom = new GeoPortal.Control.Zoom();
        zoom.on("handClick", function(){
            console.log("icon hand click");
        },this);
        // Позиционируем карту. Это позиционирование по умолчанию.
        mapObject.on("ready",function(){
                mapObject.setView(92.93111444,56.0641667,9);
        },this);

        mapObject.addControl(zoom);
        // end 
        
        // Добавляем элемент Измерение дистанции
        distance = new GeoPortal.Control.Distance();
        distance.on("control:distance:enable", function(data){},this);
        distance.on("control:distance:disable", function(data){},this);
        mapObject.addControl(distance);
        $('div#distanceButton img').prop("title", "Измерение расстояния");
        // end
        
        // Добавляем элемент Рисования объекта
        PolygonDraw = new GeoPortal.Control.PolygonDraw;
        PolygonDraw.on("control:DrawPolygon:enable", function(data){},this);
        PolygonDraw.on("control:DrawPolygon:disable", function(data){},this);
        mapObject.addControl(PolygonDraw);
        $("#polygondraw").hide();
        // end
        
        RectangleDraw = new GeoPortal.Control.RectangleDraw();
        RectangleDraw.on("control:RectangleDraw:enable", function(data){},this);
        RectangleDraw.on("control:RectangleDraw:created", function(data){
            Filter._filter_bbox = new GeoPortal.LatLngBounds(data.latLngs[0],data.latLngs[2]);
            $(Filter.selectors['spanCoordinates']).prepend('('+Filter._filter_bbox._northEast.lng+'), ('+Filter._filter_bbox._northEast.lat+'), ('+Filter._filter_bbox._southWest.lng+'), ('+Filter._filter_bbox._southWest.lat+')');
            
            $("body div#waiting-mode").show();
            $(RegistrationForm.selectors['divRegFormWrapper']).show();
            Users._bbox = new GeoPortal.LatLngBounds(data.latLngs[0],data.latLngs[2]);
            $(RegistrationForm.selectors['spanCoordinates']).prepend('('+Users._bbox._northEast.lng+'), ('+Users._bbox._northEast.lat+'), ('+Users._bbox._southWest.lng+'), ('+Users._bbox._southWest.lat+')');
        },this);
        RectangleDraw.on("control:RectangleDraw:disable", function(data){
             $(Filter.selectors['spanCoordinates']).empty();
             $(RegistrationForm.selectors['spanCoordinates']).empty();
        },this);
        mapObject.addControl(RectangleDraw);
        $("#rectangleDrawButton").hide();
        
        // Добавляем элементы select для выбора карт-подложек
        currentBaseLayer = mapObject.baseLayer();
        
        $("body").find("#map").after('<div id="baseLayerContainer"/>');
            $("#baseLayerContainer").append('<div class="schemasContainer"><select class="selectBaseLayer"><option value="" selected>'+GPMessages("default.select")+'...</option></select></div>');
            $("#baseLayerContainer").append('<div class="spacesContainer"><select class="selectBaseLayer"><option value="" selected>'+GPMessages("default.select")+'...</option></select></div>');
        
        if(schemasLen >0) {
            for(i=0; i<schemasLen; i++) {
                baseLayer = schemas[i];
                baseLayerArray[baseLayer.id()] = baseLayer;

                if(baseLayer.id() == currentBaseLayer.id())
                    selected = "selected";
                else
                    selected = "";

                $("#baseLayerContainer>.schemasContainer>select").append('<option value="'+baseLayer.id()+'" '+selected+'>'+baseLayer.name()+'</option>');
            }
        }
        if(spacesLen >0) {
            for(i=0;i<spacesLen;i++) {
                baseLayer = spaces[i];
                baseLayerArray[baseLayer.id()] = baseLayer;

                if(baseLayer.id() == currentBaseLayer.id())
                    selected = "selected";
                else
                    selected = "";

                $("#baseLayerContainer>.spacesContainer>select").append('<option value="'+baseLayer.id()+'" '+selected+'>'+baseLayer.name()+'</option>');
            }
        }
        // Поключаемся к событию "change" у каждого селектора. Когда новый базовый слой выбран, устанавливаем карте.
        $(".selectBaseLayer").change(function(){
            var id = $(this).val(),
                baseLayer = baseLayerArray[id];

            if(typeof baseLayer != 'undefined') {
                mapObject.setBaseLayer(baseLayer);
                currentBaseLayer = baseLayer;
            }
            if($(this).parent("div").hasClass("schemasContainer")){
                $("#baseLayerContainer>.spacesContainer>select").children("option:first").attr("selected","selected");
            }
            else{
                $("#baseLayerContainer>.schemasContainer>select").children("option:first").attr("selected","selected");
            }

        });
        // end
        
        // ПОЛУЧИТЬ КООРДИНАТЫ КЛИКА ПО КАРТЕ
        mapObject.on("click",function(e) {
            clickLatLng = e.latlng;
            $("#click").remove();
            $(".wrap").append('<div id="click"><p class="latLng">Широта|Долгота: ' + clickLatLng.lat + " | " + clickLatLng.lng + '</p></div>');
        },this);
        
        // ОБРАБОТКА КЛИКА ПО ОБЪЕКТУ НА КАРТЕ
        mapObject.on("featureClicked",function(data) {
            
            if(typeof data.features == 'undefined') {
                // console.log("Request features error. Status = " + status + ". Error text: " + error);
                return;
            }
            // Делаем деселект редактируемого контура
            if (ObjectInfo._layerEditor != null) {
                ObjectInfo._layerEditor.editing.disable();
                mapObject.removeLayer(ObjectInfo._layerEditor);
                ObjectInfo._layerEditor = null;
                mapObject.removeLayer(ObjectInfo._layerSelector);
                ObjectInfo._layerSelector = null;
            }

            var features = data.features;
            ObjectInfo._features = features;

            // закраска объекта
            var objGeometry = ObjectInfo._geom = JSON.parse(features[0]._model._values.data.geom);
            geoJson = objGeometry;

            var layer1 = new GeoPortal.Layer.GeoJSON(
                geoJson, {color: '#32A4D2', /*weight: 5, opacity: 0.65,*/ editable:true,fillOpacity:0.65} //#32A4D2
            );
            
            ObjectInfo._layerSelector = layer1;
            selectorLayer.push(layer1);
            //alert(selectorLayer.length);
            mapObject.addLayer(layer1);
            
            // убираем выделение объекта одинарным щелчком мыши по нему
            layer1.on("click",function(e) {
                // когда делаем деселект контура, то убираем ID этого контура из строки idsString
                mapObject.removeLayer(layer1);
                layer1 = null;
                idsString = idsString.replace(new RegExp(objFID.toString()+',','g'),"");
                someString = someString.replace(new RegExp(objFID.toString(),'g'),"");
                // и очищаем массив координат.
                objGeometry = null;
            },this);
            
            var objFID = JSON.parse(features[0]._model._values.data.fid);
            // строка ID выбранных контуров полей
            idsString += objFID + ',';
            
            var objGeom = JSON.parse(features[0]._model._values.data.geom);
            
            var _object_bbox = GetBbox(objGeom);
            
            someString += "|fid:"+objFID+", minLat:"+_object_bbox['minLat']+", minLng:"+_object_bbox['minLng']+", maxLat:"+_object_bbox['maxLat']+", maxLng:"+_object_bbox['maxLng']+"|";
		
            // При клике по объекту, имеющему координаты, выполняем следующие действия:
            if(clickLatLng instanceof GeoPortal.LatLng) {
                // ПРОВЕРЯЕМ НА РЕЖИМ ПЕЧАТИ КАРТЫ. ЕСЛИ НЕ В РЕЖИМЕ ПЕЧАТИ, ТО МОЖНО ВСЕ.
                if ( $("#print-mode-hidden").val() == 0 ) {
                    // убираем элемент object-info в левой панеле, если оно было создано,
                    $("#object-info-form").remove();
                    // разворачиваем левую панель,
                    userPanelDiv.slideDown(500); 
                    userPanelDiv.attr('class', 'up-hide'); 
                    rollUserPanelDiv.css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
                    // Делаем активной нужную вкладку таба.
                    $.ionTabs.setTab("Tabs_Group_name", "Tab_2_name");
                    // Перемещаем контрол zoom в видимую область.
                    $(".zoom-control").css({"left":zoomControlShift});
                    
                    // Выводим информацию о выбранных объектах, создаем элемент object-info в левой панеле.
                    var selected = ObjectInfo._selected(mapObject, layersStore, features);
                    $("div[data-name='Tab_1_name']").prepend(selected);
                }
            }
        },this);
        // end
    },this);
    // END Geoportal ready
});
</script>
<script src="/public/js/activemap-index.js" type="text/javascript"></script>
<script src="/public/js/RightPanel/RightPanelController.js" type="text/javascript"></script>
<script src="/public/js/ErsData/ErsDataController.js" type="text/javascript"></script>
<!--<script src="/public/js/RightPanelHandler.js" type="text/javascript"></script>-->

</head>

<body>
<script type="text/javascript">
/*document.addEventListener("DOMContentLoaded", function(){
    console.log("Есть!");
});*/
</script>

<div class="container">
    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="navbar-inner">
            <div class="nav-collapse">
                 <ul class="nav">
                    <li class="active">
                        <div class="logo-mid"></div>
                        <a class="brand" style="margin:0px; float: left;" href="/">
                           ГИС Агромониторинга
                        </a>
                    </li>
                    <? if ( isset($_SESSION['idRole']) && $_SESSION['idRole'] == 8 ) { ?>
                    <li class="dropdown">
                        <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/public/images/dictionary.png" alt="Справочники" title="Справочники" style="width:24px; height:18px;">
                            <b class="caret"></b>
                        </a>
                     
                        <ul class="dropdown-menu" style="width: 235px;">
                            <li class="nav-header">Управление справочниками
                            <ul style="list-style: none;">
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php">Главная</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqCultures">С/Х Культуры</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqAgriculturalWorks">Виды сельскохозяйственных работ</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqChemcomposition">Химический состав почв</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqOwnership">Формы собственности</a></li>
                                <li><a target="_blank" href="<?=$_SERVER['REMOTE_SERVER']?>/dictionaries/management/index.php?render=jqFarmers">Хозяйства</a></li>
                            </ul>
                            </li>
                        </ul>
                    </li>

					          <li class="divider-vertical"></li>

                    <li class="dropdown">
                        <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/public/images/stat.png" alt="Статистика" title="Статистика" style="height: 24px; width: 24px; margin: -6px 0px -3px 0px;">
                            <b class="caret"></b>
                        </a>
                     
                        <ul class="dropdown-menu" style="width: 235px;">
                            <li class="nav-header">Статистика
                            <ul style="list-style: none;">
                                <li><a href="<?=$_SERVER['REMOTE_SERVER']?>/Modules/Logs/View/index.php">Входы пользователей</a></li>
                            </ul>
                            </li>
                        </ul>
                    </li>
                    <? } ?>
                    <li class="divider-vertical"></li>
                    <li>
                        <div class="printMode">
                            <img src="/public/images/print.png" alt="Печать карты" title="Печать карты" style="margin-left: 12px; margin-right: 12px;">
                            <input type="hidden" id="print-mode-hidden" value="0" />
                        </div>
                    </li>
                    <li class="divider-vertical"></li>

                     <? if ( isset($_SESSION['idRole']) && $_SESSION['idRole'] == 8 ) { ?>
                       <li>
                         <button
                           type="button"
                           id="upload-layer-button"
                           class="btn btn-info"
                           onclick="onHeaderUploadBtnClicked()"
                         >Загрузить слой</button>
                       </li>

                       <li class="divider-vertical"></li>
                     <? } ?>

                </ul>
                <? if ( isset($_SESSION['uLogin']) && !empty($_SESSION['uLogin']) ) { ?>
                    <div id="hello-box">
                        <p class="hello-user"></p>
                    </div>
                    <div class="menu-authorization-exit">
                        <form action="<?=$_SERVER['PHP_SELF']?>" method="post">
                            <input type="submit" name="auth-exit" value="  Выйти">
                        </form>
                    </div>
                <? } else { ?>
                    <div class="menu-authorization">
                        <div></div>
                    </div>
                <? } ?>
                
            </div>
        </div>
    </div>
    <!--  АВТОРИЗАЦИЯ  -->
    <? if ( empty($_SESSION['uLogin']) ) { ?>
    <div id="authorization-box">
        <p class="authorization-title">Авторизация</p>
        <div class="authorization-form">
            <form action="<?=$_SERVER['PHP_SELF']?>" method="post">
                <table>
                <tr>
                    <td align="right" style="width: 100px;"><p class="authorization-login">Логин:</p></td>
                    <td align="right"><input class="authorization-login-element" type="text" id="login" name="login"></td>
                </tr>
                <tr>
                    <td align="right"><p class="authorization-password">Пароль:</p></td>
                    <td align="right"><input class="authorization-password-element" type="password" id="password" name="password"></td>
                </tr>
                </table>
                <div class="button-block">
                    <input type="submit" id="authorization-enter" name="auth" value="Вход">
                    <button type="button" id="authorization-cancel">Отмена</button>
                </div>
            </form>
        </div>
    </div>
    <? } ?>
    <!--  END  -->
</div>