<? 
include("header.php");
?>

<div class="wrap">

    <div class="content">
        <div id="map"></div>
    </div>
    
    <? if ( isset($_SESSION['uLogin']) && !empty($_SESSION['uLogin']) ) { ?>
    <div id="right-panel"></div>
    <div id="user-panel" class="up-show">
        <div class="ionTabs" id="tabs_1" data-name="Tabs_Group_name">
            <ul class="ionTabs__head">
                <li class="ionTabs__tab" data-target="Tab_1_name">Свойства</li>
                <li class="ionTabs__tab" data-target="Tab_2_name">Управление</li>
                <li class="ionTabs__tab" data-target="Tab_3_name">Отчеты</li>
                <li class="ionTabs__tab" data-target="Tab_4_name">Слои</li>
            </ul>
            <div class="ionTabs__body">
                <div class="ionTabs__item" style="padding:40px 15px;" data-name="Tab_1_name"></div>
                <div class="ionTabs__item" style="padding:20px 15px;" data-name="Tab_2_name"></div>
                <div class="ionTabs__item" style="padding:20px 15px;" data-name="Tab_3_name"></div>
                <div class="ionTabs__item" data-name="Tab_4_name"></div>
                <!--<div class="ionTabs__preloader"></div>-->
            </div>
        </div>
    </div>
    <div class="roll-user-panel"><p>С/Х ПРЕДПРИЯТИЕ</p></div>
    
    <div id="dialog-box" title="">
        <div class="dialog-content"></div>
    </div>
    <? } else { ?>
    <div class="logo"></div>
    <? } ?>
</div>

<?
include("footer.php");
?>