<?php

// СОХРАНИТЬ ИЗМЕНЕНИЕ ОБЪЕКТА СЛОЯ
if ( !empty($_POST['_action']) && $_POST['_action'] == 1 ) {
    // Проверяем входные параметры запроса
    if ( !empty($_POST['geometry']) && !empty($_POST['layerid']) && !empty($_POST['fieldid']) && !empty($_POST['token']) ) {
        //echo "<br>".var_dump($_POST['attributes'])."<br>";
        $data['attributes'] = $_POST['attributes'];
        //echo "<br>".var_dump($_POST['geometry'])."<br>";
        $data['geometry'] = $_POST['geometry'];
        $layerId = $_POST['layerid'];
        $fieldId = $_POST['fieldid'];
        $token = $_POST['token'];
        // URL, по которому будет выполнен REST запрос
        $url = 'http://edu.ikit.sfu-kras.ru/layers/'.$layerId.'/feature/'.$fieldId.'?token='.$token;
        
        $mycurl = curl_init($url);
        // Данные, которые будут отправлены в запросе
        $encodedData = json_encode($data);
        curl_setopt($mycurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($mycurl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($mycurl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($mycurl, CURLOPT_POSTFIELDS, $encodedData);
        var_dump(curl_exec($mycurl));
    }
}

// СОЗДАНИЕ ОБЪЕКТА СЛОЯ
if ( !empty($_POST['_action']) && $_POST['_action'] == 2 ) {
    // POST /layers/145/feature?token=mbs90lon2a8
    // Проверяем входные параметры запроса
    if ( !empty($_POST['geometry']) && !empty($_POST['layerid']) && !empty($_POST['token']) ) {
        // echo "<br>".var_dump($_POST['attributes'])."<br>";
        $data['attributes'] = $_POST['attributes'];
        
        // echo "<br>".var_dump($_POST['geometry'])."<br>";
        $data['geometry'] = $_POST['geometry'];
        
        // echo "<br>".var_dump($_POST['layerid'])."<br>";
        $layerId = $_POST['layerid'];
        
        // echo "<br>".var_dump($_POST['token'])."<br>";
        $token = $_POST['token'];
        // URL, по которому будет выполнен REST запрос
        $url = 'http://edu.ikit.sfu-kras.ru/layers/'.$layerId.'/feature?token='.$token;
        // echo "<br>".$url."<br>";
        
        $mycurl = curl_init($url);
        // Данные, которые будут отправлены в запросе
        $encodedData = json_encode($data);
        curl_setopt($mycurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($mycurl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($mycurl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($mycurl, CURLOPT_POSTFIELDS, $encodedData);
        var_dump(curl_exec($mycurl));
    }
}

// УДАЛЕНИЕ ОБЪЕКТА СЛОЯ
if ( !empty($_POST['_action']) && $_POST['_action'] == 3 ) {
    // Проверяем входные параметры запроса
    if ( !empty($_POST['layerid']) && !empty($_POST['fieldid']) && !empty($_POST['token']) ) {
        $layerId = $_POST['layerid'];
        $fieldId = $_POST['fieldid'];
        $token = $_POST['token'];
        // URL, по которому будет выполнен REST запрос
        $url = 'http://edu.ikit.sfu-kras.ru/layers/'.$layerId.'/feature/'.$fieldId.'?token='.$token;
        
        $mycurl = curl_init($url);
        // Данные, которые будут отправлены в запросе
        curl_setopt($mycurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($mycurl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($mycurl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($mycurl, CURLOPT_POSTFIELDS, '{}');
        var_dump(curl_exec($mycurl));
    }
}
?>
