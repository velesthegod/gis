<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class HistoryModel extends DB
{
    protected $SCHEME  = 'data';
    protected $DB_NAME = 'inf_region';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    /**
     * @description Метод SetHistorySeva() запоминает историю планируемых и фактических культур сева в таблице plan_fact
     * @param $data array Ассоциативный массив (название колонки => значение колонки)
     * @return boolean
     */
     public function SetHistorySeva($data)
     {
         // Сохраняем планируемые культуры (когда планируем, всегда выполняем insert)
         if ( isset($data['plan_fields']) )
         {
             $query = "INSERT INTO " . $this->SCHEME . "." . $this->PLAN_FACT_TABLE . " ( ";
             
             $i = 1;
             $arr_count = count($data['plan_fields']);
             $delimiter = ", ";
             foreach( $data['plan_fields'] as $key=>$val )
             {
                 if ( $i == $arr_count )
                 {
                     $delimiter = "";
                 }
                 
                 // формируем строку ключей для insert
                 $query .= $key . $delimiter;
                 
                 // формируем строку значения для insert
                 if ( is_string($val) )
                 {
                     $values .= "'" . $val . "'" . $delimiter;
                 }
                 else
                 {
                     $values .= $val . $delimiter;
                 } 
                 
                 $i++;
             }
             
             $query .= ") ";
             
             $query .= "VALUES (" . $values . ")";
         }
         // Сохраняем фактические культуры (когда факт, всегда выполняем update последнего сохраненного поля по его ID)
         if ( isset($data['fact_fields']) )
         {
             // Получаем ID хозяйства
             $idFarmer = $data['fact_fields']['id_farmer'];
             // Убираем лишнее из массива.
             unset($data['fact_fields']['id_farmer']);
             
             $query = "UPDATE " . $this->SCHEME . "." . $this->PLAN_FACT_TABLE;
             $query .= " SET ";
             
             // Сохраняем ID поля и удаляем этот элемент, чтобы не мешал формировать запрос.
             $fid = $data['fact_fields']['id_field'];
             unset($data['fact_fields']['id_field']);
             
             $i = 1;
             $arr_count = count($data['fact_fields']);
             $delimiter = ", ";
             foreach ( $data['fact_fields'] as $key=>$val )
             {
                 if ( $i == $arr_count )
                 {
                     $delimiter = "";
                 }
                 
                 if ( is_string($val) )
                 {
                     $query .= $key . " = '". $val . "'" . $delimiter;
                 }
                 else
                 {
                     $query .= $key . " = ". $val . $delimiter;
                 }
                 
                 $i++;
             }
             
             $query .= " FROM ( SELECT max(id_plan_fact) ipf FROM " . $this->SCHEME . "." . $this->PLAN_FACT_TABLE . " WHERE id_field = " . $fid . " AND id_farmer = " . $idFarmer . " ) p ";
             
             $query .= "WHERE id_plan_fact = p.ipf;";
             
              /*
              // Этот запрос лежит в основе
              UPDATE data.plan_fact
              SET     fact_date='2016-01-23', 
                fact_id_culture=7777
              FROM (
                SELECT max(id_plan_fact) ipf 
                FROM data.plan_fact 
                WHERE id_field=559 AND id_farmer=7
              ) p

              WHERE id_plan_fact = p.ipf;
              */
         }
         
         //echo $query;
         $result = pg_query($this->id_connect, $query);
         
         return $result;
     }
    
    public function GetHistoryById($id_plan_fact)
    {
        $query = "SELECT * FROM " . $this->SCHEME . "." . $this->PLAN_FACT_TABLE . " ";
        $query .= "WHERE id_plan_fact = " . $id_plan_fact . ";";
        $result = pg_query($this->id_connect, $query);
        return $result;
    }
    
    public function GetHistoryByFarmerId($id)
    {
        //TODO: Фильтр по дате
        $query = "SELECT max(t1.id_plan_fact) as id_plan_fact,
                       t1.id_field as id_field,
                       t1.id_farmer,
                       t3.id as field_number
                FROM data.plan_fact t2, data.contours_zshn_suchobuzimskoe_2015 t3
                  INNER JOIN data.plan_fact t1 ON (t1.id_field = t3.gid)
                WHERE t1.id_field = t2.id_field 
                  AND t1.id_plan_fact = t2.id_plan_fact
                  AND t1.id_farmer = " . $id . "
                  --AND t1.plan_date > '2016-01-01' 
                  --AND t1.plan_date < '2016-12-31'
                GROUP BY t1.id_field, 
                    t1.id_farmer,
                    t3.id
                ORDER BY t1.id_field ASC;";
                
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
}
?>
