<?php
require_once("../Form/LogsForm.php");

$logsFormObject = new LogsForm();

// ДИАГРАММА СТАТИСТИКИ ПОСЕЩЕНИЙ РЕСУРСА ЗА КОНКРЕТНУЮ ДАТУ
if ( isset($_POST['date']) && !empty($_POST['date']) ){
    $date = $_POST['date'];
    $date = date("Y-m-d", strtotime($date));
    
    $logsFormObject->DrawUserEntriesStatisticDiagram($date);        
    
}

// ДИАГРАММА СТАТИСТИКИ ПОСЕЩЕНИЙ РЕСУРСА ЗА ПЕРИОД
if ( !empty($_POST['dateFrom']) && !empty($_POST['dateTo']) ){
    $dateFrom = date("Y-m-d", strtotime($_POST['dateFrom']));
    $dateTo = date("Y-m-d", strtotime($_POST['dateTo']));
    
    $logsFormObject->DrawUserEntriesStatisticDiagramByRange($dateFrom, $dateTo);
        
}
?>
