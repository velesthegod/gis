<?php
require_once("../Model/LogsModel.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Farmers/Model/FarmersModel.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Popup/Model/PopupModel.php");

class LogsForm
{
    public $logsModelObject;
    public $farmersModelObject;
    public $popupModelObject;
    
    public function __construct()
    {
        $this->logsModelObject = new LogsModel();
        $this->farmersModelObject = new FarmersModel();
        $this->popupModelObject = new PopupModel();
    }
    
    public function DrawUserEntriesStatisticDiagram($date)
    {
        // Получить все хозяйства.
        $result = $this->popupModelObject->SelectDictionaryData($this->popupModelObject->FARMERS_TABLE, "", "");
        $statisticString = 'data: [ ';

        while ($row = pg_fetch_array($result, null, PGSQL_ASSOC))  {
            
            $result_entries = $this->logsModelObject->GetUserEntriesStatistic($date, $row['id_farmer']);
            $count = pg_fetch_array($result_entries, null, PGSQL_ASSOC);
            
            if ( $count['user_entries'] != 0 ){
                $statisticString .= '{type: "stackedBar", toolTipContent: "<strong>Пользователь:</strong> '.$row['f_name'].'<br/><strong>Количество посещений:</strong> {y}<br/><strong>Дата:</strong> {x}", legendText: "'.$row['f_name'].'", showInLegend: "true", 
                    dataPoints: [
                        { x: new Date("'.$date.'"), y: '.$count['user_entries'].' },
                    ]},';
            }
        }
        $statisticString .= "],";
        
        ?>
        <div id="block-stat">
            <script type="text/javascript">
              $(function() {
                var chart = new CanvasJS.Chart("chartContainer",
                {
                height:200,
                width:1000,
                theme: "theme1",
                  title:{
                  text: "Статистика посещений ресурса",
                   padding: 20,
                 fontSize: 24,
                 fontFamily: "Arial",
                  },
                   exportFileName: "Статистика посещений",
                   exportEnabled: true,
                  animationEnabled: true,
                  axisX: {
                    valueFormatString: "YYYY-MM-DD",     
                    labelFontSize: 14,
                    
                    
                  },
                   axisY: {
                      title: "Количество посещений",
                    titleFontFamily: "Arial",
                    titleFontSize: 14,
                    labelFontSize: 14,
                    interval: 1,
      
                  },
                dataPointMaxWidth: 60,
                  <?=$statisticString?>
                  
                  legend:{
                    cursor:"pointer",
                    fontFamily: "Arial",
                    fontSize: 16,
                    maxHeight: 1000,
                    maxWidth: 400,
                    horizontalAlign: "right", // left, center ,right 
                    verticalAlign: "bottom",
                    itemclick:function(e) {
                      if(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible){
                        e.dataSeries.visible = false;
                      }
                      else {
                        e.dataSeries.visible = true;
                      }
                      
                      chart.render();
                    }
                  }
                });

                chart.render();
              });
            </script>
            <div id="chartContainer" style="height: auto; width: auto;"></div>
        </div>
        <?
    }
    
    public function DrawUserEntriesStatisticDiagramByRange($dateFrom, $dateTo)
    {
        // Получить все хозяйства.
        $result = $this->popupModelObject->SelectDictionaryData($this->popupModelObject->FARMERS_TABLE, "", "");
        $statisticString = 'data: [ ';
        while ($row = pg_fetch_array($result, null, PGSQL_ASSOC))  {
            
            $result_entries = $this->logsModelObject->GetUserEntriesStatisticByRange($dateFrom, $dateTo, $row['id_farmer']);
            $resultCount = pg_num_rows($result_entries);
            
            if ( $resultCount != 0 ) {
                
                $statisticString .= '{type: "stackedBar", toolTipContent: "<strong>Пользователь:</strong> '.$row['f_name'].'<br/><strong>Количество посещений:</strong> {y}<br/><strong>Дата:</strong> {x}", legendText: "'.$row['f_name'].'", showInLegend: "true", 
                        dataPoints: [';
                
                while ( $row_count = pg_fetch_array($result_entries, null, PGSQL_ASSOC) ) {
                
                    $statisticString .= '{ x: new Date("'.$row_count['date_action'].'"), y: '.$row_count['user_entries'].' },';
                    
                }
                
                $statisticString .= ']},';
                
            }
        }
        $statisticString .= "],";
        
        ?>
        <div id="block-stat">
            <script type="text/javascript">
              $(function() {
                  
                  var chart = new CanvasJS.Chart("chartContainer",
                  {
                        theme: "theme1",
                        height:1000,
                        width:1000,
                        animationEnabled: true, 
                        title:{
                            text: "Статистика посещений ресурса",
                            padding: 20,
                            fontSize: 24,
                            fontFamily: "Arial",
                        },
                        animationEnabled: true,
                        toolTipContent: "{label}<br/><span style='\"'color: {color};'\"'>",
                        exportFileName: "Статистика посещений",
                        exportEnabled: true,
                        axisX: {
                            valueFormatString: "DD-MM-YYYY",  
                        
                            labelFontSize: 12,
                            titleFontFamily: "Arial",
                            interval: 1,
                            intervalType: "day" ,
                        },
                        axisY: {
                            title: "Количество посещений",
                            gridThickness: 1,   
                            tickLength: 15,
                            tickThickness: 1,
                            titleFontFamily: "Arial",
                            titleFontSize: 14,
                            labelFontSize: 14,
                            interval: 1,
                        },
                        dataPointMaxWidth: 60,
                        
                        <?=$statisticString?>
                      
                        legend:{
                            cursor:"pointer",
                            fontFamily: "Arial",
                            fontSize: 14,
                            maxHeight: 1000,
                            maxWidth: 400,
                            horizontalAlign: "right", // left, center ,right 
                            verticalAlign: "bottom",  // top, center, bottom
                            itemclick:function(e) {
                                if(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible){
                                    e.dataSeries.visible = false;
                                }
                                else {
                                    e.dataSeries.visible = true;
                                }
                          
                                chart.render();
                            }
                        }
                  });

                  chart.render();
              });
            </script>
            <div id="chartContainer" style="height: auto; width: auto;"></div>
        </div>
        <?
    }
}
?>