<?php

session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/vendor/Misc.php");
require_once("../Form/PopupForm.php");

$miscObject = new Misc();

//$gid                = $miscObject->CleanFormData($_REQUEST['gid']);
$fid                = $miscObject->CleanFormData($_REQUEST['fid']);
$showDataByGid      = $miscObject->CleanFormData($_REQUEST['showDataByGid']);
$changeDataID       = $miscObject->CleanFormData($_REQUEST['changeDataID']);

$cultureval         = $miscObject->CleanFormData($_REQUEST['cultureval']);
$data['owner']      = $miscObject->CleanFormData($_REQUEST['owner']); 
$data['area_giv']   = $miscObject->CleanFormData($_REQUEST['area_giv']); 
$data['area_calc']  = $miscObject->CleanFormData($_REQUEST['area_calc']); 
$data['culture_13'] = $miscObject->CleanFormData($_REQUEST['culture_13']); 
$data['cult14plan'] = $miscObject->CleanFormData($_REQUEST['cult14plan']);
$data['culture_14'] = $miscObject->CleanFormData($_REQUEST['culture_14']); 
$data['culture_15'] = $miscObject->CleanFormData($_REQUEST['culture_15']); 
$data['data_seva']  = $miscObject->CleanFormData($_REQUEST['data_seva']);

$culturesList       = $miscObject->CleanFormData($_REQUEST['cultures_list']);
$cancel             = $miscObject->CleanFormData($_REQUEST['cancel']);

$farmersList        = $miscObject->CleanFormData($_REQUEST['farmers_list']);

$form_type          = $miscObject->CleanFormData($_REQUEST['type_f']);

$type               = $miscObject->CleanFormData($_REQUEST['type']);

// ПЕРЕМЕННЫЕ ДЛЯ СОХРАНЕНИЯ КОНТУРОВ ЗА ХОЗЯЙСТВОМ
$ids       = $miscObject->CleanFormData($_REQUEST['ids']);
//-------------------------------------------------------------//

// ПЕРЕМЕННЫЕ ДЛЯ УДАЛЕНИЯ КОНТУРОВ ИЗ ХОЗЯЙСТВА
$removeFieldStatus = $miscObject->CleanFormData($_REQUEST['rm']);
//-------------------------------------------------------------//

// ИДЕНТИФИКАТОР, ПО КОТОРОМУ ОТОБРАЖАЕТСЯ СТАТИСТИКА NDVI ПО КОНТУРУ В VEGA-PRO
// ДЛЯ LANDSAT-8 И SENTINEL-2A СТАТИСТИКА ВЫБИРАЕТСЯ ПО gid
$gid = $miscObject->CleanFormData($_REQUEST['gid']);
$ndvi_year = $miscObject->CleanFormData($_REQUEST['ndvi_year']);
$ndvi_src = $miscObject->CleanFormData($_REQUEST['ndvi_src']);
//-------------------------------------------------------------//

// ЗАПРОС НА ПОЛУЧЕНИЕ ВСЕХ NDVI ДАТ
$ndvi_date = $miscObject->CleanFormData($_REQUEST['ndvidate']);

// ID текущего хозяйства. Глобальная переменная, доступна везде в этом коде.
$idF = $_SESSION['idFarmer'];

$popupFormsObject = new PopupForms();

// ИЗМЕНИТЬ В БД ОТРЕДАКТИРОВАННЫЕ ДАННЫЕ ВЫБРАННОГО ОБЪЕКТА СЛОЯ И ОТОБРАЗИТЬ ОБНОВЛЕННЫЕ ДАННЫЕ ИЗ БД
/*if ( !empty($gid) && !empty($data) ) {
    $result_update = $popupFormsObject->popupModelObject->UpdateLayerDataById($gid, $data);
    $popupFormsObject->DisplayObjectDataForm($gid);
}*/

// ОТОБРАЗИТЬ ДАННЫЕ ВЫБРАННОГО ОБЪЕКТА СЛОЯ
if ( !empty($showDataByGid) ) {
    $popupFormsObject->DisplayObjectDataForm($showDataByGid);
}

// ОТОБРАЗИТЬ ДАННЫЕ ВЫБРАННОГО ОБЪЕКТА СЛОЯ В ФОРМАТЕ РЕДАКТИРОВАНИЯ
if ( !empty($changeDataID) ) {
    $popupFormsObject->DisplayObjectDataChangeForm($changeDataID);
}

// ВОЗВРАТ К ГЛАВНОМУ МЕНЮ
if ( !empty($cancel) && $cancel == true ) {
    $popupFormsObject->Cancel();
    //$popupFormsObject->DisplayObjectDataForm();
}
    
// ОТОБРАЗИТЬ ВЫПАДАЮЩИЙ СПИСОК С ФЕРМЕРСКИМИ ХОЗЯЙСТВАМИ
if  ( !empty($farmersList) && $farmersList == 'farmers' ) {
    $popupFormsObject->DisplayFarmersForm($miscObject->CleanFormData($idF));
}

// ОТОБРАЗИТЬ ФОРМУ ВЫБОРА ВВОДА ПЛАНА|ФАКТА
if ( !empty($culturesList) && $culturesList == 'cultures' ) {
    $popupFormsObject->ChoiseCulturesFormType();
}

// ЗАДАЕМ ХОЗЯЙСТВО ВЫБРАННЫМ КОНТУРАМ ПОЛЕЙ
if ( isset($_POST['someString']) && !empty($_POST['someString']) ) { 
    
    $miscObj = new Misc();
    $dataField = array();
    
    $someString = $miscObj->CleanFormData($_POST['someString']);
    
    $stringsArray = explode("||", $someString);
    
    for ( $i=0; $i<count($stringsArray); $i++ )
    {
        $dataString = str_replace("|", "", $stringsArray[$i]);
        $arr = explode(", ", $dataString);
        foreach ( $arr as $str )
        {
            list($key, $val) = explode(":", $str); 
            $dataField[$key] = $val;
        }
        
        if ( isset($dataField['fid']) && !empty($dataField['fid']) )
        {
            // Получаем всю информацию о поле.
            $r_fieldInfo = $popupFormsObject->popupModelObject->SelectLayerDataById($dataField['fid']);
            $fieldInfo = pg_fetch_array($r_fieldInfo, null, PGSQL_ASSOC);
            
            if ( isset($fieldInfo['owner']) && !empty($fieldInfo['owner']) )
            {
                echo "<script type='text/javascript'>
                        $.stickr({note:'<p>Поле <b>[№=" . $fieldInfo['id'] . "]</b> уже закреплено за <b>" . $fieldInfo['owner'] . "</b></p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
                      </script>";
            }
            else
            {
                // задаем хозяйство
                $popupFormsObject->farmersModelObject->SetFarmer($dataField['fid'], $_SESSION['farmerName'], $idF);
                // Сохраняем поле и его Bbox в таблицу farmers_fields. 
                $popupFormsObject->popupModelObject->SetBboxOfFieldInFarmer($idF, $dataField['fid'], $dataField['minLat'], $dataField['minLng'], $dataField['maxLat'], $dataField['maxLng']); 

                echo "<script type='text/javascript'>
                        $.stickr({note:'Поле <b>[№=" . $fieldInfo['id'] . "]</b> закреплено за хозяйством <b>" . $_SESSION['farmerName'] . "</b>',className:'sticker sticker-success', time:3000, speed:2000, position:{right:320,top:98}});
                      </script>";
            }
        }
    }
    $popupFormsObject->Cancel();
}

// ЗАДАТЬ КУЛЬТУРУ ВЫБРАННЫМ КОНТУРАМ ПОЛЕЙ
if ( !empty($ids) && !empty($cultureval) && !empty($type) && !empty($fid) ) {
    
    $miscObj = new Misc();
    
    //echo "<br>" . $ids . " " . $cultureval . " " . $type . " " . $fid . "<br>";
    
    // Формируем массив ID's выбранных полей.
    $idsArray = $miscObj->IdsStringToArray($ids);
    
    if ( $cultureval != 'Выберите культуру ...' )
    {
        $dataset['id_field_name'] = 'gid';
        
        // Получаем ID культуры по ее названию.
        $resultCulture = $popupFormsObject->culturesModelObject->GetCultureInfoByName($cultureval);
        $rowCulture = pg_fetch_array($resultCulture, null, PGSQL_ASSOC);
        $id_culture = trim($rowCulture['id_culture']);
        
        // В плане всегда INSERT
        if ( $type == 'plan' )
        {
            // Подготавливаем набор данных ($dataset) для обновления таблицы слоя полей.
            $dataset['fields'] = array('culture_pl' => $cultureval);
            
            // Подготавливаем набор данных ($dataset_history) для вставки в истории планирования культур.
            $dataset_history['plan_fields'] = array(
                                            'plan_date' => $miscObj->getCurrentDate(), 
                                            'plan_id_culture' => $id_culture, 
                                            'id_farmer' => $fid, 
            );
        }
        // В факте всегда UPDATE
        if ( $type == 'fact' )
        {
            $dataset['fields'] = array('culture_fa' => $cultureval);
            $dataset_history['fact_fields'] = array(
                                            'fact_date'       => $miscObj->getCurrentDate(),
                                            'fact_id_culture' => $id_culture,
                                            'id_farmer' => $fid, // добавил 
            );
        }
        
        foreach($idsArray as $id)
        {
            $dataset['id_field_value'] = $id;
            if ( $type == 'plan' ) { $dataset_history['plan_fields']['id_field'] = $id; }
            if ( $type == 'fact' ) { $dataset_history['fact_fields']['id_field'] = $id; }
            
            // Проверяем принадлежит ли поле, которому задаем культуру данному хозяйству. Для этого
            // Получаем всю информацию о поле.
            $r_fieldInfo = $popupFormsObject->popupModelObject->SelectLayerDataById($id);
            $fieldInfo = pg_fetch_array($r_fieldInfo, null, PGSQL_ASSOC);
            
            // Если поле принадлежит данному хозяйству, то
            if ( !empty($fieldInfo['owner']) && $fieldInfo['owner'] == $_SESSION['farmerName'] ) {
                // сохраняем название культуры плана|факта в таблице слоя полей,
                $popupFormsObject->culturesModelObject->SetCulture($dataset);
                // сохраняем историю планирования в таблицу plan_fact и 
                $popupFormsObject->historyModelObject->SetHistorySeva($dataset_history);
                // выводим информацию какому полю какая культура присвоена.
                echo "<script type='text/javascript'>
                    $.stickr({note:'Полю <b>[№=" . $fieldInfo['id'] . "]</b> указана культура <b>" . $cultureval . "</b>',className:'sticker sticker-success', time:3000, speed:2000, position:{right:320,top:98}});
                  </script>";
            }
            // Если пытаемся присвоить культуру полю, которое не принадлежит данному хозяйству, то информируем пользователя.
            else {
                echo "<script type='text/javascript'>
                    $.stickr({note:'<p>Поле <b>[№=" . $fieldInfo['id'] . "]</b> закреплено за другим хозяйством: <b>" . $fieldInfo['owner'] . "</b></p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
                  </script>";
            }
        }
        // Возвращаемся в главное меню.
        $popupFormsObject->Cancel();
    }
    else 
    {
        echo "<script type='text/javascript'>
                    $.stickr({note:'<p>Выберите культуру из списка!</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
                  </script>";
        $popupFormsObject->DisplayCulturesForm($type);
    }
}

// УБРАТЬ ПОЛЕ ИЗ ХОЗЯЙСТВА
if ( !empty($removeFieldStatus) && $removeFieldStatus == 1 ) {
    $miscObj = new Misc();
    
    $idsArray = $miscObj->IdsStringToArray($ids);
    // Проверяем принадлежит ли поле текущему хозяйству
    foreach($idsArray as $id)
    {
        // Получаем всю информацию о поле.
        $r_fieldInfo = $popupFormsObject->popupModelObject->SelectLayerDataById($id);
        $fieldInfo = pg_fetch_array($r_fieldInfo, null, PGSQL_ASSOC);
        
        if ( $fieldInfo['owner'] == $_SESSION['farmerName'] )
        {
            // Убираем поле из хозяйства
            $popupFormsObject->popupModelObject->RemoveFieldFromFarmer($id);
            // Убираем BBOX поля из таблицы farmers_fields.
            $popupFormsObject->popupModelObject->RemoveBboxOfFieldFromFarmer($id);
            echo "<script type='text/javascript'>
                    $.stickr({note:'Поле <b>[№=" . $fieldInfo['id'] . "]</b> удалено из хозяйства <b>" . $fieldInfo['owner'] . "</b>',className:'sticker sticker-success', time:3000, speed:2000, position:{right:320,top:98}});
                  </script>";
        }
        else
        {
            echo "<script type='text/javascript'>
                    $.stickr({note:'<p>Поле <b>[№=" . $fieldInfo['id'] . "]</b> невозможно удалить, т.к. оно закреплено за другим хозяйством <b>" . $fieldInfo['owner'] . "</b></p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
                  </script>";
        }
    }
    $popupFormsObject->Cancel();
}

// ОТОБРАЗИТЬ ФОРМУ ВВОДА ПЛАНА|ФАКТА
if ( isset($form_type) && !empty($form_type) ) {
    $popupFormsObject->DisplayCulturesForm($form_type);
}

// ОТОБРАЗИТЬ ГРАФИК NDVI УКАЗАННОГО КОНТУРА
if ( isset($gid) && !empty($gid) ) {
    echo '<div class="dialog-content">';
    echo "Идентификатор поля = ".$gid."<br>";
    if ( !empty($ndvi_src) && !empty($ndvi_year)  ) {
        
        $ndvi_src = explode(',', $ndvi_src);
        $ndvi_src = array_diff($ndvi_src, array(''));
        
        if ( stripos($ndvi_year, ',') !== false ) {
            $yearStringVega = $ndvi_year;
            $ndvi_year = explode(',', $ndvi_year);
            $startYear = array_shift($ndvi_year);
            $endYear = array_pop($ndvi_year);
        }
        else {
            $yearStringVega = $ndvi_year;
            $startYear = $ndvi_year;
        }

        foreach ( $ndvi_src as $val ) {
            if ( $val == "Landsat-8" ) {
                // Берем NDVI, посчитанный по Landsat-8 из БД
                $result1 = $popupFormsObject->popupModelObject->GetNDVIStatistic($gid, $val, $startYear, $endYear);
            }
            if ( $val == "Sentinel-2A" ) {
                // Берем NDVI, посчитанный по Sentinel из БД
                $result2 = $popupFormsObject->popupModelObject->GetNDVIStatistic($gid, $val, $startYear, $endYear);
            }
            if ( $val == "Vega-PRO" ) {
                // Формируем запрос к Vege
                $result3 = $popupFormsObject->vegaModel_Object->GetVegaNDVI($gid, $yearStringVega);
                if ( $result3 == false ) {
                    echo "<script type='text/javascript'>
                            $.stickr({note:'<p>Поле <b>[№=" . $gid . "]</b> не существует в системе Vega-PRO</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
                        </script>";
                }
            }
        }
        if ( !empty($result1) || !empty($result2) || !empty($result3) ) {
            // Отображаем график
            $popupFormsObject->DisplayNDVIChart($result1, $result2, $result3);
        }
    }
    else {
        echo "<script type='text/javascript'>
                $.stickr({note:'<p>Выберите источник(и) данных и год или диапазон лет</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
            </script>";
    }
}

// ПОЛУЧИТЬ ВСЕ ДАТЫ NDVI
if ( !empty($ndvi_date) && $ndvi_date == 1 ) {
	$result = $popupFormsObject->popupModelObject->GetNDVIDates();
	
	$array = pg_fetch_all($result);

	echo json_encode($array);
}
?>