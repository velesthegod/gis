<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class PopupModel extends DB
{
    protected $SCHEME  = 'data';
    protected $DB_NAME = 'inf_region';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    /**
     * @description Метод UpdateLayerDataById() обновляет отредактированные атрибуты выбранного контура поля
     * @param $table string название таблицы
     * @param $id int Идентификатор контура поля
     * @param $data array Массив атрибутов контура поля
     * @return Object
     */
     public function UpdateLayerDataById($id, $data)
     {
         $i = 0;
         $zpt = ", ";
         $data = array_filter($data);
         
         $query = "UPDATE " . $this->SCHEME . "." . $this->FIELDS_TABLE . " ";  
         $query .= "SET ";
         
         foreach ( $data as $key=>$val )
         {
             $i++;
             if ( $i == count($data)  ) {
                 $zpt = '';
             }
             
             if ( is_string($val) ) {
                 $query .= $key ." = '". $val . "'" . $zpt;
             }
             if ( is_float($val) ) {
                 $query .= $key ." = ". $val . $zpt;
             }
             if ( is_int($val) ) {
                 $query .= $key ." = ". $val . $zpt;
             }
             if ( is_bool($val) ) {
                 $query .= $key ." = ". $val . $zpt;
             }
         }
         $query .= " WHERE gid = " .$id. "; ";
         //echo $query;
         $result = pg_query($this->id_connect, $query);
        
         return $result;
     }
     
     /**
     * @description Метод SelectLayerDataById() выводит атрибуты выбранного контура поля
     * @param $id int Идентификатор контура поля
     * @return Object
     */
     public function SelectLayerDataById($id)
     {
         $query = "SELECT * FROM " . $this->SCHEME . "." . $this->FIELDS_TABLE;
         $query .= " WHERE gid = " . $id . ";";
         
         $result = pg_query($this->id_connect, $query);
        
         //echo $query;
         return $result;
     }
     
     /*public function SelectLayerDataBy__gid($__gid)
     {
         $query = "SELECT * FROM " . $this->SCHEME . "." . $this->FIELDS_TABLE;
         $query .= " WHERE __gid = " . $__gid . ";";
         
         $result = pg_query($this->id_connect, $query);
        
         //echo $query;
         return $result;
     }*/
     
     /**
     * @description Метод SelectDictionaryData() выводит данные из справочников
     * @param $table string название таблицы
     * @return Object
     */
     public function SelectDictionaryData($table, $orderField, $idFarmer = "")
     {
         $query = "SELECT * FROM " . $this->SCHEME . "." . $table . " ";
         if ( isset($idFarmer) && !empty($idFarmer) ) {
             $query .= "WHERE id_farmer = " . $idFarmer . " ";
         }
         if ( !empty($orderField) ) {
             $query .= "ORDER BY " . $orderField . " ASC ";
         }
         
         $query .= ";";
         
         $result = pg_query($this->id_connect, $query);
        
         //echo $query;
         return $result;
     }
     
     
     /*public function GetLayerBbox($idFarmer)
     {
         $query = "SELECT * FROM " . $this->SCHEME . "." . $this->LAYER_BBOX_TABLE . " ";
         
         $query .= "WHERE id_farmer = " . $idFarmer . ";";
         
         $result = pg_query($this->id_connect, $query);
        
         //echo $query;
         return $result;
     }*/
     
     
     
     /*public function SetLayerBbox($idFarmer, $minLat, $minLng, $maxLat, $maxLng)
     {
         // Проверим существует ли уже запись в таблице layer_bbox с таким idFarmer
         // 1. делаем select по idFarmer
         $farmerBbox = $this->GetLayerBbox($idFarmer);
         $row = pg_fetch_array($farmerBbox, null, PGSQL_ASSOC);
         
         // Если существует
         if ( isset($row) && !empty($row) )
         {
             // 2. сравниваем хранящиеся координаты с полученными
             if ( $row['minlat'] > $minLat ) {
                 $row['minlat'] = $minLat;
             }
             if ( $row['minlng'] > $minLng ) {
                 $row['minlng'] = $minLng;
             }
             if ( $row['maxlat'] < $maxLat ) {
                 $row['maxlat'] = $maxLat;
             }
             if ( $row['maxlng'] < $maxLng ) {
                 $row['maxlng'] = $maxLng;
             }
             
             // 3. делаем update координат по idFarmer
             $query = "UPDATE " . $this->SCHEME . "." . $this->LAYER_BBOX_TABLE . 
                      " SET minlat=" . $row['minlat'] . ", minlng=" . $row['minlng'] . ", maxlat=" . $row['maxlat'] . ", maxlng=" . $row['maxlng'] . 
                      " WHERE id_farmer=" . $idFarmer . ";";
         }
         // Если не существует, то делаем insert
         else
         {
             $query = "INSERT INTO " . $this->SCHEME . "." . $this->LAYER_BBOX_TABLE . 
                      " ( id_farmer, minlat, minlng, maxlat, maxlng ) " . 
                      "VALUES (" . $idFarmer . ", " . $minLat . ", " . $minLng . ", " . $maxLat . ", " . $maxLng . ");";
         }
         
         $result = pg_query($this->id_connect, $query);
        
         //echo $query;
         return $result;
     }*/

     public function SetBboxOfFieldInFarmer($idFarmer, $idField, $minLat, $minLng, $maxLat, $maxLng)
     {
         $query = "INSERT INTO " . $this->SCHEME . "." . $this->FARMERS_FIELDS_TABLE . 
                  " ( id_farmer, id_field, minlat, minlng, maxlat, maxlng ) " . 
                  "VALUES (" . $idFarmer . ", " . $idField . ", " . $minLat . ", " . $minLng . ", " . $maxLat . ", " . $maxLng . ");";
         $result = pg_query($this->id_connect, $query);
         return $result;
     }
     
     public function RemoveBboxOfFieldFromFarmer($idField)
     {
         $query = "DELETE FROM " . $this->SCHEME . "." . $this->FARMERS_FIELDS_TABLE . 
                  " WHERE id_field = " . $idField . ";";
         $result = pg_query($this->id_connect, $query);
         return $result;
     }
     
     /**
     * @description Метод RemoveFieldFromFarmer() убрать поле из хозяйства
     * @param $idField integer Идентификатор поля
     * @return bool
     */
     public function RemoveFieldFromFarmer($idField)
     {
         $query = "UPDATE " . $this->SCHEME . "." . $this->FIELDS_TABLE;
         $query .= " SET owner=NULL, id_owner=0 ";
         $query .= "WHERE gid=" . $idField . ";";
         
         $result = pg_query($this->id_connect, $query);
         
         return $result;
     }
     
     /**
     * @description Метод GetNDVIStatistic() - получить статистику NDVI по контуру
     * @param $__gid integer Идентификатор поля с двойным подчеркиванием
     * @return Object
     */
     public function GetNDVIStatistic($__gid, $satellite_type, $ndvi_year_start, $ndvi_year_end)
     {
         // AND DATE_PART('YEAR', ndvi_year) >= 2015 AND DATE_PART('YEAR', ndvi_year) <= 2016
         $query = "SELECT * FROM " . $this->SCHEME . "." . $this->A_NDVI_STATS;
         $query .= " WHERE field_uniq_gid = " . $__gid . " AND satellite_type='$satellite_type'";
         
         if ( !empty($ndvi_year_start) && empty($ndvi_year_end) ) {
             $query .= " AND DATE_PART('YEAR', ndvi_year) = " . $ndvi_year_start;
         }
         
         if ( !empty($ndvi_year_start) && !empty($ndvi_year_end) ) {
             $query .= " AND DATE_PART('YEAR', ndvi_year) >= " . $ndvi_year_start . " AND DATE_PART('YEAR', ndvi_year) <= " . $ndvi_year_end;
         }
         
         $query .= " ORDER BY ndvi_year ASC, ndvi_day_number ASC;";
         
         // echo $query;
         
         $result = pg_query($this->id_connect, $query);
        
         return $result;
     }
     
	 /**
     * @description Метод GetNDVIHistory() - получить историю измененния NDVI по контуру или по всему слою
     * @param $date datetime Дата в формате YYYY-MM-DD, за которую требуется получить значения NDVI
     * @param $gid integer. Default value is NULL. Идентификатор объекта, по которому требуется получить значение NDVI
     * @return Array
     */
     public function GetNDVIHistory($date, $gid = NULL)
     {
         $query = "SELECT DISTINCT c.gid, ST_AsKML(c.geom), n.ndvi_year, n.ndvi_value ";
         //$query = "SELECT DISTINCT c.gid, ST_AsGeoJSON(c.geom), n.ndvi_year, n.ndvi_value ";
         $query .= "FROM " . $this->SCHEME . "." . $this->FIELDS_TABLE . " c ";
         $query .= "INNER JOIN " . $this->SCHEME . "." . $this->A_NDVI_STATS . " n ON c.gid = n.field_uniq_gid ";
         $query .= "WHERE n.ndvi_year = '$date' AND ndvi_value >=0 ";
         if ( isset($gid) && $gid != NULL ) {
             $query .= "AND n.field_uniq_gid = " . $gid ;
         }
         $query .= "ORDER BY c.gid;";
         
         $result = pg_query($this->id_connect, $query);
         return $result;
     }

	 /**
     * @description Метод GetNDVIDates() - получить список дат, в которые рассчитывался NDVI
     * @return Array
     */
	 public function GetNDVIDates()
	 {
		$query = "SELECT DISTINCT ndvi_year ";
		$query .= "FROM " . $this->SCHEME . "." . $this->A_NDVI_STATS . " ";
		$query .= "ORDER BY ndvi_year;";
		
		$result = pg_query($this->id_connect, $query);
        return $result;
	 }
}
?>
