<?php
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Map/Form/MapForm.php");
require_once("../Model/PopupModel.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/History/Model/HistoryModel.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Cultures/Model/CulturesModel.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Farmers/Model/FarmersModel.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Vega/Model/VegaModel.php");

class PopupForms extends MapForm
{
    public $popupModelObject;
    public $historyModelObject;
    public $culturesModelObject;
    public $farmersModelObject;
    public $vegaModel_Object;
    
    public function __construct()
    {
        $this->popupModelObject = new PopupModel();
        $this->historyModelObject = new HistoryModel();
        $this->culturesModelObject = new CulturesModel();
        $this->farmersModelObject = new FarmersModel();
        $this->vegaModel_Object = new VegaModel();
    }
    
    /**
    * @description Метод DisplayObjectDataForm() - отобразить форму с данными объекта (контура)
    * 
    * @return Form
    */
    public function DisplayObjectDataForm($gid)
    {
        $result_select = $this->popupModelObject->SelectLayerDataById($gid);
        ?>
        <div id="div-popup-form">
            <div id="box-object-params" style="width:300px; height:auto;">
                <form>
                <?
                while ($row = pg_fetch_array($result_select, null, PGSQL_ASSOC))  { ?>
                        <p style="color:#2C87D2; font-size:12pt;"><?=$row['owner']?></p>
                        <p id="obj-fid"><b>Идентификатор: </b><?=$row['gid']?><p>
                        <p id="obj-owner"><b>Хозяйство: </b><?=$row['owner']?></p>
                        <p id="obj-area-giv"><b>Площадь введенная: </b><?=$row['area_giv']?> Га</p>
                        <p id="obj-area-calc"><b>Площадь расчитанная: </b><?=$row['area_calc']?> Га</p>
                        <p id="obj-culture-2013"><b>Культура в 2013 г.: </b><?=$row['culture_13']?></p>
                        <p id="obj-culture-plan-2014"><b>Культура запланированная в 2014 г.: </b><?=$row['cult14plan']?></p>
                        <p id="obj-culture-2014"><b>Культура в 2014 г.: </b><?=$row['culture_14']?></p>
                        <p id="obj-culture-2015"><b>Культура в 2015 г.: </b><?=$row['culture_15']?></p>
                        <p id="obj-data-seva"><b>Дата сева: </b><?=$row['data_seva']?></p>
                        <button type="button" class="cancel" onclick="Cancel()">Назад</button>
                        <button type="button" class="change" onclick="ChangeAttribs()" id="attrib-change">Изменить</button>
                <? } ?>
                </form>
            </div>
        </div>
        <?
    }
    
    /**
    * @description Метод DisplayObjectDataForm() - отобразить форму с данными объекта для редактирования в POPUP окне
    * 
    * @return Form
    */
    public function DisplayObjectDataChangeForm($gid)
    {
        $result_select = $this->popupModelObject->SelectLayerDataById($gid);
        ?>
        <script type="text/javascript">
            $("#data_seva").datepicker({
              monthNames:["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
              dayNamesMin:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
              firstDay:1,
              dateFormat:"dd.mm.yy"
           });
        </script>

        <form id="object-info-form">
            <fieldset>
                <legend>Информация о объекте(ах)</legend>
                <div id="box-object-params" style="width:300px; height:auto;">
                <?
                while ($row = pg_fetch_array($result_select, null, PGSQL_ASSOC))  { ?>
                    <p id='obj-owner'><b>Хозяйство: </b><br><?self::DisplayFarmersList($row['owner'])?></p>
                    <p id='obj-area-giv'><b>Площадь введенная: </b><br><input type='text' id='area_giv' value='<?=$row['area_giv']?>' style='width:200px; height:15px;'>Га</p>
                    <p id='obj-area-calc'><b>Площадь расчитанная: </b><br><input type='text' id='area_calc' value='<?=$row['area_calc']?>' style='width:200px; height:15px;'>Га</p>
                    <p id='obj-culture-2013'><b>Культура в 2013 г.: </b><br><?self::DisplayCulturesList($row['culture_13'], 'culture_13');?></p>
                    <p id='obj-culture-plan-2014'><b>Культура запланированная в 2014 г.: </b><br><?self::DisplayCulturesList($row['cult14plan'], 'cult14plan');?></p>
                    <p id='obj-culture-2014'><b>Культура в 2014 г.: </b><br><?self::DisplayCulturesList($row['culture_14'], 'culture_14');?></p>
                    <p id='obj-culture-2015'><b>Культура в 2015 г.: </b><br><?self::DisplayCulturesList($row['culture_15'], 'culture_15');?></p>
                    <p id='obj-data-seva'><b>Дата сева: </b><br><input type='text' value='<?=$row['data_seva']?>' id='data_seva' style='width:200px; height:15px;'></p>
                    
                    <button type="button" class="cancel" onclick="Cancel()" id="attrib-save">Отмена</button>
                    <button type="button" class="save" onclick="SaveAttribs()" id="attrib-save">Сохранить</button>
                <? } ?>
                </div>
            </fieldset>
        </form>
        <?
    }
    
    public function DisplayObjectDataChangeForm2($nameEng_Value, $nameRus_Value)
    {
        $result = $this->popupModelObject->SelectLayerDataById($nameEng_Value['gid']);
        $row = pg_fetch_array($result, null, PGSQL_ASSOC);
        foreach( $nameEng_Value as $nameEng=>$value )
        {
            foreach ( $row as $key=>$val )
            {
                //if (  )
                echo $row[$nameEng].' = '.$val.'<br>';
            }
            //echo $row[$nameEng] . '<br>';
        }

        
    }
    
    /**
    * @description Метод DisplayFarmersForm() - отобразить форму выбора хозяйства
    * @param integer $idFarmer. Default empty. Для отображения в списке хозяйств только текущего хозяйства.
    * @return Form
    */
    public function DisplayFarmersForm($idFarmer = "")
    {
        $result = $this->popupModelObject->SelectDictionaryData($this->popupModelObject->FARMERS_TABLE, 'f_name', $idFarmer);
        ?>
        <div id="div-popup-form">
            <div id="group1">
                <!--Форма ввода плана-->
                <div>
                    <p class="c-form-title">Закрепить за хозяйством</p>
                </div>
                <div class="c-form">
                    <div id="div-farmers-list" style="width:300px; height:auto; margin-bottom: 5px;">
                        <b>Выберите хозяйство:</b>
                        <select id="select-farmers-list" style="width: 250px; height: 25px; margin: 3px 0px 5px 0px;">
                            <option>Выберите хозяйство ...</option>
                    <?
                    while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) { ?>
                        
                            <option value="<?=$row['f_name']?>"><?=$row['f_name']?></option>
                        
                    <? } ?>
                        </select>
                    </div>
                    <div style="margin: 110px 0px 0px 0px;">
                        <button type="button" class="cancel" onclick="Cancel()">Отмена</button>
                        <button type="button" class="save" onclick="SetObjectsAsFarmer()">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
        <?
    }
    
    /**
    * @description DisplayFarmersList() - отобразить список хозяйств
    * 
    * @return Select element
    */
    public function DisplayFarmersList($currentVal)
    {
        $result = $this->popupModelObject->SelectDictionaryData($this->popupModelObject->FARMERS_TABLE, 'f_name');
        ?>
        <div id="div-popup-form">
                <select id='owner' style="width: 214px; height: 25px; margin: 0px 0px 0px 0px;">
                    <? if ( isset($currentVal) && !empty($currentVal) ) { ?>
                           <option value="<?=$currentVal?>"><?=$currentVal?></option>
                    <? } else { ?>
                           <option>Выберите хозяйство ...</option>
                    <? }

               while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) { ?>
                
                    <option value="<?=$row['f_name']?>"><?=$row['f_name']?></option>
                
            <? } ?>
                </select>
        </div>
        <?
    }
    
    /**
    * @description DisplayCulturesForm() - отобразить форму выбора культур (план или факт в зависимости от указанного типа)
    * @param string $type. Тип формы plan|fact
    * @param integer $idFarmer. Default empty. Идентификатор текущего хояйства. Используется для записи в историю севооборота хозяйства.
    * @return Form
    */
    public function DisplayCulturesForm($type)
    {
        $result = $this->popupModelObject->SelectDictionaryData($this->popupModelObject->CULTURES_TABLE, 'c_name');
        ?>
        <div id="div-popup-form">
            <div id="group1">
                <!--Форма ввода плана-->
                <div>
                    <p class="c-form-title"><? if ( $type == 'plan' ) echo "Указать план"; else echo "Указать факт"; ?></p>
                </div>
                <div class="c-form">
                    <div id="div-cultures-list" style="width:300px; height:auto; margin-bottom: 5px;">
                        <b>Выберите культуру:</b>
                        <select id="select-cultures-list" style="width: 250px; height: 25px; margin: 3px 0px 5px 0px;">
                            <option>Выберите культуру ...</option>
                        <?
                        while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) { ?>
                            
                            <option value="<?=$row['c_name']?>"><?=$row['c_name']?></option>
                            
                        <? } ?>
                        </select>
                    </div>
                    <div style="margin: 110px 0px 0px 0px;">
                        <button type="button" class="cancel" onclick="Cancel()">Отмена</button>
                        <? if ( $type == 'plan' ) { ?>
                            <button type="button" class="save" onclick="SetCulture('plan');">Сохранить</button>
                        <? } else { ?>
                               <button type="button" class="save" onclick="SetCulture('fact');">Сохранить</button>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
        <?
    }
    
    /**
    * @description DisplayCulturesList() - отобразить список культур
    * 
    * @return Select element
    */
    public function DisplayCulturesList($currentVal, $fieldname)
    {
        $result = $this->popupModelObject->SelectDictionaryData($this->popupModelObject->CULTURES_TABLE, 'c_name');
        ?>
        <div id="div-popup-form">
                <select id='<?=$fieldname?>' style="width: 214px; height: 25px; margin: 0px 0px 0px 0px;">
                    <? if ( isset($currentVal) && !empty($currentVal) ) { ?>
                           <option value="<?=$currentVal?>"><?=$currentVal?></option>
                    <? } else { ?>
                           <option>Выберите культуру ...</option>
                    <? }
            
                while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) { ?>
                
                    <option value="<?=$row['c_name']?>"><?=$row['c_name']?></option>
                
            <? } ?>
                </select>
        </div>
        <?
    }
    
    /**
    * @description ChoiseCulturesFormType() - отобразить форму выбора ввода плановых или фактических культур
    * 
    * @return Form
    */
    public function ChoiseCulturesFormType()
    {
        ?>
        <script type="text/javascript">
            
            $(".plan-form").click(function(){
                var send = "type_f=plan";
                $.ajax({
                    url:"Modules/Popup/View/popup-view.php",
                    data:send,
                    success:function(result){
                        $("#div-popup-form").replaceWith(result);
                    }
                });
            });
            
            $(".fact-form").click(function(){
                var send = "type_f=fact";
                $.ajax({
                    url:"Modules/Popup/View/popup-view.php",
                    data:send,
                    success:function(result){
                        $("#div-popup-form").replaceWith(result);
                    }
                });
            });
        </script>
        
        <div id="div-popup-form">
            <div id="group1">
                <!--Форма ввода плана-->
                <div>
                    <button type="button" class="plan-form">Указать план</button>
                </div>
                <!--Форма ввода факта-->
                <div>
                    <button type="button" class="fact-form">Указать факт</button>
                </div>
                <div style="margin: 110px 0px 0px 0px;">
                    <button type="button" class="cancel" onclick="Cancel()">Назад</button>
                </div>
            </div>
        </div>
        <?
    }
    
    /**
    * @description Cancel() - отмена или возврат к главной форме
    * 
    * @return Form
    */
    public function Cancel()
    {
        ?>
        <div id="div-popup-form">
            <button type="button" onclick="ShowFarmers()" id="set-farm">Закрепить за хозяйством</button> 
            <button type="button" onclick="RemoveFieldFromFarmer()" id="unset-farm">Убрать из хозяйства</button> 
            <button type="button" onclick="ShowCultures()" id="set-cultures">Указать культуру</button>
        </div>
        <?
    }
    
    /**
    * @description DisplayNDVIDiagram() - отобразить график NDVI
    * @param $ndviArray Object Набор значений NDVI из таблицы a_ndvi_stats
    * @return Form
    */
    public function DisplayNDVIDiagram($ndviArray)
    {
        $pointSet = 'data.addRows([';
        while ($row = pg_fetch_array($ndviArray, null, PGSQL_ASSOC)) 
        {
            $ndviDate = date("Y, m, d", mktime(0, 0, 0, 0, $row['ndvi_day_number'], $row['ndvi_year']));
            $pointSet .= '[' . 'new Date('.$ndviDate.'), ' . $row['ndvi_value'] . '],';
        }
        $pointSet .= ']);';
        ?>
        
        <div class="dialog-content">    
            <script type="text/javascript">
            google.charts.load('current', {packages: ['corechart', 'line']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                  var data = new google.visualization.DataTable();
                  data.addColumn('date', 'Дата');
                  data.addColumn('number', 'NDVI');
                  
                  <?=$pointSet?>

                  var options = {
                    hAxis: {
                      title: 'Дата',
                      format: 'yy-M-d',
                      gridlines: {count: 15}
                    },
                    vAxis: {
                      title: 'NDVI',
                      gridlines: {color: 'none'},
                      minValue: 0
                    },
                    width: 900,
                    height: 500,
                    backgroundColor: '#ffffff'
                  };

                  var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                  //var chart = new google.charts.Line(document.getElementById('chart_div'));  // показывает точные значения, а не диапазоны.
                  chart.draw(data, options);
                }
            </script>
            
            <div id="chart_div" style="height: 700px; width: auto;"></div>
        </div>
    <?
    }
    
    public function DisplayNDVIChart($result1='', $result2='', $result3='')
    {
        // Получения даты по номеру дня в году
        // $ndviDate = date("m/d/Y", mktime(0, 0, 0, 1, $row['ndvi_day_number'], $row['ndvi_year']));
        // $pointSet .= '["'.$ndviDate.'",' . $row['ndvi_value'] . '],';
        
        $plot = "plot1 = $.jqplot('chart1', [";
        
        $POINTS = "";
        $goog = "";
        
        // Значения NDVI по данным Landsat-8
        if ( isset($result1) && !empty($result1) ) {
            $goog .= "goog1,";
            $pointSet1 = 'goog1 = [';
            while ($row = pg_fetch_array($result1, null, PGSQL_ASSOC)) 
            {
                $pointSet1 .= '["'.$row['ndvi_year'].'",' . $row['ndvi_value'] . '],';
            }
            $pointSet1 .= '];';
            //echo $pointSet1.'<br>';
            $POINTS .= $pointSet1;
        }
        // Значения NDVI по данным Sentinel-2A
        if ( isset($result2) && !empty($result2) ) {
            $goog .= "goog2,";
            $pointSet2 = 'goog2 = [';
            while ($row = pg_fetch_array($result2, null, PGSQL_ASSOC)) 
            {
                $pointSet2 .= '["'.$row['ndvi_year'].'",' . $row['ndvi_value'] . '],';
            }
            $pointSet2 .= '];';
            //echo $pointSet2.'<br>';
            $POINTS .= $pointSet2;
        }
        // Значения NDVI по данным сервиса Vega-PRO
        if ( isset($result3) && !empty($result3) ) {
            $goog .= "goog3,";
            $pointSet3 = 'goog3 = [';
            foreach ( $result3 as $ndvi_date => $ndvi_value )
            {
                $pointSet3 .= '["'.$ndvi_date.'",' . $ndvi_value . '],';
            }
            $pointSet3 .= '];';
            //echo $pointSet3.'<br>';
            $POINTS .= $pointSet3;
        }
        
        $plot .= $goog;
        
        $plot .= "], opts);";
        ?>
        <!--<div class="dialog-content">-->
            <!--<script src="/public/jquery/jquery.min.js" type="text/javascript"></script>-->
            <script src="/public/jqplot/js/jquery.jqplot.js" type="text/javascript"></script>
            <script src="/public/jqplot/js/jqplot.dateAxisRenderer.js" type="text/javascript"></script>
            <script src="/public/jqplot/js/jqplot.cursor.js" type="text/javascript"></script>
            <script src="/public/jqplot/js/jqplot.highlighter.js" type="text/javascript"></script>
            <link rel="stylesheet" type="text/css" href="/public/jqplot/css/jquery.jqplot.css" />
            <script type="text/javascript">
            $(document).ready(function(){
                // Enable plugins like cursor and highlighter by default.
                $.jqplot.config.enablePlugins = true;
                // For these examples, don't show the to image button.
                $.jqplot._noToImageButton = true;
             
              <?=$POINTS?>
              
              opts = {
                  title: 'Динамика вегетации (NDVI)',
                  series: [{
                      neighborThreshold: 0
                  }],
                  axes: {
                      xaxis: {
                        renderer:$.jqplot.DateAxisRenderer,
                        label:'Дата',
                        // min:'August 1, 2007',
                        // tickInterval: "6 months",
                        tickOptions:{formatString:"%Y/%#m/%#d"}
                      },
                      yaxis: {
                          renderer: $.jqplot.LogAxisRenderer,
                          label:'NDVI',
                          pad: 0,
                          rendererOptions: {
                            minorTicks: 1
                          },
                          tickOptions:{showMark: false, formatString: '%.3f', prefix: ' '} // ndvi - 
                      }
                  },
                  cursor:{zoom:true}
              };
             
              //plot1 = $.jqplot('chart1', [goog1, goog2, goog3], opts);
              <?=$plot?>
              });
            </script>
            
            <div id="chart1" style="height: 500px; width: 900px;"></div>
            <center>
                <button value="reset" type="button" onclick="plot1.resetZoom();" id="reset-zoom" style="margin-top: 20px; margin-bottom: 20px;">Показать весь график</button>
            </center>
        </div>
    <?
    }
    
}
?>
