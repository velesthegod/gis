<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class LayersModel extends DB
{
    protected $SCHEME  = 'layers';
    protected $DB_NAME = 'rcku_region';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    public function GetMetadata($id)
    {
        $query = "SELECT id_raster, path, date, satellite_name, clouds_cover, id_layer, ST_AsText(raster_bbox) as raster_bbox, encode(metadata::bytea, 'escape') as metadata, s_resolution, fname " .
                 "FROM " . $this->SCHEME . ".layers_raster_metadata " .
                 "WHERE id_layer=" . $id;
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
    
    /**
    * @description Метод GetRastersByFilter() возвращает выбранные по параметрам, заданным в фильтре, строки.
    * @param Array $data двумерный массив, содержащий названия и значения элементов, заданных на форме фильтрации.
    * @returns Array
    */
    public function GetRastersByFilter($data)
    {
        /** @TODO $data - пустая строка */
        if (!is_array($data)) {
            $data = [];
        }

        $query = "SELECT id_layer, date, satellite_name " .
                 "FROM " . $this->SCHEME . ".layers_raster_metadata ";
        if ( !empty($data) )
        {
            $query .= "WHERE ";
        }
        for ( $i=0; $i<count($data); $i++ )
        {
            if ( !empty($data[$i]['value']) )
            {
                if ( $i != count($data)-1 ) 
                {
                    $and = " AND ";
                }
                else
                {
                    $and = "";
                }
                if ( !empty($data[$i]['operator']) )
                {
                    switch ($data[$i]['operator'])
                    {
                        case 0: 
                            $operator = "=";
                            break;
                        case 1: 
                            $operator = ">";
                            break;
                        case 2: 
                            $operator = "<";
                            break;
                    }
                }
                else
                {
                    $operator = "=";
                }
                if ( $data[$i]['name'] == 'date' )
                {
                    $query .= "date between " . $data[$i]['value']['date-from'] . "::date AND " . $data[$i]['value']['date-to'] . "::date " . $and;
                }
                if ( $data[$i]['name'] == 'raster_bbox' )
                {
                    $query .= "ST_Intersects(
                                        ST_GeographyFromText('SRID=4326;' || ST_AsText(raster_bbox)),
                                        ST_GeographyFromText('SRID=4326;POLYGON((" . $data[$i]['value'] . "))')
                    )";
                }
                if ( $data[$i]['name'] == 'satellite_name' )
                {
                    $query .= $data[$i]['name'] . $operator . $data[$i]['value'] . $and;
                }
                if ( $data[$i]['name'] == 's_resolution' )
                {
                    $query .= $data[$i]['name'] . $operator . $data[$i]['value'] . $and;
                }
                if ( $data[$i]['name'] == 'clouds_cover' )
                {
                    $query .= $data[$i]['name'] . $operator . $data[$i]['value'] . $and;
                }
            }
        }
        
        $query .= " ORDER BY date DESC LIMIT 30;";
        
        //echo $query;
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
    
    public function GetSatellites()
    {
        $query = "SELECT DISTINCT satellite_name as satellite " . 
                 "FROM " . $this->SCHEME . ".layers_raster_metadata";
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
}
?>