<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class LayersDataModel extends DB
{
    protected $SCHEME  = 'data';
    protected $DB_NAME = 'inf_region';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    public function GetAttribs($layerName)
    {
        $query = "SELECT * FROM " . $this->SCHEME . "." . $layerName . " LIMIT 1;";
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
}
?>
