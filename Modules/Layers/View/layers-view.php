<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/vendor/Misc.php");
require_once("../Model/LayersDataModel.php");
require_once("../Model/LayersModel.php");

$miscObject = new Misc();

$layerName = $miscObject->CleanFormData($_REQUEST['layer']);

// ПОЛУЧИТЬ АТРИБУТЫ ВЫБРАННОГО СЛОЯ ИЗ БД
if ( isset($layerName) && !empty($layerName) ) {
    $layersDataModelObject = new LayersDataModel();
    
    $layerName = str_replace("_vw", "", $layerName);
    $layerName = str_replace("workspace:", "", $layerName);
    
    $result = $layersDataModelObject->GetAttribs($layerName);
    
    $attribs = pg_fetch_array($result, null, PGSQL_ASSOC);
    
    $array = array();
    $i = 0;
    foreach ( $attribs as $key => $val ) {
        $array[$key] = pg_field_type($result, $i);
        $i++;
    }
    echo json_encode($array);
}

// ПОЛУЧИТЬ АТРИБУТЫ ВЫБРАННОГО СЛОЯ ЧЕРЕЗ REST
// EXAMPLE: GET /layers/199/attributes?token=mbs90lon2a8
if ( !empty($_POST['layers']) && !empty($_POST['token']) ) {
        $layerId = $miscObject->CleanFormData($_POST['layers']);
        $token = $miscObject->CleanFormData($_POST['token']);
        
        // URL, по которому будет выполнен REST запрос
        $url = 'http://edu.ikit.sfu-kras.ru/layers/'.$layerId.'/attributes?token='.$token;
        
        $mycurl = curl_init($url);
        // Данные, которые будут отправлены в запросе
        $encodedData = json_encode($data);
        curl_setopt($mycurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($mycurl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($mycurl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($mycurl, CURLOPT_POSTFIELDS, $encodedData);
        echo curl_exec($mycurl);
}
// ВЫВЕСТИ СПИСОК НАИМЕНОВАНИЙ СПУТНИКОВ В ЭЛЕМЕНТ SELECT
if ( !empty($_POST['msi']) && $_POST['msi'] == true ) {
    $layersModelObject = new LayersModel();
    
    $result = $layersModelObject->GetSatellites();

    $rows = pg_fetch_all($result);
    
    echo json_encode($rows);
}
// ВЫВЕСТИ МЕТАДАННЫЕ ВЫБРАННОЙ РАСТРОВОЙ СЦЕНЫ
if ( $_POST['type'] == "rasterMetadata" ) {
    $layersModelObject = new LayersModel();

    $attribs = $_POST['attributes'];
    
    $result = $layersModelObject->GetMetadata($attribs['id']);
    
    unset($_POST);
    
    $rows = pg_fetch_all($result);
    // Преобразовать специальные символы в метафайле для корректного отображения в браузере.
    $rows[0]['metadata'] = "<pre>". htmlspecialchars($rows[0]['metadata']). "</pre>";
    
    echo json_encode($rows);
}
// ВЫВЕСТИ СПИСОК РАСТРОВЫХ СЦЕН ПО УМОЛЧАНИЮ ИЛИ ПО ФИЛЬТРУ
if ( $_POST['type'] == "Filter" ) {
    $layersModelObject = new LayersModel();
    //print_r($_POST);
    $attribs = $_POST['attributes'];
    
    $result = $layersModelObject->GetRastersByFilter($attribs);
    
    unset($_POST);
    
    $rows = pg_fetch_all($result);
    
    echo json_encode($rows);
}
?>