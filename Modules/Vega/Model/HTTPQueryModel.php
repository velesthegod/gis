<?php

class HTTPQueryModel 
{
    private static $instance;
    
    public function __construct() {}
    
    public static function getInstance() 
    {
        if(empty($instance)) 
        {
            self::$instance = new HTTPQueryModel();
        }
        return self::$instance;
    }
    
    public function getBaseUrl() 
    {
        return 'http://pro-vega.ru';
    }
    
    public static function call($slug, $params = array()) 
    {
        return HTTPQueryModel::getInstance()->callReal($slug, $params);
    }
    
    public static function getApiKey() 
    {
        return '16ebab6e2067b1423da718788e7edcf3aaf947a9';
    } 
    
    private function callReal($slug, $params = array()) 
    {
        $mycurl = curl_init();
        curl_setopt_array($mycurl, array(
            CURLOPT_URL => $this->getBaseUrl()."/".$slug,
            CURLOPT_RETURNTRANSFER => true,
        ));
        
        return (string)curl_exec($mycurl);
    }
}
