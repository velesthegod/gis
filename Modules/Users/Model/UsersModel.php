<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");
//require_once($_SERVER['DOCUMENT_ROOT']."/vendor/Misc.php");

class UsersModel extends DB
{
    public $DB_NAME = 'rcku_region';
    
    public $SCHEME = 'users';
    
    public $TABLE_USERS = 'users';
    public $TABLE_USERS_INFO = 'users_info';
    
    public $id_connect;
    
    protected $miscObject;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
        $this->miscObject = new Misc();
    }
    
    public function GetUser($login)
    {
        $query = "SELECT *, ST_AsText(location_bbox) as bbox FROM " . $this->SCHEME . "." . $this->TABLE_USERS . " u ";
        $query .= "INNER JOIN " . $this->SCHEME . "." . $this->TABLE_USERS_INFO . " ui ";
        $query .= "ON (u.id=ui.user_id) ";
        $query .= "WHERE login = '" . $login . "'";
        
        $query .= ";";
        //echo "<br><br><br>".$query;
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
    
    public function Select($columns, $table, $_where=array(), $_order=array(), $_limit=NULL, $_scheme='users')
    {
        $query = "SELECT ";
        
        $c = ', ';
        
        for ( $i=0; $i<count($columns); $i++ )
        {
            if ( $i == count($columns)-1 )
            {
                $c = '';
            }
            $query .= $columns[$i] . $c;
        }
        
        $query .= " FROM " . $_scheme . "." . $table;
        
        if ( count($_where) >= 1 )
        {
            $query .= " WHERE ";
            $and = " AND ";
            $i = 0;
            foreach ( $_where as $key => $val ) {
                if ( $i == count($_where)-1 ){
                    $and = '';
                }
                $query .= $key . " = " . $this->miscObject->GetType($val) . $and;
                $i++;
            }
        }
        
        if ( count($_order) >= 1 ) 
        {
            $query .= " ORDER BY ";
            $c = ", ";
            $i = 0;
            foreach ( $_order as $key => $val ) {
                if ( $i == count($_order)-1 ) {
                    $c = '';
                }
                $query .= $key . " " . $val . $c;
                $i++;
            }
        }
        
        if ( $_limit != NULL )
        {
            $query .= " LIMIT " . $_limit;
        }
        
        $query .= ";";
        
        //echo $query;
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
    
    public function Insert($data, $table, $_scheme='users', $_returning=array())
    {
        $query_p1 = "INSERT INTO " . $_scheme . "." . $table . " (";
        $query_p2 = "VALUES (";
        $i = 0;
        $c = ", ";
        $count = count($data);
        foreach ( $data as $key => $val ) {
            if ( $i == $count-1 ) {
                $c = ") ";
            }
            if ( $key == 'location_bbox' ) {
                $val = "polygon(box " . $this->miscObject->GetType($val) . ")::geometry";
                $query_p1 .= $key . $c;
                $query_p2 .= $val . $c;
            }
            else {
                $query_p1 .= $key . $c;
                $query_p2 .= $this->miscObject->GetType($val) . $c;
            }
            $i++;
        }
        $query = $query_p1 . $query_p2;
        
        if ( count($_returning) >= 1 ) {
            $query .= " RETURNING ";
            $c = ", ";
            for ( $i=0; $i<count($_returning); $i++ ) {
                if ( $i == count($_returning)-1 ) {
                    $c = "";
                }
                $query .= $_returning[$i] . $c;
            }
        }
        $query .= ";";
        
        //echo $query;
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
    
    public function Update($data, $table, $_where=array(), $_scheme='users')
    {
        $query = "UPDATE " . $_scheme . "." . $table;
        $query .= " SET ";
        $i = 0;
        $c = ", ";
        $count = count($data);
        foreach ( $data as $key => $val ) {
            if ( $i == $count-1 ) {
                $c = "";
            }
            $query .= $key . "=" . $this->miscObject->GetType($val) . $c;
        }
        
        if ( count($_where) >= 1 )
        {
            $query .= " WHERE ";
            $and = " AND ";
            $i = 0;
            foreach ( $_where as $key => $val ) {
                if ( $i == count($_where)-1 ){
                    $and = '';
                }
                $query .= $key . " = " . $this->miscObject->GetType($val) . $and;
                $i++;
            }
        }
        $query .= ";";
        
        //echo $query;
        
        $result = pg_query($this->id_connect, $query);
        
        return $result;
    }
}
/*  Получить названия колонок таблицы
    SELECT column_name
    FROM information_schema.columns
    WHERE table_schema = 'users'
    AND table_name   = 'users_location'
*/
?>