<?php
require_once($_SERVER['DOCUMENT_ROOT']."/vendor/Misc.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Users/Model/UsersModel.php");

$miscObject = new Misc();
$usersModelObject = new UsersModel();

if ( isset($_GET['code']) && !empty($_GET['code']) ) {
    $code = $_GET['code'];
    
    // 1. Проверка на спецсимволы
    $code = $miscObject->CleanFormData($code);
    // 2. UPDATE
    $data = array(
        'activation_flag' => 'TRUE'
    );
    $table = 'users_info';
    $_where = array(
        'activation_key' => md5($code), // Двойное кодирование, чтоб не светить натуральный код
    );
    $usersModelObject->Update($data, $table, $_where);
    // 3. Очистить поле с кодом в БД
    // 4. Редирект на 
    header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
    // 5. Сообщение, что теперь можно авторизоваться
}
?>
