<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/vendor/Misc.php");
require_once($_SERVER['DOCUMENT_ROOT']."/Modules/Users/Model/UsersModel.php");

$miscObject = new Misc();
$usersModelObject = new UsersModel();

// ПРОВЕРКА СУЩЕСТВОВАНИЯ LOGIN
if ( isset($_POST['login']) and !empty($_POST['login']) ) {
    $p = $miscObject->CleanFormData($_POST['login']);
    
    $_columns = array('login');
    $_where = array('login' => $p);
    $_order = array('id' => 'ASC');
    $result = $usersModelObject->Select($_columns, 'users', $_where, $_order, 1);
    unset($_POST);
    $rows = pg_fetch_all($result);
    
    echo json_encode($rows);
}
// ПРОВЕРКА СУЩЕСТВОВАНИЯ EMAIL
if ( isset($_POST['email']) and !empty($_POST['email']) ) {
    $p = $miscObject->CleanFormData($_POST['email']);
    
    $_columns = array('email');
    $_where = array('email' => $p);
    $result = $usersModelObject->Select($_columns, 'users_info', $_where);
    unset($_POST);
    $rows = pg_fetch_all($result);
    
    echo json_encode($rows);
}
// РЕГИСТРАЦИЯ ПОЛЬЗОВАТЕЛЯ
if ( $_POST['type'] == "RegistrationUsers" ) {
    $attribs = $_POST['attributes'];
    unset($_POST);
    $message = array();
    $data = array();
    // Проверка на спецсимволы
    for ( $i=0; $i<count($attribs); $i++ ) {
        $data[$attribs[$i]['name']] = $miscObject->CleanFormData($attribs[$i]['value']);
    }
    // Проверка captcha
    if ( isset($data['captcha']) && !empty($data['captcha']) ) {
        // Если коды совпадают, тогда обрабатываем запросы.
        if ( strtolower($data['captcha']) == strtolower($_SESSION['captcha']) ) {
            // Определеить какие атрибуты в какую таблицу надо добавить
            $_columns = array('column_name');
            $_table = 'columns';
            $_where = array('table_schema' => 'users', 'table_name' => 'users');
            $_order = array();
            $_limit = NULL;
            $_scheme = 'information_schema';
            $result = $usersModelObject->Select($_columns, $_table, $_where, $_order, $_limit, $_scheme);
            // 1. Получить колонки таблицы users
            $rows_users = pg_fetch_all($result);
            // 2. Получить колонки таблицы users_info
            $_where = array('table_schema' => 'users', 'table_name' => 'users_info');
            $result = $usersModelObject->Select($_columns, $_table, $_where, $_order, $_limit, $_scheme);
            $rows_users_info = pg_fetch_all($result);
            // 3. INSERT данных в таблицу users
            // 3.1. Добавляем недостающие данные
            $data['password'] = md5($data['password-repeat']);
            unset($data['password-repeat']);
            $data['role_id'] = 12;
            $data['type_id'] = 3;
            // По умолчанию всех сохраняем в отдельную организаццию (42)
            $data['organization_id'] = 42;
            // 3.2. Выбираем данные по названиям колонок таблицы users
            $_data = array();
            for ( $i=0; $i<count($rows_users); $i++ ) {
                foreach ( $data as $key => $val ) {
                    if ( $rows_users[$i]['column_name'] == $key ) {
                        $_data[$key] = $val;
                    }
                }
            }
            // 3.3. INSERT
            $result = $usersModelObject->Insert($_data, 'users', 'users', array('id'));
            $rows = pg_fetch_all($result);
            // 4. INSERT данных в таблицу users_info
            // 4.1. Добавляем недостающие данные
            $data['user_id'] = $rows[0]['id'];
            $data['location_bbox'] = $data['bbox'];
            unset($data['bbox']);
            $_activation_key = md5($data['login'].$data['email']); // Полетит в запросе GET
            $data['activation_key'] = md5($_activation_key); // В БД
            $data['activation_flag'] = 'FALSE';
            // 4.2. Выбираем данные по названиям колонок таблицы users
            $_data = array();
            for ( $i=0; $i<count($rows_users_info); $i++ ) {
                foreach ( $data as $key => $val ) {
                    if ( $rows_users_info[$i]['column_name'] == $key ) {
                        $_data[$key] = $val;
                    }
                }
            }
            // 4.3. INSERT
            $result = $usersModelObject->Insert($_data, 'users_info', 'users');
            // Если запрос выполнен, то отправляем email с активационным кодом
            if ( $result ) {
                $mail_to = $data['email'];
                $mail_subject = "Подтверждение регистрации в ГИС Агромониторинга ИКИТ";
                $mail_message = '
                    <html><head><title></title></head><body>
                      <div style="width:700px; height:auto; background: #f2f4f7; display: inline-grid;">
                        <div style="margin: 20px auto; background: #ffffff; width:90%; box-shadow: 0px 0px 1px 0 rgba(0,0,0,0.5);">
                            <table style="width: 98%; border-collapse: collapse; margin: 5px auto; font:15px Arial; color: #585858;">
                            <tr>
                                <td style="background-image: url(http://science.ikit.sfu-kras.ru/public/images/logo_300.png);"><img style="width:200px;" src="file:///' . $_SERVER['HTTP_HOST'] . '/public/images/logo_300.png" /></td>
                                <td style="text-align: center; font: 28px Arial;">Подтверждение регистрации в ГИС Агромониторинга ИКИТ</td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr><td colspan="2">Уважаемый(ая) <b>'. $data['fio'] .'</b>!</td></tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr><td colspan="2">Вы зарегистрировались в геоинформационной системе Агромониторинга Института космических и информационных технологий СФУ: 
                                    <a style="text-decoration: none; color: #0000FF; font-weight: 600;" href="' . $_SERVER['HTTP_HOST'] . '">ГИС Агромониторинга ИКИТ</a></td></tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr><td colspan="2">Ваше имя пользователя для входа в Систему: <b>' . $data['login'] . '</b></td></tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr><td colspan="2"><p>Для подтверждения регистрации, пожалуйста, перейдите по ссылке:</p><a href="http://' . $_SERVER['HTTP_HOST'] . '/users/activation/' . substr(md5($data['login']), 0, 7) . '/?actCode=' . $_activation_key . '"><i>http://' . $_SERVER['HTTP_HOST'] . '/users/activation/' . substr(md5($data['login']), 0, 7) . '/?actCode=' . $_activation_key . '</i></a></td></tr>
                            <tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>
                            <tr><td colspan="2">С уважением, коллектив разработчиков ГИС Агромониторинга</td></tr>
                            <tr><td colspan="2">Институт космических и информационных технологий СФУ</td></tr>
                            <tr><td colspan="2">660074, г. Красноярск, ул. Академика Киренского 26к1</td></tr>
                            <tr><td colspan="2">E-mail: brejnev.ruslan@gmail.com</td></tr>
                            </table>
                        </div>
                      </div>
                    </body></html>';
                $mail_headers = 'From: ГИС Агромониторинга' . "\r\n";
                $mail_headers .= 'Reply-To: brejnev.ruslan@gmail.com' . "\r\n";
                $mail_headers .= "MIME-Version: 1.0" . "\r\n";
                $mail_headers .= "Content-Type: text/html; charset=utf-8" . "\r\n";
                $result = mail($mail_to, $mail_subject, $mail_message, $mail_headers);
                if ( $result ) {
                    $message = array(
                        "query" => array(
                            "result" => true,
                            "message" => 'На указанный Вами электронный адрес отправлено письмо с кодом активации. Для завершения регистрации пройдите по ссылке в кодом активации. <img style="padding-top: 10px;" src="/public/images/reg-success.png">',
                        ),
                    );
                }
                else {
                    $message = array(
                        "query" => array(
                            "result" => false,
                            "message" => 'Произошла ошибка отправки письма с кодом активации. <img style="padding-top: 10px;" src="/public/images/reg-error.png">',
                        ),
                    );
                }
            }
        }
        else {
            $message = array(
                "query" => array(
                    "result" => false,
                    "message" => 'Код, веденный с картинки, не совпадает! <img style="padding-top: 10px;" src="/public/images/reg-error.png">',
                ),
            );
        }
    }
    else {
        $message = array(
            "query" => array(
                "result" => false,
                "message" => 'Введите код с картинки! <img style="padding-top: 10px;" src="/public/images/reg-error.png">',
            ),
        );
    }
    echo json_encode($message);
}
?>
