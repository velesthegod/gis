<?php
require_once($_SERVER['DOCUMENT_ROOT']."/config/DB.php");

class CulturesModel extends DB
{
    protected $SCHEME  = 'data';
    protected $DB_NAME = 'inf_region';
    
    public $id_connect;
    
    public function __construct()
    {
        $this->id_connect = $this->PG_GetConnect($this->DB_NAME);
    }
    
    /**
    * @description Метод GetCultureInfoById() получить информацию о культуре по ID
    * @param $id_culture integer Идентификатор культуры
    * @return Object
    */
    public function GetCultureInfoById($id_culture)
    {
        $query = "SELECT * FROM " . $this->SCHEME . "." . $this->CULTURES_TABLE . " ";

        $query .= "WHERE id_culture = " . $id_culture . ";";

        $result = pg_query($this->id_connect, $query);

        //echo $query;
        return $result;
    }
    
    /**
     * @description Метод GetCultureInfo() получить информацию о культуре по названию
     * @param $c_name string Название культуры
     * @return Object
     */
     public function GetCultureInfoByName($c_name)
     {
         $query = "SELECT * FROM " . $this->SCHEME . "." . $this->CULTURES_TABLE . " ";
         
         $query .= "WHERE c_name = '" . $c_name . "';";
         
         $result = pg_query($this->id_connect, $query);
        
         return $result;
     }
     
     /**
     * @description Метод UpdateLayerDataSetCulture() записывает в таблицу слоя полей названия культур плана и факта
     * @param array Ассоциативный массив (название колонки => значение колонки)
     * @return boolean
     */
     public function SetCulture($data)
     {
         $query = "UPDATE " . $this->SCHEME . "." . $this->FIELDS_TABLE;
         $query .= " SET ";
         foreach ( $data['fields'] as $key=>$val )
         {
             if ( is_string($val) )
             {
                 $query .= $key . " = '". $val . "' ";
             }
             else
             {
                 $query .= $key . " = ". $val . " ";
             }
         }
         $query .= " WHERE " . $data['id_field_name'] . " = " .$data['id_field_value']. "; ";
         
         $result = pg_query($this->id_connect, $query);
         
         return $result;
         
     }
}
?>
