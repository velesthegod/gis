<?php
$name = $_POST['name'];
$title = $_POST['title'];
$attributes = $_POST['attributes'];

$xmlBuilder = new XmlBuilder($title, $name, $attributes);

$file = file_get_contents($name . '.sld');
$file = str_replace('xmlns:ogc="ogc"', '', $file);
$file = str_replace('xmlns:xmlns="xmlns"', '', $file);
file_put_contents($name . '.sld', $file);

$filePath = realpath($name . '.sld');

/*var_dump("curl -v -u admin:geoserver -XPUT -H \"Content-type: application/vnd.ogc.sld+xml\" -d @$filePath http://localhost:8080/geoserver/rest/styles/$name");
die(1);*/

exec("curl -v -u admin:geoserver -XPOST -H \"Content-type: text/xml\" -d \"<style><name>" .$name."</name><filename>" .$name. ".sld</filename></style>\" http://localhost:8080/geoserver/rest/styles");

exec("curl -v -u admin:geoserver -XPUT -H \"Content-type: application/vnd.ogc.sld+xml\" -d @$filePath http://localhost:8080/geoserver/rest/styles/$name");

echo json_encode([]);

class XmlBuilder{
    /** @var SimpleXMLElement*/
    private $dom;

    public function __construct(string $title, string $name, array $attributes){
        $this->addXmlHeaders();
        $featureTypeStyle = $this->createXmlWithMainAttributes($title, $name);
        $this->addRuleToXml($featureTypeStyle, $attributes);
        return $this->saveXml($name . '.sld');
    }

    private function addXmlHeaders(){
        $this->dom = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><StyledLayerDescriptor></StyledLayerDescriptor>", 0, false, 'ogc', true);
        $root = $this->dom;
        $root->addAttribute('xsi:xsi:schemaLocation', "http://www.opengis.net/sld%20StyledLayerDescriptor.xsd");
        $root->addAttribute('xmlns', "http://www.opengis.net/sld");
        $root->addAttribute('xmlns:ogc', "http://www.opengis.net/ogc", "xmlns");
        $root->addAttribute('xmlns:xlink', "http://www.w3.org/1999/xlink", "xmlns");
        $root->addAttribute('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance", "xmlns");
    }

    private function createXmlWithMainAttributes(string $title, string $name){
        $styledLayer = $this->dom;

        $namedLayer = $styledLayer->addChild('NamedLayer');

        $namedLayer->addChild('Name', $name);

        $userStyle = $namedLayer->addChild('UserStyle');
        $userStyle->addChild('Title', $title);
        $featureTypeStyle = $userStyle->addChild('FeatureTypeStyle');

        return $featureTypeStyle;
    }

    public function addRuleToXml(SimpleXMLElement $featureTypeStyle, array $attributes){
        foreach ($attributes as $attribute){
            $rule = $featureTypeStyle->addChild('Rule');

            $rule->addChild('Name', $attribute['Literal']);
            $rule->addChild('Title', $attribute['Literal']);

            $ogcFilter = $rule->addChild('ogc:Filter', null, 'ogc');
            $ogcPropertyIsEqualTo = $ogcFilter->addChild('ogc:PropertyIsEqualTo', null, 'ogc');

            $ogcPropertyIsEqualTo->addChild('ogc:PropertyName',$attribute['PropertyName'], 'ogc');
            $ogcPropertyIsEqualTo->addChild('ogc:Literal',$attribute['Literal'], 'ogc');

            $polygonSymbolizer = $rule->addChild('PolygonSymbolizer');


            $fill = $polygonSymbolizer->addChild('Fill');

            $cssParameterFill = $fill->addChild('CssParameter', $attribute['fill']);
            $cssParameterFill->addAttribute('name', 'fill');

            $cssParameterFillOpacity = $fill->addChild('CssParameter', $attribute['fill-opacity']);
            $cssParameterFillOpacity->addAttribute('name', 'fill-opacity');


            $stroke = $polygonSymbolizer->addChild('Stroke');

            $strokeCssParameterStroke = $stroke->addChild('CssParameter', $attribute['stroke']);
            $strokeCssParameterStroke->addAttribute('name', 'stroke');

            $strokeCssParameterStrokeWidth = $stroke->addChild('CssParameter', $attribute['stroke-width']);
            $strokeCssParameterStrokeWidth->addAttribute('name', 'stroke-width');
        }
    }

    private function saveXml(string $filePath){
        return $this->dom->saveXML($filePath);
    }
}

/*$doc = new DOMDocument();
    $doc->preserveWhiteSpace = false;
    $doc->formatOutput = true;
    $doc->load($filePath);
    $doc->save($filePath);*/