Users = {
    
    login: null,
    password: null,
    role: null,
    id: null,
    workingLayerId: null,
    bboxWkt: null,
    adminRole: 8,
    
    REG_DATA: [],
    
    _bbox: null,
    
    URLS: {
        'reg': 'Modules/Users/View/reg-view.php',
    },
    
    check: function(elem, val) {
        var regs = {
            'login': /^[a-zA-Z]{1}[\w\d_-]{0,19}$/g,
            'password': /^[a-zA-Z0-9_!@#$%^&*-~]{8,24}$/g,
            "email": /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/g,
        };
        return regs[elem].test(val);
    },
    
    /**
    * @description Метод _sendAjax() возвращает массив метаданных растровых слоев.
    * @param Object dataObject: dataObject['date_from'], dataObject['date_to'], dataObject['satellite']
    * @returns Array
    */
    _sendAjax: function(dataObject) {
        var output = new Object();
        var url = this.URLS['reg'];
        $.ajax({
            url:url,
            method:'POST',
            data:dataObject,
            cache: false,
            async: false,
            headers:[{
                'Access-Control-Request-Headers': 'X-Requested-With'
            }],
            xhrFields: {
                withCredentials:true
            },
            success:function(result){
                output = JSON.parse(result);
            },
            error:function(xhr, textStatus, errorTrown){
                console.log(xhr);
                $.stickr({note:'<p>Действие не выполнено.</p><p>Ошибка' + xhr + '</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
            },
            beforeSend: function(jqXHR, settings){}
        });
        return output;
    },
    
    /**
    * @description Функция convertFormDataToJSON() получает значения всех полей формы регистрации и преобразовывает их в формат JSON.
    *                   Использует функцию Users.check() для проверки корректности введенных данных.
    *                   Использует стикеры для вывода сообщений.
    * @param elems Object содержит элементы формы регистрации.
    * @returns JSON Object если все поля формы заполнены корректно. Иначе False.
    */
    convertFormDataToJSON: function(elems) {
        var flag = true;
        // Обходим все поля формы
        for ( var key in RegistrationForm.CONTROLS ) {
            // Ищем сопадение названий полей на форме и в массиве
            for ( var i=0; i<elems.length; i++ ) {
                // Если совпадение найдено
                if ( key == elems[i].id ) {
                    var selector = $(RegistrationForm.selectors['divRegForm']+' #'+key);
                    var val = selector.val();
                    // Проверить если стикер для элемента уже был выведен ранее, то сначала удалить стикер
                    if ( selector.prev().attr('class') == 'sticker' ) {
                        selector.prev().remove();
                    }
                    // Проверяем является ли поле обязательным
                    if ( RegistrationForm.CONTROLS[key].required == true ) {
                        // Проверяем заполнено ли обязательное поле
                        if ( val ) {
                            // Проверяем есть ли у него регулярное выражение
                            if ( RegistrationForm.CONTROLS[key].regs != undefined ) {
                                var result = Users.check(key, val);
                                // Если значение введено корректно, 
                                if ( result ) {
                                    // проверяем существует ли такое в БД
                                    if ( RegistrationForm.CONTROLS[key].exists ) {
                                        var data = {};
                                        data[key] = val;
                                        var obj = Users._sendAjax(data);
                                        if ( obj.length > 0 ) {
                                            Users.REG_DATA.push({'name': key, 'value': false});
                                            flag = false;
                                            //console.log(RegistrationForm.CONTROLS[key].name+' уже занят другим пользователем!'); // TODO: Стикер
                                            selector.before('<div class="sticker">' + RegistrationForm.CONTROLS[key].name+' уже занят другим пользователем!' + '</div>');
                                            selector.css({"border-color": "#ad4547"});
                                        }
                                        else {
                                            Users.REG_DATA.push({'name': key, 'value': val});
                                            selector.css({"border-color": "#00FF00"});
                                        }
                                    }
                                }
                                // Если не корректно
                                else {
                                    Users.REG_DATA.push({'name': key, 'value': false});
                                    flag = false;
                                    //console.log('Значение '+RegistrationForm.CONTROLS[key].name+' введено не корректно!'); // TODO: Стикер
                                    selector.before('<div class="sticker">' + 'Значение "'+RegistrationForm.CONTROLS[key].name+'" введено не корректно!' + '</div>');
                                    selector.css({"border-color": "#ad4547"});
                                }
                            }
                            else {
                                Users.REG_DATA.push({'name': key, 'value': val});
                                selector.css({"border-color": "#00FF00"});
                            }
                        }
                        // Если не заполнено, то сообщаем
                        else {
                            Users.REG_DATA.push({'name': key, 'value': false});
                            flag = false;
                            //console.log('Поле '+RegistrationForm.CONTROLS[key].name+' должно быть заполнено!'); // TODO: Стикер
                            selector.before('<div class="sticker">' + 'Поле "'+RegistrationForm.CONTROLS[key].name+'" должно быть заполнено!' + '</div>');
                            selector.css({"border-color": "#ad4547"});
                        }
                    }
                    else {
                        if ( val ) {
                            Users.REG_DATA.push({'name': key, 'value': val});
                        }
                    }
                }
            }
        }
        // Проверяем задана ли пространственная локализация (обязательный параметр)
        if ( Users._bbox ) {
            Users.REG_DATA.push({
                'name': 'bbox', 
                'value': '((' + Users._bbox.northWest().lng + ',' + Users._bbox.northWest().lat + '),' +
                         '(' + Users._bbox.southEast().lng + ',' + Users._bbox.southEast().lat + '))'
            }); 
        }
        else {
            Users.REG_DATA.push({'name': 'bbox', 'value': false});
            flag = false;
            $(RegistrationForm.selectors['spanCoordinates']).css({'background-color': 'antiquewhite'});
            $(RegistrationForm.selectors['spanCoordinates']).before('<div class="sticker">' + '<b>Границы области должны быть заданы!</b> Это позволит облегчить Вашу навигацию по карте.' + '</div>');
        }
        
        if ( flag ) {
            var _JSON = {"type":"RegistrationUsers", "attributes": Users.REG_DATA};
            return _JSON;
        }
        else {
            return false;
        }
    },
    
    polygonWktToObject: function(wkt) {
        wkt = wkt.replace('POLYGON((', '');
        wkt = wkt.replace('))', '');
        var arr = wkt.split(',');
        
        var coords = [];
        for ( var i=0; i<arr.length; i++ ) {
            var XY = arr[i].split(' ');
            coords.push({lng: parseFloat(XY[0]), lat: parseFloat(XY[1])});
        }
        
        var obj = {type: 'bbox', coordinates: coords};
        
        return obj;
    },
    
    position: function() {
        if ( this.bboxWkt ) {
            var bbox = this.polygonWktToObject(this.bboxWkt);
            mapObject.fitBounds(bbox.coordinates[0].lng, bbox.coordinates[0].lat, bbox.coordinates[2].lng, bbox.coordinates[2].lat)
        }
        else {
            console.log('Границы не заданы');
        }
    },
    
    isAdmin: function() {
        if ( this.role == this.adminRole ) {
            return true;
        }
        else {
            return false;
        }
    },
}