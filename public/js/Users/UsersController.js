$(document).ready(function(){
    
    $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' #phone').mask("+7(000) 000-00-00", {placeholder: "+7(___) ___-__-__" });
    $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' #fax').mask("+7(000) 000-00-00", {placeholder: "+7(___) ___-__-__" });
    $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' #inn').mask("0000000000");
    $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' #kpp').mask("000000000");
    $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' #bank_kpp').mask("000000000");
    $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' #bank_bik').mask("000000000");
    $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' #bank_schet').mask("00000000000000000000");
    
    // КНОПКА ВЫЗОВА ФОРМЫ РЕГИСТРАЦИИ
    RegistrationForm.displayRegButton();
    
    // ОТОБРАЖЕНИЕ ФОРМЫ РЕГИСТРАЦИИ
    $(document).on('click', RegistrationForm.selectors['urMenuButton'], function(){
        // TODO: Проверка существования формы.
        $("body").prepend('<div id="waiting-mode"></div>');
        // Если была скрыта форма регистрации, то после закрытия формы активации показать ее.
        if ( $(RegistrationForm.selectors['divRegFormWrapper']).length > 0 ) {
            //$("body").prepend('<div id="waiting-mode"></div>');
            RegistrationForm.show();
        }
        else {
            //$("body").prepend('<div id="waiting-mode"></div>');
            $('.wrap').append(RegistrationForm.display());
        }
    });
    
    // ОБРАБОТКА НАЖАТИЯ НА ПРАВУЮ СТРЕЛОЧКУ
    $(document).on('click', RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divArrowRight'], function(){
        var currentRegForm = $(this).parent().parent().attr('class');
        var nextRegForm = $('.'+currentRegForm).next().attr('class');
        var curElLeftPos = Number($("."+currentRegForm).css('left').replace('px', ''));
        var nextElLeftPos = Number($("."+nextRegForm).css('left').replace('px', ''));
        var zindex = Number($("."+currentRegForm).css("z-index"));
        
        $("."+currentRegForm).animate({"left":String(curElLeftPos-200)+'px', "z-index":Number(zindex)-1}, 200);
        $("."+nextRegForm).animate({"left":String(nextElLeftPos+200)+'px', "z-index":Number(zindex)+1}, 200);
        $("."+currentRegForm).animate({"left":String(curElLeftPos)+'px'}, 200);
        $("."+nextRegForm).animate({"left":String(nextElLeftPos)+'px'}, 200);
    });

    // ОБРАБОТКА НАЖАТИЯ НА ЛЕВУЮ СТРЕЛОЧКУ
    $(document).on('click', RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divArrowLeft'], function(){
        var currentRegForm = $(this).parent().parent().attr('class');
        //console.log(currentRegForm);
        var prevRegForm = $('.'+currentRegForm).prev().attr('class');
        //console.log(prevRegForm);
        var curElLeftPos = Number($("."+currentRegForm).css('left').replace('px', ''));
        var prevElLeftPos = Number($("."+prevRegForm).css('left').replace('px', ''));
        var zindex = Number($("."+currentRegForm).css("z-index"));
        
        $("."+currentRegForm).animate({"left":String(curElLeftPos+200)+'px', "z-index":Number(zindex)-1}, 200);
        $("."+prevRegForm).animate({"left":String(prevElLeftPos-200)+'px', "z-index":Number(zindex)+1}, 200);
        $("."+currentRegForm).animate({"left":String(curElLeftPos)+'px'}, 200);
        $("."+prevRegForm).animate({"left":String(prevElLeftPos)+'px'}, 200);
    });
    
    // ОБРАБОТКА НАЖАТИЯ НА КНОПКУ РИСОВАНИЯ ПРЯМОУГОЛЬНИКА
    $(document).on('click', RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' '+RegistrationForm.selectors['butBoundary'], function(){
        // Убираем background и форму регистрации.
        $("body div#waiting-mode").hide();
        //$(RegistrationForm.selectors['divRegFormWrapper']).hide();
        RegistrationForm.hide();
        // Если ранее область уже была выделена, то очищаем предыдущие значения
        Users._bbox = null;
        // и деактивиуем контрол,
        RectangleDraw.disable();
        // затем активируем контрол заново.
        RectangleDraw._activateControl();
    });
    
    // ОБРАБОТКА НАЖАТИЯ НА КНОПКУ ОТМЕНА
    /*$(document).on('click', RegistrationForm.selectors['divRegFormWrapper']+' '+RegistrationForm.selectors['urCancelButton'], function(){
        var settings = {'confirm': true, 'message': 'Вы уверены, что хотите отменить регистрацию?', 'selector': $(RegistrationForm.selectors['divRegFormWrapper'])};
        Form.close(settings);
    });*/
    
    // ОБРАБОТКА НАЖАТИЯ НА КНОПКУ ЗАКРЫТЬ
    $(document).on('click', RegistrationForm.selectors['divRegFormWrapper']+' .close', function(){
        // Если была выделена область, то очищаем предыдущие значения
        Users._bbox = null;
        // и деактивиуем контрол.
        RectangleDraw.disable();
        var settings = {'confirm': true, 'message': 'Вы уверены, что хотите отменить регистрацию?', 'selector': $(RegistrationForm.selectors['divRegFormWrapper'])};
        Form.close(settings);
    });
    
    // ОБРАБОТКА НАЖАТИЯ НА КНОПКУ ЗАКРЫТЬ (ФОРМА АКТИВАЦИИ)
    $(document).on('click', ActivationForm.selectors['divActivationFormWrapper']+' .close', function(){
        var settings = {'confirm': false, 'selector': $(ActivationForm.selectors['divActivationFormWrapper'])};
        Form.close(settings);
        // Если была скрыта форма регистрации, то после закрытия формы активации показать ее.
        if ( $(RegistrationForm.selectors['divRegFormWrapper']).length > 0 ) {
            $("body").prepend('<div id="waiting-mode"></div>');
            RegistrationForm.show();
        }
    });
    
    // ОБРАБОТКА НАЖАТИЯ НА КНОПКУ ЗАРЕГИСТРИРОВАТЬСЯ
    $(document).on('click', RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['urRegButton'], function(){
        var elems = $(RegistrationForm.selectors['divRegForm']+' input[type="text"], ' + RegistrationForm.selectors['divRegForm']+' input[type="password"], ' + RegistrationForm.selectors['divRegForm']+' textarea');
        Users.REG_DATA = []; // Очищаем массив данных
        var _JSON = Users.convertFormDataToJSON(elems);
        if ( _JSON ) {
            //console.log('Полетел AJAX');
            //console.log(_JSON);
            var result = Users._sendAjax(_JSON);
            // Если регистрация прошла успешно
            if ( result.query.result ) {
                // 1. Очистить значения кординат выделенной области
                Users._bbox = null;
                // 2. Деактивировать контрол рисования
                RectangleDraw.disable();
                // 3. Закрыть форму регистрации
                var settings = {'confirm': false, 'selector': $(RegistrationForm.selectors['divRegFormWrapper'])};
                Form.close(settings);
                // 4. Открыть форму активации с сообщением
                $("body").prepend('<div id="waiting-mode"></div>');
                $('.wrap').append(ActivationForm.display(result.query.message));
            }
            // Если при регистрации возникли ошибки
            else {
                // 1. Скрыть форму регистрации
                RegistrationForm.hide();
                // 2. Отобразить форму активации с сообщением об ошибке
                $('.wrap').append(ActivationForm.display(result.query.message));
                // 3. Закрыть форму активации и вернуть обратно форму регистрации с бэкграундом
            }
        }
        else {
            console.log(_JSON);
        }
        // Очистить поля ввода паролей и captcha
        var clearElems = $(RegistrationForm.selectors['divRegForm']+' input[type="password"], ' + RegistrationForm.selectors['divRegForm']+' input#captcha');
        for ( var i=0; i<clearElems.length; i++ ) {
            $(RegistrationForm.selectors['divRegForm'] + " #" + clearElems[i].id).val('');
        }
        // Обновить captcha
        Captcha.refresh();
    });
    
    // ОБРАБОТКА СОБЫТИЯ ONCHANGE - ПРОВЕРКА КОРРЕКТНОСТИ И СУЩЕСТВОВАНИЯ LOGINа
    $(document).on('change', RegistrationForm.selectors['divRegForm']+' #login', function(){
        // Если стикер для элемента уже выведен, то сначала удалить его
        if ( $(this).prev().attr('class') == 'sticker' ) {
            $(this).prev().remove();
        }
        var result = Users.check($(this).attr('id'), $(this).val());
        // Введенный логин корректен
        if ( result ) {
            // Проверка существования логина
            var data = {'login': $(this).val()};
            var login = Users._sendAjax(data);
            if ( login.length > 0 ) {
                $(this).before('<div class="sticker">Логин занят другим пользователем</div>');
                $(this).css({"border-color": "#ad4547"});
            }
            else {
                $(this).css({"border-color": "#00FF00"});
            }
        }
        else {
            $(this).before('<div class="sticker">Некорректный логин. Логин не должен начинаться с цифры, содержать пробелы или специальные символы (~`!\'\"<>@#№;%?&*()=+/\\).</div>');
            $(this).css({"border-color": "#ad4547"});
        }
    });
    // ОБРАБОТКА ВВОДА ПАРОЛЯ
    $(document).on('change', RegistrationForm.selectors['divRegForm']+' #password', function(){
        // Если стикер для элемента уже выведен, то сначала удалить его
        if ( $(this).prev().attr('class') == 'sticker' ) {
            $(this).prev().remove();
        }
        var result = Users.check($(this).attr('id'), $(this).val());
        if ( result ) {
            $(this).css({"border-color": "#00FF00"});
        }
        else {
            $(this).before('<div class="sticker">Некорректный пароль. Убедитесь, что пароль не содержит пробелов и не превышает 24 знаков</div>');
            $(this).css({"border-color": "#ad4547"});
        }
    });
    // ОБРАБОТКА ПОВТОРНОГО ВВОДА ПАРОЛЯ
    $(document).on('change', RegistrationForm.selectors['divRegForm']+' #password-repeat', function(){
        // Если стикер для элемента уже выведен, то сначала удалить его
        if ( $(this).prev().attr('class') == 'sticker' ) {
            $(this).prev().remove();
        }
        var p = $(RegistrationForm.selectors['divRegForm']+' #password').val();
        var id = $(RegistrationForm.selectors['divRegForm']+' #password').attr('id');
        if ( p ) {
            if ( p == $(this).val() ) {
                Users.REG_DATA[id] = p;
                $(this).css({"border-color": "#00FF00"});
            }
            else {
                $(this).before('<div class="sticker">Пароли не совпадают!</div>');
                Users.REG_DATA[id] = false;
                $(this).css({"border-color": "#ad4547"});
            }
        }
    });
    
    // ОБРАБОТКА СОБЫТИЯ ONCHANGE - ПРОВЕРКА КОРРЕКТНОСТИ EMAIL
    $(document).on('change', RegistrationForm.selectors['divRegForm']+' #email', function(){
        // Если стикер для элемента уже выведен, то сначала удалить его
        if ( $(this).prev().attr('class') == 'sticker' ) {
            $(this).prev().remove();
        }
        var result = Users.check($(this).attr('id'), $(this).val());
        // Введенный пароль корректен
        if ( result ) {
            // Проверка существования email
            var data = {'email': $(this).val()};
            var email = Users._sendAjax(data);
            if ( email.length > 0 ) {
                $(this).before('<div class="sticker">На этот Email уже зарегистрирован аккаунт!</div>');
                $(this).css({"border-color": "#ad4547"});
            }
            else {
                $(this).css({"border-color": "#00FF00"});
            }
        }
        else {
            $(this).before('<div class="sticker">Некорректный Email!</div>');
            $(this).css({"border-color": "#ad4547"});
        }
    });
    
    // ОБРАБОТКА НАЖАТИЯ НА РЕЖИМ ОТОБРАЖЕНИЯ ПАРОЛЯ
    $(document).on('click', RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' .form-control input#pswd-switcher', function() {
        if ( document.getElementById('pswd-switcher').checked == true ) {
            $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' .form-control input#password').attr('type', 'text');
        }
        else {
            $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' .form-control input#password').attr('type', 'password');
        }
    });
    
    // ОБРАБОТКА НАЖАТИЯ НА РЕЖИМ ОТОБРАЖЕНИЯ ПОВТОРНОГО ПАРОЛЯ
    $(document).on('click', RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' .form-control input#pswdr-switcher', function() {
        if ( document.getElementById('pswdr-switcher').checked == true ) {
            $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' .form-control input#password-repeat').attr('type', 'text');
        }
        else {
            $(RegistrationForm.selectors['divRegForm']+' '+RegistrationForm.selectors['divRegFormRow']+' .form-control input#password-repeat').attr('type', 'password');
        }
    });
});