Form = {
    close: function(settingsObj) {
        if ( settingsObj.confirm ) {
            if ( !confirm(settingsObj.message) )
                return;
            settingsObj.selector.remove(); // detach()
            $("div#waiting-mode").remove();
        }
        else {
            settingsObj.selector.remove();
            $("div#waiting-mode").remove();
            //$("div[id^='waiting-mode']").remove();
        }
    },
};

RegistrationForm = {
    CONTROLS: {
        'login': {
            'required': true,
            'regs': /^[a-zA-Z]{1}[\w\d_-]{0,19}$/g,
            'exists': true,
            'name': 'Логин'+'<span style="color:red;">*</span>',
            'id': 'login',
        
        },
        'password': {
            'required': true,
            'regs': /^[a-zA-Z0-9_!@#$%^&*-~]{8,24}$/g,
            'exists': false,
            'name': 'Пароль'+'<span style="color:red;">*</span>',
            'id': 'password',
        },
        'email': {
            'required': true,
            'regs': /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/g,
            'exists': true,
            'name': 'E-mail'+'<span style="color:red;">*</span>',
            'id': 'email',
        },
        'password-repeat': {
            'required': true,
            'exists': false,
            'name': 'Повторите пароль'+'<span style="color:red;">*</span>',
            'id': 'password-repeat',
        },
        'fio': {
            'required': true,
            'exists': false,
            'name': 'Фамилия Имя Отчество'+'<span style="color:red;">*</span>',
            'id': 'fio',
        },
        'phone': {
            'required': true,
            'exists': false,
            'name': '№ Телефона'+'<span style="color:red;">*</span>',
            'id': 'phone',
        },
        'address': {
            'required': false,
            'exists': false,
            'name': 'Адрес',
            'id': 'address',
        },
        'fax': {
            'required': false,
            'exists': false,
            'name': 'Факс',
            'id': 'fax',
        },
        'inn': {
            'required': false,
            'exists': false,
            'name': 'ИНН',
            'id': 'inn',
        },
        'kpp': {
            'required': false,
            'exists': false,
            'name': 'КПП',
            'id': 'kpp',
        },
        'bank': {
            'required': false,
            'exists': false,
            'name': 'Банк',
            'id': 'bank',
        },
        'bank_kpp': {
            'required': false,
            'exists': false,
            'name': 'КПП Банка',
            'id': 'bank_kpp',
        },
        'bank_bik': {
            'required': false,
            'exists': false,
            'name': 'БИК Банка',
            'id': 'bank_bik',
        },
        'bank_schet': {
            'required': false,
            'exists': false,
            'name': 'Расчетный счет',
            'id': 'bank_schet',
        },
        'captcha': {
            'required': true,
            'exists': false,
            'name': 'Код с картинки',
            'id': 'captcha',
        },
    },
    
    selectors: {
        divRegFormWrapper: "#urf-wrapper",
        divRegForm: "#ur-form",
        divRegFormRow: "div.ur-row",
        spanRowName: "span.nameru",
        butBoundary: "button#button-boundary",
        spanCoordinates: "div#ur-form div.ur-row span.coordinates",
        urMenuButton: "#ur-menu-button",
        divArrowRight: ".arrow-right",
        divArrowLeft: ".arrow-left",
        urCancelButton: ".urc-button",
        urRegButton: ".urr-button",
    },
    
    elementsNames: {
        divRegFormWrapper: "urf-wrapper",
        urButton: "ur-button",
        divRegForm: "ur-form",
        divRegFormRow: "ur-row",
        spanRowName: "nameru",
        butBoundary: "button-boundary",
        urMenuButton: "ur-menu-button",
        divArrowRight: "arrow-right",
        divArrowLeft: "arrow-left",
        urCancelButton: "urc-button",
        urRegButton: "urr-button",
    },
    
    displayRegButton: function() {
        $('.navbar ul.nav').append('<li><button id="' + this.elementsNames['urMenuButton'] + '">Зарегистрироваться</button></li>');
        //$('#authorization-box .authorization-form').after('<div><button name="ur-button" class="' + this.elementsNames['urButton'] + '">Зарегистрироваться</button></<div>');
        //$('.menu-authorization').before('<button name="ur-button" class="' + this.elementsNames['urButton'] + '">Зарегистрироваться</button>');
    },
    
    display: function() {
        var html = '<div id="' + this.elementsNames['divRegFormWrapper'] + '">' + 
                        // 1
                        '<div id="' + this.elementsNames['divRegForm'] + '" class="urf-1">' + 
                            '<div class="urf-header">' +
                                '<div class="title">Шаг 1: Учетные данные для входа в программу</div>' +
                                '<div class="close"></div>' +
                            '</div>' +
                            '<div class="urf-content">' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['login'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['login'].id + '" type="text" placeholder="Не более 20 символов">' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['password'].name + '</span>'+
                                    '<div class="form-control">' +
                                        '<input id="' + this.CONTROLS['password'].id + '" type="password" maxlength="30" placeholder="Не менее 8 символов">' +
                                        '<input type="checkbox" id="pswd-switcher">' +
                                        '<label for="pswd-switcher"></label>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['password-repeat'].name + '</span>'+
                                    '<div class="form-control">' +
                                        '<input id="' + this.CONTROLS['password-repeat'].id + '" type="password" maxlength="30" placeholder="Пароль">' +
                                        '<input type="checkbox" id="pswdr-switcher">' +
                                        '<label for="pswdr-switcher"></label>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['fio'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['fio'].id + '" type="text" placeholder="Фамилия Имя (Отчество)">' +
                                '</div>' +
                            '</div>' +
                            '<div class="urf-footer">' +
                                //'<button class="' + this.elementsNames['urCancelButton'] + '">Отмена</button>' +
                                '<div class="' + this.elementsNames['divArrowRight'] + '"></div>' +
                            '</div>' +
                        '</div>' +
                        // 2
                        '<div id="' + this.elementsNames['divRegForm'] + '" class="urf-2">' +
                            '<div class="urf-header">' +
                                '<div class="title">Шаг 2: Юридические данные организации</div>' +
                                '<div class="close"></div>' +
                            '</div>' +
                            '<div class="urf-content">' +
                                // Данные о юридическом лице:
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['address'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['address'].id + '" type="text" placeholder="Введите адрес">' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['phone'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['phone'].id + '" type="text">' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['fax'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['fax'].id + '" type="text">' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['email'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['email'].id + '" type="text">' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['inn'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['inn'].id + '" type="text">' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['kpp'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['kpp'].id + '" type="text">' +
                                '</div>' +
                                // Банковские реквизиты:
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['bank'].name + '</span>'+
                                    '<textarea id="' + this.CONTROLS['bank'].id + '" rows="3"></textarea>' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['bank_kpp'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['bank_kpp'].id + '" type="text">' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['bank_bik'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['bank_bik'].id + '" type="text">' +
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">' + this.CONTROLS['bank_schet'].name + '</span>'+
                                    '<input id="' + this.CONTROLS['bank_schet'].id + '" type="text">' +
                                '</div>' +
                            '</div>' +
                            '<div class="urf-footer">' +
                                '<div class="' + this.elementsNames['divArrowLeft'] + '"></div>' +
                                //'<button class="' + this.elementsNames['urCancelButton'] + '">Отмена</button>' +
                                '<div class="' + this.elementsNames['divArrowRight'] + '"></div>' +
                            '</div>' +
                        '</div>' +
                        // 3
                        '<div id="' + this.elementsNames['divRegForm'] + '" class="urf-3">' +
                            '<div class="urf-header">' +
                                '<div class="title">Шаг 3: Задание локации</div>' +
                                '<div class="close"></div>' +
                            '</div>' +
                            '<div class="urf-content">' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">Границы выделенной области:</span>' +
                                    '<button type="button" class="' + this.elementsNames['butBoundary'] + '" id="' + this.elementsNames['butBoundary'] + '"></button>'+
                                    '<span class="coordinates"></span>'+
                                '</div>' +
                                '<div class="' + this.elementsNames['divRegFormRow'] + '">' +
                                    '<span class="' + this.elementsNames['spanRowName'] + '">Ведите код с картинки</span>' +
                                    CaptchaForm.display() +
                                '</div>' +
                            '</div>' +
                            '<div class="urf-footer">' +
                                '<div class="' + this.elementsNames['divArrowLeft'] + '"></div>' +
                                //'<button class="' + this.elementsNames['urCancelButton'] + '">Отмена</button>' +
                                '<button class="' + this.elementsNames['urRegButton'] + '">Зарегистрироваться</button>' +
                            '</div>'+
                        '</div>' +
                   '</div>';
        return html;
    },
    
    hide: function() {
        // Убираем форму регистрации.
        $(this.selectors['divRegFormWrapper']).hide();
    },
    
    show: function() {
        $(this.selectors['divRegFormWrapper']).show();
    },
};

ActivationForm = {
    selectors: {
        divActivationFormWrapper: "#af-wrapper",
    },
    
    elementsNames: {
        divActivationFormWrapper: "af-wrapper",
    },
    
    display: function(msg) {
        var html = '<div id="' + this.elementsNames['divActivationFormWrapper'] + '">' + 
                        '<div id="' + RegistrationForm.elementsNames['divRegForm'] + '" class="form-position">' + 
                            '<div class="urf-header">' +
                                '<div class="title">Активация аккаунта</div>' +
                                '<div class="close"></div>' +
                            '</div>' +
                            '<div class="urf-content">' +
                                '<div class="message">' + msg + '</div>' +
                            '</div>' +
                        '</div>' +
                   '</div>';
        return html;
    },
};

