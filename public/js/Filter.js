var Filter = {
    
    selectors: {
        spanCoordinates: "#layer-filter-form .filter-box .coordinates",
        input: "#layer-filter-form input",
    },
    
    _layer: new Object(),
    _filter_bbox: null,
    
    /**
    * @description Метод _sendAjax() получает атрибуты заданного слоя через REST-запрос к слою.
    * @param integer layerId. Example of param: layerId = 302
    * @param string token. Example of param: token = 'ac26b5e8-abaf-44f7-a9b4-4b71e0ee51c9'
    * @returns Object
    */
    _sendAjax: function(layerId, token) {
        var output = new Object();
        var url = 'Modules/Layers/View/layers-view.php';
        var send = "layers="+layerId+"&token="+token;
        $.ajax({
            url:url,
            method:'POST',
            data:send,
            cache: false,
            async: false,
            headers:[{
                'Access-Control-Request-Headers': 'X-Requested-With'
            }],
            xhrFields: {
                withCredentials:true
            },
            success:function(result){
                output = JSON.parse(result);
            },
            error:function(xhr, textStatus, errorTrown){
                console.log(xhr);
                $.stickr({note:'<p>Действие не выполнено.</p><p>Ошибка' + xhr + '</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
            },
            beforeSend: function(jqXHR, settings){}
        });
        return output;
    },
    
    /**
    * @description Метод _getLayerAttribsTypes() получает типы данных атрибутов слоя через SQL-запрос к таблице слоя.
    * @param string type. Example of param: layerEngName = workspace:contours_zshn_suchobuzimskoe_2015_vw
    * Example of getting: var layerEngName = layer._model._values.info.typeName;
    * @returns Object
    */
    _getLayerAttribsTypes: function(layerEngName){
        var url = 'Modules/Layers/View/layers-view.php';
        var send = "layer="+layerEngName;
        
        var output = new Object();
        
        $.ajax({
            url:url,
            method:'POST',
            data:send,
            cache: false,
            async: false,
            headers:[{
                'Access-Control-Request-Headers': 'X-Requested-With'
            }],
            xhrFields: {
                withCredentials:true
            },
            success:function(result){
                //handleResult(JSON.parse(result));
                
                output = JSON.parse(result);
            },
            error:function(xhr, textStatus, errorTrown){
                console.log(xhr);
                $.stickr({note:'<p>Действие не выполнено.</p><p>Ошибка' + xhr + '</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
            },
            beforeSend: function(jqXHR, settings){}
        });
        
        return output;
    },
    
    _mergeAttributes: function(a, b){
        for ( var i=0; i<b.length; i++ )
        {
            for ( var j=0; j<a.length; j++ )
            {
                if ( a[j].name == b[i].name )
                {
                    a[j].nameRu = b[i].nameRu;
                }
            }
        }
        return a;
    },
    
    FormToJSON: function(elems) {
        var inputElems = elems;
        var attrJson = [];
        
        for (var i=0; i<inputElems.length; i++)
        {
            attrJson[i] = {
                "name": inputElems[i]['name'],
                "value": inputElems[i]['value'],
                "type": inputElems[i]['id'],
            };
        }
        return attrJson;
    },
    
    Form: function() {
        var isRaster = AMLayers._isRaster(this._layer._model._values.info.style);
        
        var layerFilterHTML = '';
        layerFilterHTML += '<div id="layer-filter-form" class="vector">';
        
        if ( isRaster == false ) 
        {
            var idLayer = this._layer._model._pkValue;
            var token = GeoPortal._accessToken;
            var attributes = this._sendAjax(idLayer, token);
            attributes = attributes.attributes;
            
            var fields = this._layer.fields();
            
            var attributes_m = this._mergeAttributes(attributes, fields);
            
            for ( var j=0; j<attributes_m.length; j++ )
            {
                if ( attributes_m[j].nameRu != '' )
                {
                    layerFilterHTML += '<div class="filter-box">';
                    
                    if ( attributes_m[j].type == 'string' || attributes_m[j].type == 'date' )
                    {
                        layerFilterHTML += '<select size="1" class="filter-entrance '+attributes_m[j].name+'"><option value="byentry">По вхождению</option><option value="fullcompliance">Полное соответствие</option></select>';
                        layerFilterHTML += '<span class="filter-nameru">' + attributes_m[j].nameRu + ':</span> ';
                        layerFilterHTML += '<input type="text" name="'+attributes_m[j].name+'" id="'+attributes_m[j].type+'" class="input-text" />';
                    }
                    if ( attributes_m[j].type == 'double' || attributes_m[j].type == 'decimal' || attributes_m[j].type == 'int' )
                    {
                        layerFilterHTML += '<span class="filter-nameru">' + attributes_m[j].nameRu + ':</span> ';
                        layerFilterHTML += '<select size="1" class="filter-compare '+attributes_m[j].name+'"><option value="0">=</option><option value="1">></option><option value="2"><</option></select>';
                        layerFilterHTML += '<input type="number" step="0.1" name="'+attributes_m[j].name+'" id="'+attributes_m[j].type+'" class="input-number" />';
                    }
                    layerFilterHTML += '</div>';
                }
            }
            
            layerFilterHTML += '<div class="filter-box">';
            layerFilterHTML += '<span class="filter-nameru">Границы выделенной области:</span> ';
            layerFilterHTML += '<button type="button" class="button-boundary" id="button-boundary"></button>';
            layerFilterHTML += '<span class="coordinates"></span>';
            layerFilterHTML += '</div>';
            
            layerFilterHTML += '<p style="margin:20px 0;">';
            layerFilterHTML += '<button type="button" class="button-clear" id="button-clear">Очистить</button>';
            layerFilterHTML += '<button type="button" class="button-show" id="button-show">Показать</button>';
            layerFilterHTML += '</p>';
        }
        layerFilterHTML += '</div>';
        return layerFilterHTML;
    },
    
    /**
    * @description Метод ExecuteCQL() формирует и выполняет CQL-запрос.
    * @param Object type. attrJson - данные с формы "layer-filter-form"
    * Example of getting: var attrJson = Filter.FormToJSON($("#layer-filter-form input"));
    * @returns void
    */
    ExecuteCQL: function(attrJson) {
        var filter = [];
        
        for ( var i=0; i<attrJson.length; i++ ) 
        {
            var val = attrJson[i].value.trim();
            var name = attrJson[i].name;
            var type = attrJson[i].type;
            
            if ( val != '' ) 
            {
                var _filter = {};
                _filter.field = name;
                _filter.type = type;
                
                if ( type == 'string' || type == 'date' )
                {
                    var operator = $("select.filter-entrance."+name+" option:selected").val();
                    
                    if ( operator == 'byentry' )
                    {
                        _filter.value = '%'+val+'%';
                    }
                    if ( operator == 'fullcompliance' )
                    {
                        _filter.value = val;
                    }
                }
                if ( type == 'double' || type == 'decimal' || type == 'int' )
                {
                    var operator = $("select.filter-compare."+name+" option:selected").text();
                    _filter.value = val;
                    _filter.compare = operator;
                }
                filter.push(_filter);
            }
        }
        
        if ( this._filter_bbox != null )
        {
            var _filter_bbox = {};
            
            _filter_bbox.type = "geometry";
            
            var _value = [{
                        lng: this._filter_bbox._northEast.lng,
                        lat: this._filter_bbox._northEast.lat
                    },{
                        lng: this._filter_bbox._southWest.lng,
                        lat: this._filter_bbox._southWest.lat
                    }];
            _filter_bbox.value = _value;
            filter.push(_filter_bbox);
            RectangleDraw.disable();
            console.log(this._filter_bbox);
            mapObject.fitBounds(this._filter_bbox);
        }
        
        var filterCQL = new GeoPortal.Filter.CQL(filter);
        this._layer.setFilter(filterCQL);
    },
    
    ClearForm: function(attrJson) {
        for ( var i=0; i<attrJson.length; i++ )
        {
            $(".filter-box input")[i]['value'] = '';
        }
        
        if ( this._filter_bbox != null )
        {
            RectangleDraw.disable();
            this._filter_bbox = null;
            $(this.selectors['spanCoordinates']).empty();
        }
        
        var filterCQL = new GeoPortal.Filter.CQL([]);
        this._layer.setFilter(filterCQL);
        
        this._layer.requestBbox(
            function(bbox){ 
                mapObject.fitBounds(bbox.minx, bbox.miny, bbox.maxx, bbox.maxy);
            }, 
            function(status, error){ 
                console.log(error); 
        });
    },
    
};