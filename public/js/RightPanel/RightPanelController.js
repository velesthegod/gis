$(document).ready(function(){
    var divRightPanel = $(RightPanelForm.selectors['divRightPanel']),
        divUserPanel = $(ObjectInfo.selectors['divUserPanel']),
        divRollUserPanel = $(ObjectInfo.selectors['divRollUserPanel']);
        
    // ОБРАБОТКА СВОРАЧИВАНИЕ/РАЗВОРАЧИВАНИЕ ПРАВОЙ ПАНЕЛИ.
    $(document).on('click', RightPanelForm.selectors['divRoller'], function(){
        if ( divRightPanel.hasClass('groups-hide') == true )
        {
            divRightPanel.removeClass('groups-hide');
            divRightPanel.addClass('groups-show');
            divRightPanel.slideUp(500);
            $(this).css({"opacity":"0.5", "-webkit-opacity":"0.5", "-moz-opacity":"0.5", "-o-opacity":"0.5"});
        }
        else
        {
            divRightPanel.removeClass('groups-show');
            divRightPanel.addClass('groups-hide');
            divRightPanel.slideDown(500);
            $(this).css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
        }
    });
    // ОБРАБОТКА СОБЫТИЙ, СВЯЗАННЫХ С ОБЩИМИ СЛОЯМИ.
    $(document).on("click", 'div.layer input', function() { 
        var id = $(this).val(), 
        layer = layersStore[id];
        
        // ВКЛЮЧАЕМ КАРТИНКУ ПЕРЕЛЕТА К СЛОЮ И ОТРАБАТЫВАЕМ САМ ПЕРЕЛЕТ.
        if ( document.getElementById('checkbox-layer-' + id).checked == true ) 
        { 
            //if ( Users.role == adminRole ) {
            if ( Users.isAdmin() ) {
                $("#last-" + id).after('<div style="float:right;"><span class="create-layer-object" id="create-layer-object-' + id + '"><img src="public/images/img-add-object.png" title="Создать объект в этом слое" /></span></div>');
                $("#last-" + id).after('<div style="float:right;"><span class="download-layer" id="download-layer-' + id + '"><img src="public/images/download-layer.png" title="Скачать слой" /></span></div>');
            }
            $("#last-" + id).after('<div style="float:right;"><span class="fly" id="fly-' + id + '"><img src="public/images/fly-to-layer.png" title="Перелет к слою" /></span></div>'); 
            
            $("#fly-" + id).click(function(){ 
                layer.requestBbox( 
                    function(bbox){ 
                        mapObject.fitBounds(bbox.minx, bbox.miny, bbox.maxx, bbox.maxy);
                    }, 
                    function(status, error){ 
                        console.log(error); 
                }); 
            });
            
            $("#download-layer-" + id).click(function(){
                var layer_name = layer.name();

                if ( layer._model._values.info.style == "raster" )
                {
                    var url = "http://"+hostName+"/geoserver/workspace/wms/kml?"+"layers="+layer_name;
                }
                else
                {
                    var url = "http://"+hostName+"/geoserver/workspace/ows?"+
                              "service=WFS&"+
                              "version=1.1.0&"+
                              "request=GetFeature&"+
                              "typeName="+layer_name+"&"+
                              "outputFormat=SHAPE-ZIP&"+
                              "format_options=charset:cp1251";
                }
                
                window.open(url, '_blank');
            });
            
            $("#create-layer-object-" + id).click(function(){
                // Если в левой панеле во вкладке Свойства уже была открыта какая то форма, то удаляем ее:
                $('#user-panel #object-info-form').remove();
                // Активируем контрол для рисования:
                PolygonDraw._activateControl();
                // Разворачиваем левую панель
                divUserPanel.slideDown(500);
                divUserPanel.attr('class', 'up-hide');
                divRollUserPanel.css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
                // Делаем активной нужную вкладку таба.
                $.ionTabs.setTab("Tabs_Group_name", "Tab_1_name");
                // Перемещаем контрол zoom вправо
                $(".zoom-control").css({"left":zoomControlShift}); 
                // Создаем форму для ввода атрибутов объекта
                ObjectInfo._layer = layersStore[id];
                var createForm = ObjectInfo.CreateForm();
                $("div[data-name='Tab_1_name']").prepend(createForm);
            });
        } 
        else 
        { 
            $("#create-layer-object-" + id).remove();
            $("#download-layer-" + id).remove();
            $("#fly-" + id).remove();
        } 

        if(typeof layer != 'undefined') { 
            layer.turn(mapObject); 
        }
    });
    // ОТРАБОТКА НАЖАТИЯ НА КАРТИНКУ ИНФОРМАЦИИ О СЛОЕ.
    // ОТОБРАЖАЕТСЯ ВКЛАДКА ЛЕГЕНДЫ СЛОЯ, ВКЛАДКА ВЕКТОРНЫХ/РАСТРОВЫХ ФИЛЬТРОВ, ВКЛАДКА МЕТАДАННЫХ РАСТРОВЫХ СЦЕН.
    $(document).on('click', 'div.layers div.layer .info', function(){
        $("#layer-legend").remove();
        
        var id = this.id, 
            layer = layersStore[id], 
            legend, 
            legend = layer.legend();

        /** Сохраним ид и название выбранного слоя */
        clickedLayerForInfoTab.id = Number(id);
        clickedLayerForInfoTab.name = layer._model._values.name;
        clickedLayerForInfoTab.typeName = layer._model._values.info.typeName;

        // Разворачиваем левую форму    
        divUserPanel.slideDown(500); 
        divUserPanel.attr('class', 'up-hide'); 
        divRollUserPanel.css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
        // Делаем активной нужную вкладку таба.
        $.ionTabs.setTab("Tabs_Group_name", "Tab_4_name");
        $.ionTabs.setTab("SubTabs_Group_name", "SubTab_1_legend");
        $(".zoom-control").css({"left":zoomControlShift}); 
        $("div[data-name='SubTab_1_legend']").prepend(`
          <div id="layer-legend" class="tab-content">
          
           <button
              type="button"
              class="btn btn-info"
              onclick="switchLayerEditMode()"
            >Редактировать</button>
    
            <div class="legend">
              <img src="${legend}">
            </div>
          </div>
        `);
        if ( layer._model._values.isKosmosnimok == false )
        {
            // Вывод формы фильтрации объектов векторных слоев.
            Filter._layer = layer;
            var filter = Filter.Form();
            $("div[data-name='SubTab_2_filter'] #layer-filter-form").replaceWith(filter);
            // Если ранее была выделена прямоугольная область фильтрации векторных объектов или растровых сцен, то очистить координаты.
            Filter._filter_bbox = null;
        }
        else
        {
            // Вывод формы метаданных выбранной сцены.
            var dataJson = {"type":"rasterMetadata", "attributes": {"id": id}};
            var metadata = ErsData._sendAjax(dataJson);
            // Для отображения в модальном окне.
            ErsData._rasterName = metadata[0].fname;
            ErsData._metadata = metadata[0].metadata;
            var metadataForm = ErsDataFormMetadata.display(metadata);
            // Делаем активной нужную вкладку таба.
            $.ionTabs.setTab("Tabs_Group_name", "Tab_4_name");
            $.ionTabs.setTab("SubTabs_Group_name", "SubTab_3_metadata");
            $("div[data-name='SubTab_3_metadata'] #layer-metadata-form").replaceWith(metadataForm);
        }
    });
});