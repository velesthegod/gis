RightPanelForm = {
    selectors: {
        divRightPanel: "#right-panel",
        divRoller: "div#roll-up",
    },
    
    /**
    * @description Метод drawRightPanel() формирует скелет правой панели.
    */
    display: function() {
        var divRightPanel = $(this.selectors['divRightPanel']);
        divRightPanel.before('<div id="roll-up"><p>ОБЩИЕ СЛОИ</p></div>');
        divRightPanel.addClass('groups-hide');
        divRightPanel.addClass('tabbable');
        divRightPanel.prepend('<ul class="nav nav-tabs">'+
                                '<li class="active vlayers"><a href="#rp-tab0" data-toggle="tab" class="vlayers"></a></li>'+
                                '<li class="rlayers"><a href="#rp-tab1" data-toggle="tab" class="rlayers"></a></li>'+
                              '</ul>'+
                              '<div class="tab-content"></div>');
        
        $(this.selectors['divRightPanel']).height(RightPanel.calcHeight());
    },
}