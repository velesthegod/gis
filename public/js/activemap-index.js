
$.minicolors.defaults = $.extend($.minicolors.defaults, {
  opacity: true,
});


/** Для наглядности */
class LayerFeatureModel {
  id = '';
  geometry = {
    type: '',
  };
  geometry_name = '';
  properties = [];

  stylesPerProperty = [];

  // fill; // (цвет заливки)
  // 'fill-opacity'; // (прозрачность заливка)
  // stroke; // (цвет обводки)
  // 'stroke-width'; // (толщина обводки)
  // PropertyName; // (название атрибута)
  // Literal; // (значение атрибута)
}


let currentLayer = {
  id: 0,
  name: '',
  typeName: '',
  features: [],
};

let editLayerMode = false;
let currentLayerAttribute = null;


/**
 * Доступные аттрибуты
 *
 * @returns {string[]|Array}
 */
function availableFeatureProperties() {
  if (!currentLayer || !(currentLayer.features || []).length) {
    return [];
  }
  return Object.keys(currentLayer.features[0].properties);
}


/**
 * Получить features для выбранного атрибута
 *
 * @returns {*[]}
 */
function getFeaturesForAttribute() {
  const allFeatures = (currentLayer.features || [])
    .filter(feature =>
      feature.properties &&
      typeof feature.properties === typeof {} &&
      feature.properties[currentLayerAttribute]
    )
    .sort((a, b) =>
      a.id > b.id ? 1 : -1 &&
      a.properties[currentLayerAttribute] > b.properties[currentLayerAttribute] ? 1 : -1
    );

  return allFeatures.filter(feature =>
    !allFeatures.some(fItem =>
      feature.id > fItem.id &&
      feature.properties[currentLayerAttribute] === fItem.properties[currentLayerAttribute]
    )
  );
}


/**
 * Переключение режима редактирования
 */
function switchLayerEditMode() {
  editLayerMode = !editLayerMode;

  if (editLayerMode) {
    setLayerEditMode();
  } else {
    setLayerLegendMode();
  }
}


/**
 * Установка легенды слоя
 *
 * @param type
 */
function setLayerLegendMode(type = 'farmer') {

  const getUrl = () => {
    switch (type) {
      case 'farmer':
        return `http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=${farmerUniversalStyleName}&layer=workspace:${currentLayer.name}&format=image/png`;
      case 'plan':
        return `http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=${farmerUniversalStyleName}&layer=workspace:${currentLayer.name}&format=image/png`;
      case 'fact':
        return `http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=${farmerUniversalStyleName}&layer=workspace:${currentLayer.name}&format=image/png`;
      case 'ndvi-add':
        return `http://kedu.ikit.sfu-kras.ru/service/vms?request=GetLegendGraphic&style=${NDVIUniversalStyle}&layer=workspace:${currentLayer.name}&format=image/png`;
    }
  };

  const html = `
    <button
      type="button"
      class="btn btn-info"
      onclick="switchLayerEditMode()"
    >Редактировать</button>
  
    <div class="legend">
      <img src="${getUrl()}">
    </div>
  `;

  $('div[data-name="SubTab_1_legend"] #layer-legend').html(html);
}



/**
 * Режим редактирования стиля
 */
function setLayerEditMode() {

  currentLayer.id = clickedLayerForInfoTab.id;
  currentLayer.name = clickedLayerForInfoTab.name;
  currentLayer.typeName = clickedLayerForInfoTab.typeName;

  /** @TODO Убрать */
  currentLayer.features = layerAttributes.features;

  setTimeout(() => {
    printEditModelView();
    setTimeout(selectAttribute, 10);
  }, 10);

  return;

  $.ajax({
    type: 'GET',
    url: `http://localhost:8080/geoserver/workspace/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${currentLayer.typeName}&maxFeatures=1500&outputFormat=json`,
    dataType: 'json',
    success: result => {
      console.log(result);

      if (!result || typeof result !== typeof {} || !Array.isArray(result.features)) {
        return console.error('Something went wrong');
      }

      currentLayer.features = result.features;

      setTimeout(() => {
        printEditModelView();
        setTimeout(selectAttribute, 10);
      }, 10);

    }
  });
}


/**
 * Вывести HTML для редактирования аттрибутов
 */
function printEditModelView() {
  const html = `    
      <div style="display: flex">

        <select
          id="layer-legend-attribute-select"
          class="form-control"
          style="margin-right: 5px"
          onchange="selectAttribute(this.value)">
          ${
            availableFeatureProperties().map(property =>
              `<option value="${property}">${property}</option>`
            )
          }
        </select>
      
        <button
          type="button"
          class="btn btn-success"
          style="margin-right: 5px"
          onclick="saveFeatures()"
        >Сохранить</button>
        
        <button
          type="button"
          class="btn btn-default"
          onclick="switchLayerEditMode()"
        >Закрыть</button>
    
      </div>
      
      <div class="form-inline" style="margin-top: 10px;">
        <input
          id="styleNameInput"
          style="padding: 5px; width: 96%;"
          placeholder="Название стиля (англиские символы)">
      </div>
  
      <div class="list" style="margin-top: 10px"></div>
    `;

  $('#layer-legend').html(html);
}


/**
 * Выбор атрибута из селекта
 *
 * @param attribute
 */
function selectAttribute(attribute = null) {
  currentLayerAttribute = attribute || availableFeatureProperties()[0] || null;

  const html = getFeaturesForAttribute()
    .map(feature => {

      const attributeStyle = (feature.stylesPerProperty || [])
        .find(style => style['PropertyName'] === currentLayerAttribute) || {};

      return `
        <div id="${feature.id}" style="padding: 5px; margin-bottom: 3px; border: 1px solid #808080; cursor: pointer">
          <div style="padding-bottom: 5px" onclick="$(this).next().slideToggle()">
            ${feature.properties[currentLayerAttribute]}
          </div>
          <div style="display: none">
           
            <div style="display: flex; justify-content: space-between">
              <div>
                Заливка: 
                <input
                  value="${attributeStyle['fill'] || '#fff'}"
                  data-feature-id="${feature.id}"
                  data-attribute-name="${currentLayerAttribute}"
                  data-opacity="${attributeStyle['fill-opacity'] || '1'}"
                  class="color-picker--layer-fill"
                  style="display: inline; width: 0; height: 26px;">
              </div>
            
              <div>
                Граница: 
                <input
                  value="${attributeStyle['stroke'] || '#fff'}"
                  data-feature-id="${feature.id}"
                  data-attribute-name="${currentLayerAttribute}"
                  class="color-picker--layer-stroke"
                  style="display: inline; width: 0; height: 26px;">
              </div>
              
              <div>
                Толщина: 
                <input
                  type="number"
                  value="${attributeStyle['stroke-width'] || '1'}"
                  oninput="setFeatureStyleProperty('${feature.id}', '${currentLayerAttribute}', 'stroke-width', this.value)"
                  style="display: inline; width: 50px;">
              </div>
                        
          </div>
        </div>
      `;
  });

  $('#layer-legend .list').html(html);

  $('.color-picker--layer-fill').each(function () {
      $(this).minicolors({
        change: (colorHex, opacity) => {
          const featureId = $(this).data('feature-id');
          const attributeName = $(this).data('attribute-name');
          setFeatureStyleProperty(featureId, attributeName, 'fill', colorHex);
          setFeatureStyleProperty(featureId, attributeName, 'fill-opacity', opacity);
        }
      });
    });

  $('.color-picker--layer-stroke').each(function () {
    $(this).minicolors({
      change: (colorHex) => {
        const featureId = $(this).data('feature-id');
        const attributeName = $(this).data('attribute-name');
        setFeatureStyleProperty(featureId, attributeName, 'stroke', colorHex);
      }
    });
  });
}


/**
 * Добавление свойств в массив features
 *
 * @param featureId
 * @param attributeName
 * @param property
 * @param value
 * @param forceUpdate
 */
function setFeatureStyleProperty(featureId, attributeName, property, value, forceUpdate = true) {
  const featureIndex = currentLayer.features
    .findIndex(feature => feature.id === featureId);

  if (featureIndex === -1) {
    return console.error(`index not found for feature: ${featureId}, attribName: ${attributeName}`);
  }

  if (!Array.isArray(currentLayer.features[featureIndex].stylesPerProperty)) {
    currentLayer.features[featureIndex].stylesPerProperty = [];
  }

  const styleIndex = currentLayer.features[featureIndex].stylesPerProperty
    .findIndex(style => style['PropertyName'] === attributeName);

  if (styleIndex !== -1) {
    if (!currentLayer.features[featureIndex].stylesPerProperty[styleIndex]) {
      currentLayer.features[featureIndex].stylesPerProperty[styleIndex] = {};
    }
    if (forceUpdate || !currentLayer.features[featureIndex].stylesPerProperty[styleIndex][property]) {
      currentLayer.features[featureIndex].stylesPerProperty[styleIndex][property] = value;
    }
  } else {
    const obj = {
      PropertyName: currentLayerAttribute,
      Literal:      (currentLayer.features[featureIndex].properties || [])[currentLayerAttribute],
    };
    obj[property] = value;
    currentLayer.features[featureIndex].stylesPerProperty.push(obj);
  }
}



/**
 * Сохранение features для выбранного атрибута
 */
function saveFeatures() {

  /** Стили для атрибута */
  const attributes = [];

  /** Дефолтные значения */
  getFeaturesForAttribute().forEach(feature => {

    setFeatureStyleProperty(
      feature.id,
      currentLayerAttribute,
      'fill',
      '#fff',
      false,
    );

    setFeatureStyleProperty(
      feature.id,
      currentLayerAttribute,
      'fill-opacity',
      '0',
      false,
    );

    setFeatureStyleProperty(
      feature.id,
      currentLayerAttribute,
      'stroke',
      '#fff',
      false,
    );

    setFeatureStyleProperty(
      feature.id,
      currentLayerAttribute,
      'stroke-width',
      '0',
      false,
    );

    const style = (feature.stylesPerProperty || [])
      .find(style => style['PropertyName'] === currentLayerAttribute);

    attributes.push(style);

  });


  let name = $('#styleNameInput').val();
  name = (name || '').trim();
  if (!name.length || name.replace(/[A-z\d\s]/g, '').length) {
    return alert('Имя стиля не задано, либо имеет не латинские символы/цифры');
  }

  const exportToDb = {
    name,
    title: name,
    attributes,
  };

  console.log(exportToDb);


  $.ajax({
    type: 'POST',
    data: exportToDb,
    url: '/Modules/XmlStyles/xml.php',
    dataType: 'json',
    success: result => {
      console.log(result);
      alert('Стиль создан');
    },
    error: err => {
      console.error(err);
      alert('Стиль не создан');
    },
  });
}




$(document).ready(function(){
    
    var authorizationBox = $("#authorization-box");
    authorizationBox.hide();
    
    $(".menu-authorization").click(function(){
        authorizationBox.slideDown(300);
    });
    
    $("#authorization-cancel").click(function(){
        authorizationBox.slideUp(300);
    });
    
    
    
    $(".printMap").click(function(){
        var baselayer = mapObject.baseLayer();
        var currentzoom = mapObject.zoom();
        var centercoords = mapObject.center();
        var windowheight = $(window).height();
        var openedLayers = mapObject.layers();
        var layersArray = openedLayers.getArray();
        var URLString = "http://edu.ikit.sfu-kras.ru/print?";
        if ( layersArray.length > 0 ) {
            URLString += "layers=";
            var zpt = ",";
            for(var i=0; i<layersArray.length; i++) {
                //alert(layersArray[i]._model._pkValue);
                if ( i == layersArray.length-1 ) {
                    zpt = "";
                }
                URLString += layersArray[i]._model._pkValue + zpt;
            }
            URLString += "&";
        }
        URLString += "lat="+centercoords.lat+"&lon="+centercoords.lng+"&zoom="+currentzoom+"&baselayer="+baselayer.id();
        window.open(URLString, "a", "resizable=yes,scrollbars=yes,width=1003,height="+windowheight+",left=10px, top=100px");
    });
    
    //{"layers":[{"id":226,"filters":null,"geomFilter":null}],"lat":56.55077850339641,"lon":93.23822021484375,"zoom":10,"baseLayer":-1780345091}
    
    // расчитываем высоту панели
    var windowHeight = $(window).height();
    var userPanelHeight = windowHeight-150;
    var userPanelDiv = $("#user-panel");
    userPanelDiv.height(userPanelHeight);
    
    // Скрываем панель по умолчанию
    userPanelDiv.hide();
    $(".roll-user-panel").click(function(){
        
        if ( userPanelDiv.attr('class') == 'up-show' )
        {
            userPanelDiv.slideDown(500);
            userPanelDiv.attr('class', 'up-hide');
            $(this).css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
            $(".zoom-control").css({"left":zoomControlShift});
        }
        else
        {
            userPanelDiv.slideUp(500);
            userPanelDiv.attr('class', 'up-show');
            $(this).css({"opacity":"0.5", "-webkit-opacity":"0.5", "-moz-opacity":"0.5", "-o-opacity":"0.5"});
            $(".zoom-control").removeAttr("style");
        }
    });
    
    var modes = '<div class="tab-content">' +
                    '<div id="div-popup-form">' +
                        '<button type="button" onclick="ShowFarmers()" id="set-farm">Закрепить за хозяйством</button>' + 
                        '<button type="button" onclick="RemoveFieldFromFarmer()" id="unset-farm">Убрать из хозяйства</button>' + 
                        '<button type="button" onclick="ShowCultures()" id="set-cultures">Указать культуру</button>' + 
                    '</div>' +
                '</div>';
                
    $("div[data-name='Tab_2_name']").prepend(modes);
    
    // ОТОБРАЖАЕМ ОТЧЕТЫ.
    var reports = '<div class="tab-content">' +
                        '<div id="my-reports">' +
                            '<button type="button" class="report1">Справка о посевных площадях</button>' +
                            '<button type="button" class="report2">План посевных площадей</button>' +
                            '<button type="button" class="report3">План весенне-полевых работ</button>' +
                        '</div>' +
                  '</div>';
    $("div[data-name='Tab_3_name']").prepend(reports);
    
    var reportButton = $("button[class*='report']");
    var reportType;
    // ОТОБРАЖАЕМ МОДАЛЬНОЕ ОКНО С ОТЧЕТАМИ.
    reportButton.click(function(){
        reportType = $(this).attr("class");
        title = $(this).text();
        $('span[class="ui-dialog-title"]').text(title);
        $("#dialog-box").attr('title', title);
        //var send = "report="+reportType+"&idf="+"<?//=$_SESSION['idFarmer']?>";
        var send = "report="+reportType;
        var dialogHeight = $("#map").height() - 50;
    
        $.ajax({
            url:"Modules/Reports/View/reports-view.php",
            data:send,
            success:function(result){
                $("div[class='dialog-content']").replaceWith(result);
            }
        });
        
        $("#dialog-box").dialog({
            minWidth: 980,
            height: dialogHeight,
            position: ['top',50],
            resizable: false,
            draggable: true
        });
    });
    // ВРЕМЕННО: ПРОВЕРКА ЗАПУСКА СКРИПТА НА LINUX ИЗ WEB-ИНТЕРФЕЙСА
    $("#start-inhomogeneity-algoritm").click(function(){
        $.ajax({
            url:"/test.php",
            
            success:function(result){
                $("div[id='object-info-form']").replaceWith(result);
            }
        });
    });
    
    // ОТОБРАЖАЕМ ДИНАМИЧЕСКИЕ СЛОИ ХОЗЯЙСТВА.
    var dynamicLayers = '<form id="dlayers-panel">' +
                            '<fieldset>' +
                                '<legend>Мои сельхоз контуры</legend>' +
                                '<div class="dlayers">' +
                                    '<div class="dlayer">' +
                                        '<div>' +
                                            '<span class="dynamic-info" id="farmer">' +
                                                '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                            '</span>' +
                                        '</div>' +
                                        '<div>' +
                                            '<input type="checkbox" class="show-saved-layer" id="my-farmer" ><label for="my-farmer"><span>Контуры хозяйства</span></label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="dlayer">' +
                                        '<div>' +
                                            '<span class="dynamic-info" id="plan">' +
                                                '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                            '</span>' +
                                        '</div>' +
                                        '<div>' +
                                            '<input type="checkbox" class="show-saved-layer" id="my-plan" ><label for="my-plan"><span>Культуры плановые</span></label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="dlayer">' +
                                        '<div>' +
                                            '<span class="dynamic-info" id="fact">' +
                                                '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                            '</span>' +
                                        '</div>' +
                                        '<div>' +
                                            '<input type="checkbox" class="show-saved-layer" id="my-fact" ><label for="my-fact"><span>Культуры фактические</span></label>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="dlayer">' +
                                        '<div>' +
                                            '<span class="dynamic-info" id="ndvi-add">' +
                                                '<img src="public/images/info-icon.png" title="Информация о слое" />' +
                                            '</span>' +
                                        '</div>' +
                                        '<div>' +
                                            '<input type="checkbox" class="show-saved-layer" id="my-ndvi-add" ><label for="my-ndvi-add"><span>История изменения растительности</span></label>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</fieldset>' +
                        '</form>';
    
    var subtabs = '<div class="ionTabs" id="subtabs_1" data-name="SubTabs_Group_name">' +
                        '<ul class="ionTabs__head">' +
                            '<li class="ionTabs__tab" data-target="SubTab_1_legend">Легенда</li>' +
                            '<li class="ionTabs__tab" data-target="SubTab_2_filter">Фильтры</li>' +
                            '<li class="ionTabs__tab" data-target="SubTab_3_metadata">Метаданные</li>' +
                        '</ul>' +
                        '<div class="ionTabs__body">' +
                            '<div class="ionTabs__item" data-name="SubTab_1_legend" style="padding: 20px 10px;"></div>' +
                            '<div class="ionTabs__item" data-name="SubTab_2_filter"><div id="layer-filter-form"></div></div>' +
                            '<div class="ionTabs__item" data-name="SubTab_3_metadata"><div id="layer-metadata-form"></div></div>' +
                            '<div class="ionTabs__preloader"></div>' +
                        '</div>' +
                  '</div>';
    
    $("div[data-name='Tab_4_name']").prepend(dynamicLayers + subtabs);
    
    // ОТОБРАЖАЕМ ЛЕГЕНДУ ДИНАМИЧЕСКОГО СЛОЯ.
    $(".dynamic-info").click(function(){
        var id = this.id;
        // Включаем subtab с легендой
        $.ionTabs.setTab("SubTabs_Group_name", "SubTab_1_legend");
        // Удаляем предыдущую легенду
        // $("#layer-legend").remove();

        setLayerLegendMode(id);

    //     if ( id == 'farmer' ) {
    //       $("div[data-name='SubTab_1_legend']").prepend(`
    //         <div id="layer-legend" class="tab-content">
    //
    //           <button
    //             type="button"
    //             onclick="switchLayerEditMode()"
    //           >Редактировать</button>
    //
    //           <div class="legend">
    //             <img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=${farmerUniversalStyleName}&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png">
    //           </div>
    //         </div>
    //       `);
    //     }
    //     if ( id == 'plan' ) {
    //         $("div[data-name='SubTab_1_legend']").prepend('<div id="layer-legend" class="tab-content"><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + culturesPlanUniversalStyleName + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></div>');
    //     }
    //     if ( id == 'fact' ) {
    //         $("div[data-name='SubTab_1_legend']").prepend('<div id="layer-legend" class="tab-content"><div class="legend"><img src="http://edu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + culturesFactUniversalStyleName + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></div>');
    //     }
		// if ( id == 'ndvi-add' ) {
    //         $("div[data-name='SubTab_1_legend']").prepend('<div id="layer-legend" class="tab-content"><div class="legend"><img src="http://kedu.ikit.sfu-kras.ru/service/wms?request=GetLegendGraphic&style=' + NDVIUniversalStyle + '&layer=workspace:contours_zshn_suchobuzimskoe_2015_vw&format=image/png" /></div></div>');
    //     }
    });
    
    // ОТОБРАЖАЕМ КАРТИНКУ ПЕРЕЛЕТА К СЛОЮ ПРИ КЛИКЕ НА CHECKBOX ДИНАМИЧЕСКОГО СЛОЯ.
    $('#dlayers-panel input[type="checkbox"]').click(function(){
        var id = this.id;
        if ( document.getElementById(id).checked == true ) {
            $(this).parent('div').after('<div style="float:right;"><span id="fly" class="fly fly-to-' + id + '"><img src="public/images/fly-to-layer.png" alt="Перелет к слою"></span></div>');
            // ВЫПОЛНЯЕМ ПЕРЕЛЕТ К СЛОЮ ПРИ КЛИКЕ НА КАРТИНКУ ПЕРЕЛЕТА.
            $('.fly-to-' + id).click(function(){
                // Позиционируем карту.
                if ( _minLat!=0 && _minLng!=0 && _maxLat!=0 && _maxLng!=0 ) {
                    mapObject.fitBounds(_minLng, _minLat, _maxLng, _maxLat);
                }
            }); 
        }
        else {
            $(".fly-to-"+id).remove();
        }
    }); 
    
    // ОТОБРАЖАЕМ ДИНАМИЧЕСКИЕ СЛОИ НА КАРТЕ ПРИ КЛИКЕ НА СООТВЕТСТВУЩИЙ CHECKBOX
    $("#my-farmer").click(function(){
        ShowMyLayer('farmer', this);
    });
    $("#my-plan").click(function(){
        ShowMyLayer('plan', this);
    });
    $("#my-fact").click(function(){
        ShowMyLayer('fact', this);
    });
    $("#my-ndvi-add").click(function(){
		NDVI.ShowNdviGeojsonLayer(this/*, '2016-05-16'*/);
    });

    // ИНИЦИАЛИЗИРУЕМ ИНТЕРФЕЙС TABS. ПРИМЕР: http://ionden.com/a/plugins/ion.tabs/#tabs|Product:Info|Company:History
    $.ionTabs("#tabs_1, #subtabs_1", {
        type: "storage",                    // hash, storage или none
        onChange: function(obj){         // функция обратного вызова
            //console.log(obj);
        }
    });
    // ИНИЦИАЛИЗИРУЕМ ИНТЕРФЕЙС TABS для SUBTUBS. ПРИМЕР: http://ionden.com/a/plugins/ion.tabs/#tabs|Product:Info|Company:History
    //$.ionTabs("#subtabs_1");
    /*$.ionTabs("#subtabs_1", {
        type: "storage",                    // hash, storage или none
        onChange: function(obj){         // функция обратного вызова
            console.log(obj);
        }
    });*/
    
    // ПРИ ЗМЕНЕНИИ ВЫСОТЫ ОКНА БРАУЗЕРА ПЕРЕСЧИТЫВАЕМ ВЫСОТУ ПРАВОЙ ПАНЕЛИ С ОБЩИМИ СЛОЯМИ.
    $(window).resize( function(){
        // Высота правой панели.
        $(RightPanelForm.selectors['divRightPanel']).height(RightPanel.calcHeight());
        $(GroupsForm.selectors['divAccordionContent']).height(Groups.calcContentHeight());
        $(ErsDataForm.selectors['divErsPanel']+" .layers").height(RightPanel.calcHeight()-120);
        
        // Высота левой панели.
        var windowHeight = $(window).height();
        var userPanelHeight = windowHeight-150;
        var userPanelDiv = $("#user-panel");
        userPanelDiv.height(userPanelHeight);
    } );
    
    //----------------------------НАЧАЛО-----------------------------------------
    //-----------------КАСИКОВ А.О. грКИ13-15Б-----------------------------------
    //---------------------------------------------------------------------------
    
    // Сразу добавляем кнопки, чтобы обработчик на них сработал 
    $("body").append('<div class="adding_button"><input id="printButton" class="printMode" value="Печать" type="submit"></input></div>');
    $("body").append('<div class="adding_button"><input id="exitMode" class="printMode" value="Выйти из режима печати" type="submit"></input></div>');
    
    // Изначально делаем их невидимыми
    $("#printButton").css("display", "none");
    $("#exitMode").css("display", "none");
    
    var choose = true;
   
    $(".printMode").on("click", function(){
        if(choose == true){ 
            $("#print-mode-hidden").val(1);
            $("head").append('<link rel="stylesheet" type="text/css" href="public/css/print.css">');
            $("body").append('<div class="textarea_class"><textarea placeholder="Комментарии:" id="comments" rows="6" name="comment"></textarea></div>');
            
            // Делаем кнопки видимыми
            $("#printButton").css("display", "block");
            $("#exitMode").css("display", "block");
            
            $("#printButton").css("color", "black");
            $("#exitMode").css("color", "black");
            
            // Убираем во время печати колонку слева (Хозяйства)
            $("#user-panel").css("display", "none");
            $("#user-panel").css("height", "0px");
            $(".zoom-control").css("left", "10px");
            
            choose = false;
        }
    });
    
    // Печать и выход из режима печати
    $("input#printButton").on("click", function(){           
        $("#printButton").css("display", "none");
        $("#exitMode").css("display", "none");
        
        $("#comments").attr("placeholder", "");
        
        window.print();
        
        $('link[href~="public/css/print.css"]').remove();
        $('textarea[id=comments]').remove();
        
        // Восстанавливаем прежний стиль для левой колонки (Хозяйства)
        $("#print-mode-hidden").val(0);
        $("#user-panel").css("display", "block");
        $("#user-panel").height(userPanelHeight);
        choose = true;
    });
    
    // Выход из режима печати
    $("input#exitMode").on("click", function(){           
        $('link[href~="public/css/print.css"]').remove();
        $('textarea[id=comments]').remove();
        
        $("#printButton").css("display", "none");
        $("#exitMode").css("display", "none");
        
        // Восстанавливаем прежний стиль для левой колонки (Хозяйства)
        $("#print-mode-hidden").val(0);
        $("#user-panel").css("display", "block");
        $("#user-panel").height(userPanelHeight);;
        
        choose = true;
    });
    
    //---------------------------------------------------------------------------
    //-----------------КАСИКОВ А.О. грКИ13-15Б-----------------------------------
    //------------------------------КОНЕЦ----------------------------------------
});
