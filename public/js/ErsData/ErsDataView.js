ErsDataForm = {
    selectors: {
        divErsPanel: "#ers-panel",
    },
    
    _scene: null,
    
    /**
    * @description Метод display() формирует скелет панели для отображения списка растровых сцен.
    */
    display: function() {
        $(RightPanelForm.selectors['divRightPanel']+' div.tab-content').append('<div class="tab-pane" id="rp-tab1">' +
                                                                        '<div id="ers-panel">'+
                                                                            '<div class="ers-title-bar">' +
                                                                                '<div style="width: 20px; margin-left: 3px;"><span></span></div>' +
                                                                                '<div style="width: 20px; margin-left: 1px;"><span></span></div>' +
                                                                                '<div style="width: 80px; margin-left: 2px;"><span>Спутник</span></div>' +
                                                                                '<div style="width: 80px;"><span>Дата</span></div>' +
                                                                                '<div style="width: 80px;"><span>Действие</span></div>' +
                                                                            '</div>' +
                                                                            '<div class="expand-filters">' +
                                                                                '<button type="button" id="expand-filters" class="expand-filters">Поиск снимков</button>'+
                                                                            '</div>' +
                                                                            '<div class="layers"></div>'+
                                                                        '</div>'+
                                                                     '</div>');
        // Запрос сцен по умолчанию представляет собой фильтр без атрибутов.
        var filterJson = {"type":"Filter", "attributes": ''};
        var rasters = ErsData._sendAjax(filterJson);
        $(ErsDataForm.selectors['divErsPanel']+' div.layers').empty();
        $(ErsDataForm.selectors['divErsPanel']+' div.layers').height(RightPanel.calcHeight()-120);
        this.displayErsData(rasters);
    },
    /**
    * @description Метод displayErsData() выводит список растровых сцен в элемент $('#ers-panel div.layers').
    * @param Array scenes: Массив растровых сцен, полученных после AJAX запроса.
    */
    displayErsData: function(scenes) {
        for ( var i=0; i<scenes.length; i++ ) {
            GeoPortal.findLayerById (
                scenes[i].id_layer,
                function(layer){
                    for ( var key in scenes )
                    {
                        if ( scenes[key].id_layer == layer.id() )
                        {
                            layer.metadata = scenes[key];
                            break;
                        }
                    }
                    // Записываем полученные слои в layersStore, который доступен везде. 
                    // Таким образом, в layersStore будут находиться не все возможные слои на Геосервере, а только те, которые отфильтрованы пользователем.
                    // Если за несколько фильтраций выбраны повторяющиеся слои, то в layersStore остаются только уникальные. Повторяющихся нет.
                    layersStore[layer.id()] = layer;
                    
                    var layerName = layer.rusName();
                    
                    shortLayerName = GetShortLayerName(layerName);
                    
                    $(ErsDataForm.selectors['divErsPanel']+' div.layers').append(
                        '<div class="layer">' +
                            '<div>' +
                                '<span class="info" id="' + layer.id() + '">' +
                                    '<img src="public/images/info-icon.png" title="Информация о слое">' +
                                '</span>' +
                            '</div>' +
                            '<div>' +
                                '<input type="checkbox" class="checkbox-layer" id="checkbox-layer-'+ layer.id() +'" value="'+ layer.id() +'">' +
                                '<label for="checkbox-layer-'+ layer.id() +'"><span></span>' +
                            '</div>' + 
                            '<div class="layer-name">' +
                                '<span title="' + layerName + '">' + layer.metadata.satellite_name + '</span>' +
                            '</div>' + 
                            '<div id="last-' + layer.id() + '" class="layer-name">' +
                                '<span>' + layer.metadata.date + '</span>' +
                            '</div>' +  
                        '</div>');
                },
                function(status,error){
                    console.log(error);
            });
        }
    },
}

ErsDataFormFilters = {
    selectors: {
        spanCoordinates: "div#layer-filter-form div.filter-box span.coordinates",
    },
    
    display: function() {
        var ERSFiltersHTML = '';
        // Вывод списка названий спутников в select
        var dataObject = {};
            dataObject['msi'] = true;
        var satellites = ErsData._sendAjax(dataObject);
        
        ERSFiltersHTML += '<div id="layer-filter-form" class="raster">'+
                                '<div class="filter-box">'+
                                    '<span class="filter-nameru">Период:</span>'+
                                    //'<label for="date-from">From</label>'+
                                    '<div><input type="text" id="date-from" name="date"></div>'+
                                    //'<label for="date-to">to</label>'+
                                    '<div class="dash"></div>'+
                                    '<div><input type="text" id="date-to"></div>'+
                                '</div>'+
                                '<div class="filter-box">'+
                                    '<span class="filter-nameru">Спутник:</span>'+
                                    '<select id="satellites" class="filter-entrance satellite_name" name="satellite_name" size="1">'+
                                        '<option value="">Все спутники</option>';
        
        for ( var i=0; i<satellites.length; i++ ) {
            ERSFiltersHTML += '<option value="'+satellites[i].satellite+'">'+satellites[i].satellite+'</option>';
        }
        
        ERSFiltersHTML +=           '</select>'+
                                '</div>'+
                                '<div class="filter-box">'+
                                    '<span class="filter-nameru">Разрешение:</span>'+
                                    '<select class="filter-compare s_resolution" size="1">'+
                                        '<option value="0">=</option><option value="1">></option><option value="2"><</option>'+
                                    '</select>'+
                                    '<input type="number" step="1" name="s_resolution" class="input-number">'+
                                '</div>'+
                                '<div class="filter-box">'+
                                    '<span class="filter-nameru">Облачность:</span>'+
                                    '<select class="filter-compare clouds_cover" size="1">'+
                                        '<option value="0">=</option><option value="1">></option><option value="2"><</option>'+
                                    '</select>'+
                                    '<input type="number" step="1" name="clouds_cover" class="input-number">'+
                                '</div>'+
                                '<div class="filter-box">'+
                                    '<span class="filter-nameru">Границы выделенной области:</span>'+
                                    '<button type="button" class="button-boundary" id="button-boundary"></button>'+
                                    '<span class="coordinates"></span>'+
                                '</div>'+
                                '<p style="margin:20px 0;">'+
                                    '<button type="button" class="button-clear" id="button-clear">Очистить</button>'+
                                    '<button type="button" class="button-show" id="button-show">Показать</button>'+
                                '</p>'+
                           '</div>';
        
        return ERSFiltersHTML;
    },
    clear: function(elems) {
        for ( var i=0; i<elems.length; i++ )
        {
            $(".filter-box input, select.satellite_name")[i]['value'] = '';
        }
        
        $("div.raster #date-from").datepicker( "option", "maxDate", '' );
        $("div.raster #date-to").datepicker( "option", "minDate", '' );
        
        if ( Filter._filter_bbox != null )
        {
            RectangleDraw.disable();
            Filter._filter_bbox = null;
            $(this.selectors['spanCoordinates']).empty();
        }
    },
}

ErsDataFormMetadata = {
    display: function(data) {
        var html = '<div id="layer-metadata-form">' +
                        '<div class="metadata-row">' +
                            '<span class="metadata-nameru">Идентификатор:</span>' +
                            '<span class="metadata-val">'+data[0].id_raster+'</span>' +
                        '</div>' +
                        '<div class="metadata-row">' +
                            '<span class="metadata-nameru">Спутник:</span>' +
                            '<span class="metadata-val">'+data[0].satellite_name+'</span>' +
                        '</div>' +
                        '<div class="metadata-row">' +
                            '<span class="metadata-nameru">Дата получения:</span>' +
                            '<span class="metadata-val">'+data[0].date+'</span>' +
                        '</div>' +
                        '<div class="metadata-row">' +
                            '<span class="metadata-nameru">Пространственное разрешение, м/пиксель:</span>' +
                            '<span class="metadata-val">'+data[0].s_resolution+'</span>' +
                        '</div>' +
                        '<div class="metadata-row">' +
                            '<span class="metadata-nameru">Облачность, %:</span>' +
                            '<span class="metadata-val">'+data[0].clouds_cover+'</span>' +
                        '</div>' +
                        '<div class="metadata-row">' +
                            '<span class="metadata-nameru">Расположение:</span>' +
                            '<span class="metadata-val">'+data[0].path+data[0].fname+'</span>' +
                        '</div>' +
                        '<div class="metadata-row">' +
                            '<span class="metadata-nameru">Координаты сцены:</span>' +
                            '<span class="metadata-val">'+data[0].raster_bbox+'</span>' +
                        '</div>' +
                        '<button type="button" class="metadata-expand">Полный текст метафайла<button>'+
                   '</div>';
        return html;
    },
}