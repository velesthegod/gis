$(document).ready(function(){
    // ВЫЗОВ ФОРМЫ ФИЛЬТРАЦИИ РАСТРОВЫХ СЦЕН
    $(document).on('click', ErsDataForm.selectors['divErsPanel']+" button#expand-filters", function(){
        
        var divUserPanel = $(ObjectInfo.selectors['divUserPanel']),
            divRollUserPanel = $(ObjectInfo.selectors['divRollUserPanel']);
            
        // Если ранее была выделена прямоугольная область фильтрации векторных объектов или растровых сцен, то очистить координаты и
        Filter._filter_bbox = null;
        // деактивировать контрол рисования прямоугольника
        if ( !RectangleDraw.disabled )
        {
            RectangleDraw.disable();
        }
        // Разворачиваем левую форму
        divUserPanel.slideDown(500); 
        divUserPanel.attr('class', 'up-hide'); 
        divRollUserPanel.css({"opacity":"1", "-webkit-opacity":"1", "-moz-opacity":"1", "-o-opacity":"1"});
        // Делаем активной нужную вкладку таба.
        $.ionTabs.setTab("Tabs_Group_name", "Tab_4_name");
        $.ionTabs.setTab("SubTabs_Group_name", "SubTab_2_filter");
        $(".zoom-control").css({"left":zoomControlShift});
        // Выводим форму
        var filter = ErsDataFormFilters.display();
        $("div[data-name='SubTab_2_filter'] #layer-filter-form").replaceWith(filter);
        // Инициализация календарей.
        // КАЛЕНДАРЬ ДЛЯ ЗАДАНИЯ НАЧАЛЬНОЙ ДАТЫ
        //$(ErsDataFormFilters.selectors['inputDateFrom']).datepicker({
        $("div.raster #date-from").datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
        }).on( "change", function() {
            $("div.raster #date-to").datepicker( "option", "minDate", $("div.raster #date-from").val() );
        });
        // КАЛЕНДАРЬ ДЛЯ ЗАДАНИЯ КОНЕЧНОЙ ДАТЫ
        $("div.raster #date-to").datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1
        })
        .on( "change", function() {
            $("div.raster #date-from").datepicker( "option", "maxDate", $("div.raster #date-to").val() );
        });
    });
    // ОБРАБОТКА НАЖАТИЯ НА КНОПКУ РИСОВАНИЯ ПРЯМОУГОЛЬНИКА
    $(document).on('click', 'div.raster button.button-boundary', function(){
        // Если ранее область уже была выделена, то очищаем предыдущие значения
        Filter._filter_bbox = null;
        // и активируем контрол заново.
        RectangleDraw._activateControl();
    });
    // ОБРАБОТКА НАЖАТИЯ НА КНОПКУ ФИЛЬТРАЦИИ СЦЕН
    $(document).on('click', "div.raster button.button-show", function(){
        // Перечисляются все интересующие элементы формы фильтрации, которые будут преобразованы в JSON.
        var dataObject = ErsData.convertFormDataToJSON($('div.raster input#date-from, input[type="number"], select.satellite_name, span.coordinates'));
        // Результат AJAX запроса, который возвращает массив объектов.
        var rasters = ErsData._sendAjax(dataObject);
        // Каждый раз при выполнении запроса очищается карта от уже открытых растровых слоев и 
        ErsData.clearMap();
        // предыдущий список растровых слоев на форме,
        $(ErsDataForm.selectors['divErsPanel']+' div.layers').empty();
        // затем выводится новый список.
        ErsDataForm.displayErsData(rasters);
    });
    // ОБРАБОТКА НАЖАТИЯ НА КНОПКУ "ОЧИСТИТЬ" ФОРМУ ФИЛЬТРАЦИИ СЦЕН
    $(document).on('click', "div.raster button.button-clear", function(){
        ErsDataFormFilters.clear($('div.raster input, select.satellite_name'));
    });
    // ВЫВЕСТИ ФАЙЛ МЕТАДАННЫХ РАСТРОВОЙ СЦЕНЫ В ДИАЛОГОВОЕ ОКНО
    $(document).on('click', "div#subtabs_1 div[data-name='SubTab_3_metadata'] #layer-metadata-form button.metadata-expand", function(){
        // Очистить содержимое модального окна, если туда уже было что то выведено.
        $("div.dialog-content").empty();
        // Вставить новое содержимое в модальное окно.
        $("div.dialog-content").append(ErsData._metadata);
        // Задать Заголовок модального окна
        $("#dialog-box").attr('title', 'Метаданные: ' + ErsData._rasterName);
        // Изменить Заголовок модального окна, если до этого были открыты окна с другими заголовками.
        $('span[class="ui-dialog-title"]').text('Метаданные: ' + ErsData._rasterName);
        // Отобразить диалоговое окно.
        $("#dialog-box").dialog({
            minWidth: 980,
            height: $("#map").height() - 50,
            position: ['top',50],
            resizable: false,
            draggable: true,
            close: function() {
                $("div.dialog-content").empty();
            }
        });
        
    });
});