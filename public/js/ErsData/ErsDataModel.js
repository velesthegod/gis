ErsData = {
    _ERS_GROUP_ID: 56,  // Идентификатор группы зарезервированной под данные ДЗЗ.
    
    _metadata: null,    // Свойство, которое хранит содержимое метафайла для вывода в модальное окно.
    
    _rasterName: '',    // Свойство, которое хранит название растра для вывода в панель заголовка модального окна.
    
    /**
    * @description Метод _sendAjax() возвращает массив метаданных растровых слоев.
    * @param Object dataObject: dataObject['date_from'], dataObject['date_to'], dataObject['satellite']
    * @returns Object
    */
    _sendAjax: function(dataObject) {
        var output = new Object();
        var url = 'Modules/Layers/View/layers-view.php';
        
        $.ajax({
            url:url,
            method:'POST',
            data:dataObject,
            cache: false,
            async: false,
            headers:[{
                'Access-Control-Request-Headers': 'X-Requested-With'
            }],
            xhrFields: {
                withCredentials:true
            },
            success:function(result){
                try {
                    output = JSON.parse(result);
                } catch (e) {
                    console.error(e.message);
                }
            },
            error:function(xhr, textStatus, errorTrown){
                console.log(xhr);
                $.stickr({note:'<p>Действие не выполнено.</p><p>Ошибка' + xhr + '</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
            },
            beforeSend: function(jqXHR, settings){}
        });
        return output;
    },
    /**
    * @description Метод FormToJSON() преобразует элементы формы фильтрации растровых сцен в формат JSON для передачи на сервер.
    * @param Array elems: Массив элементов формы (объектов).
    * @returns JSON
    */
    convertFormDataToJSON: function(elems) {
        var inputElems = elems;
        var attrJson = [];
        
        for (var i=0; i<inputElems.length-1; i++)
        {
            if ( inputElems[i]['value'] != "" )
            {
                if ( inputElems[i]['name'] == 's_resolution' )
                {
                    attrJson[i] = {
                        "name": inputElems[i]['name'],
                        "value": inputElems[i]['value'],
                        "operator": $('select.s_resolution').val(),
                    };
                }
                else if ( inputElems[i]['name'] == 'clouds_cover' )
                {
                    attrJson[i] = {
                        "name": inputElems[i]['name'],
                        "value": inputElems[i]['value'],
                        "operator": $('select.clouds_cover').val(),
                    };
                }
                else if ( inputElems[i]['name'] == 'date' )
                {
                    attrJson[i] = {
                        "name": inputElems[i]['name'],
                        "value": {
                            "date-from": "'"+$('div.raster input#date-from').val()+"'",
                            "date-to": "'"+$('div.raster input#date-to').val()+"'",
                        },
                    };
                }
                else
                {
                    attrJson[i] = {
                        "name": inputElems[i]['name'],
                        "value": "'"+inputElems[i]['value']+"'",
                    };
                }
            }
        }
        if ( Filter._filter_bbox != null )
        {
            attrJson.push({
                        "name": 'raster_bbox',
                        "value": Filter._filter_bbox.northWest().lng + ' ' +
                                 Filter._filter_bbox.northWest().lat + ', ' +
                                 Filter._filter_bbox.northEast().lng + ' ' +
                                 Filter._filter_bbox.northEast().lat + ', ' +
                                 Filter._filter_bbox.southEast().lng + ' ' +
                                 Filter._filter_bbox.southEast().lat + ', ' +
                                 Filter._filter_bbox.southWest().lng + ' ' +
                                 Filter._filter_bbox.southWest().lat + ', ' +
                                 Filter._filter_bbox.northWest().lng + ' ' +
                                 Filter._filter_bbox.northWest().lat,
            });
        }
        var filterJson = {"type":"Filter", "attributes": attrJson};
        //console.log(filterJson);
        return filterJson;
    },
    
    clearMap: function() {
        var layers = mapObject.layers();
        
        for (var key in layers.map) 
        {
            var layer = layers.map[key];
            // Если слой является векторным или растровым, а не полигоном (_bounds) или объектом _geojson, то он имеет свойство-объект _layersForMaps, то
            if ( typeof(layer._layersForMaps) == 'object' )
            {
                // проверить, что это космоснимок и убрать с карты.
                if ( layer._model._values.isKosmosnimok == true )
                {
                    mapObject.removeLayer(layer);
                }
            }
        }
    },
};
