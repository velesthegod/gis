
$(document).ready(function(){
    
    $(document).on('click', '#user-panel div.vector button.button-show', function(){
        var attrJson = Filter.FormToJSON($(Filter.selectors['input']));
        Filter.ExecuteCQL(attrJson);
    });
    
    $(document).on('click', '#user-panel div.vector button.button-clear', function(){
        var attrJson = Filter.FormToJSON($(Filter.selectors['input']));
        Filter.ClearForm(attrJson);
    });
    
    $(document).on('click', '#user-panel div.vector button.button-boundary', function(){
        // Если ранее область уже была выделена, то очищаем предыдущие значения
        Filter._filter_bbox = null;
        RectangleDraw._activateControl();
    });
    
});