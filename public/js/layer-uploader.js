
const layerUploaderFormHtml = `

   <div id="layer-uploader-form-container" style="display: flex; justify-content: center; align-items: center; position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 10001">
   
    <div
      style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; cursor: pointer; z-index: 10002; background-color: #19191957;"
      onclick="closeForm()"
    ></div>
   
    <div style="max-width: 500px; width: 100%; min-height: 400px; padding: 20px; background-color: #fff; z-index: 10003">
    
      <div style="margin-bottom: 10px; text-align: right">
      
         <button
          type="button"
          class="btn btn-sm btn-danger"
          onclick="closeForm()">
          Закрыть
        </button>
      
      </div>
      
  
      <div id="messages" style="margin-bottom: 10px;"></div>
  
  
      <div style="margin-bottom: 10px">
  
        <h3>Загрузка файла</h3>
        
        
        <div style="margin: 10px 0;">
        
          <div style="display: flex">
          
            <button
              type="button"
              class="btn btn-sm btn-secondary"
              onclick="openFilePicker()">
              Выбрать файл
            </button>
            
            <button
              type="button"
              class="btn btn-sm btn-success"
              style="margin-left: 10px"
              onclick="saveFile()">
              Сохранить слой
            </button>
          
          </div>
          
          
          <div
            id="layer-file-info"
            class="alert alert-primary"
            style="display: none; margin-top: 10px"
          ></div>
        
        </div>
  
      </div>
  
  
      <form id="layer-form">
  
        <div class="form-group">
          <label>Название</label>
          <input
            type="text"
            name="title"
            class="form-control"
            placeholder="Название">
        </div>
        
        <div class="form-group">
          <label>Английское название</label>
          <input
            type="text"
            name="title"
            class="form-control"
            placeholder="Название">
        </div>
  
        <div class="form-group">
          <label>Группа</label>
          <select
            name="group"
            class="form-control">
            <option value="">Выбрать</option>
            <option value="mygroup">kakogo?</option>
          </select>
        </div>
  
        <div class="form-group">
          <label>Хранилище данных</label>
          <select
            name="storage_type"
            class="form-control">
            <option value=""></option>
            <option value="4">mapadmin_store</option>
            <option value="3">editor_store</option>
          </select>
          
        </div>
  
  
        <ul
          id="layer-attributes"
          class="list-group mt-3"
        ></ul>
  
     </form>
    
  </div>
  
</div>
`;


const apiUrl = 'http://edu.ikit.sfu-kras.ru';

let layerFile = null;
let layerParamsModel = null;


/**
 * Открытие окна из шапки
 */
function onHeaderUploadBtnClicked() {
  $('body').append(layerUploaderFormHtml);
}

/**
 * Закрытие окна
 */
function closeForm() {
  $('#layer-uploader-form-container').remove();
}

function clearLayerFile() {
  layerFile = null;
  $('#layer-uploader-form-container').remove();
}


/**
 * Выбор файла
 */
function openFilePicker() {
  const fileInput = document.createElement('input');
  fileInput.type = 'file';

  fileInput.addEventListener('change', () => {
    if (!fileInput.files.length) {
      return;
    }
    layerFile = fileInput.files[0];
    $('#layer-file-info')
      .show()
      .html(fileInput.files[0].name);

    uploadLayerFile();
  });

  fileInput.click();
}



function getFileParams(fileIdentifier) {
  if (!(fileIdentifier || '').length) {
    return showMessage('Empty identifier', 'danger');
  }


  $.ajax({
    type: 'GET',
    url: `${apiUrl}/layers/upload/${fileIdentifier}?token=${_currentToken}`,
    dataType: 'json',
    success: result => {
      layerParamsModel = new LayerParamsModel();
      layerParamsModel.bootFromJson(result);

      console.log(layerParamsModel);
    },
    error: err => {
      showMessage(err, 'danger');
      console.error(err);
    },
  });
}


function uploadLayerFile() {

  if (!layerFile) {
    return showMessage('Выберите файл', 'danger');
  }

  $.ajax({
    type: 'POST',
    url: `${apiUrl}/layers/upload?token=${_currentToken}`,
    dataType: 'json',
    data: {
      ...layerFile,
    },
    success: (result) => {
      try {
        const identifier = result['data'][0];
        console.log(`Идентификатор: ${identifier}`);

        getFileParams(identifier);

      } catch (e) {
        showMessage(e.message, 'danger');
        console.error(e.message);
      }
    },
    error: err => {
      showMessage(err, 'danger');
      console.error(err);
    },
  });

  // let progress = 1;
  // const interval = setInterval(() => {
  //   $('#layer-file-progress').show();
  //   $('#layer-file-progress .progress-bar')[0].style.width = `${progress}%`;
  //   if (progress > 100) {
  //     clearInterval(interval);
  //     progress = null;
  //     $('#layer-file-progress').hide();
  //     $('#layer-file-info').show().html(fileInput.files[0].name);
  //   }
  //   progress += 20;
  // }, 500);
}



function saveFile() {

  if (!layerFile) {
    return showMessage('Выберите файл', 'danger');
  }

  const formData = $('#layer-form').serialize();

  console.log(formData);


}




function showMessage(text, type = 'info') {
  let html = `<div class="alert alert-${type} margin-bottom: 5px !important;">${text}</div>`;

  const $messages = $('#messages');
  $messages.prepend(html);

  const $firstElem = $messages.children().first();
  $firstElem.hide().fadeIn();

  setTimeout(() => {
    $firstElem.fadeOut();
    setTimeout(() => $firstElem.remove(), 350);
  }, 3000);
}



class LayerParamsModel {
  type; // 'vector'
  geometryType; // 'multipolygon'
  attributes;
// {
//   name,
//   rusName,
//   type, // 'MultiPolygon'
//   isGeometryField;
// }[];

  bootFromJson(data) {
    this.type = data.type;
    this.geometryType = data.geometryType;
    this.attributes = data.attributes || [];
  }


  static mock() {
    return {
      type: 'vector',
      geometryType: 'multipolygon',
      attributes: [
        {
          name: 'the_geom',
          rusName: 'the_geom',
          type: 'MultiPolygon',
          isGeometryField: true,
        },
        {
          name: 'the_geom2',
          rusName: 'the_geom2',
          type: 'MultiPolygon',
          isGeometryField: true,
        }
      ]
    };
  }

}

