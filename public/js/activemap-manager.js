
//var workingLayerId = 302;

/**
* @description Метод ShowFarmers() выводит список хозяйств.
* @returns Элемент select
*/
function ShowFarmers() {
    var send = "farmers_list=farmers";
    $.ajax({
        url:"Modules/Popup/View/popup-view.php",
        data:send,
        success:function(result){
            $("#div-popup-form").replaceWith(result);
        }
    });
}

/**
* @description Метод ShowCultures() выводит список культур.
* @returns Элемент select
*/
function ShowCultures() {
    var send = "cultures_list=cultures";
    $.ajax({
        url:"Modules/Popup/View/popup-view.php",
        data:send,
        success:function(result){
            $("#div-popup-form").replaceWith(result);
        }
    });
}

/**
* @description Метод SetObjectsAsFarmer() задает хозяйство выбранным контурам полей.
* @returns Слой хозяйства
*/
function SetObjectsAsFarmer() {
    if ( $("#select-farmers-list").val() == 'Выберите хозяйство ...' )
    {
        $.stickr({note:'<p>Выберите хозяйство из списка!</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
    }
    else
    {
        var send = "someString="+someString;
        
        $.ajax({
            url:"Modules/Popup/View/popup-view.php",
            type:'POST',
            data:send,
            success:function(result){
                $("#div-popup-form").replaceWith(result);
                $(".mapsurfer-popup-data p").delay(5000).fadeOut();   // TODO: сообщения
                
                for ( var i=0; i<selectorLayer.length; i++ )
                {
                    mapObject.removeLayer(selectorLayer[i]);
                }
                // Очищаем строку ID'шников, чтобы они не участвовали в следующем запросе
                idsString = '';
                someString = '';
                //agriculturalLayer.setOpacity(0.2);
                // Удаляем старый слой.
                mapObject.removeLayer(layerOfFarmer);
                // Чтобы слой не кэшировался, нужно инициализировать его еще раз и с другим параметром random.
                layerOfFarmer = new GeoPortal.Layer.WMS("http://edu.ikit.sfu-kras.ru/service/wms", {
                    service: 'WMS',
                    request: 'GetMap',
                    version: '1.1.1',
                    layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
                    styles: farmerUniversalStyleName,
                    format: 'image/png',
                    transparent: true,
                    cql_filter: filterCQLFarmer.filterString(),
                    token: _currentToken,
                    random: Math.random()
                });
                
                window.setTimeout(function(){
                    //mapObject.fitBounds(bbox.minx, bbox.miny, bbox.maxx, bbox.maxy);
                    // Включаем слой хозяйства
                    mapObject.addLayer(layerOfFarmer);
                }, 3000);
                
                // Ставим checkbox=true на "Показать мои контуры"
                $('#my-farmer').prop("checked", true);
                // Делаем активной нужную вкладку таба.
                $.ionTabs.setTab("Tabs_Group_name", "Tab_4_name");
            }
        });
        // Позиционируем карту
        //if ( _minLat!=0 && _minLng!=0 && _maxLat!=0 && _maxLng!=0 ) {
        //    mapObject.fitBounds(_minLng, _minLat, _maxLng, _maxLat);
        //}
    }
}

/**
* @description Метод SetCulture() задает культуру выбранным контурам полей.
* @param string type (type: plan|fact)
* @returns Слой плановых или фактических культур
*/
function SetCulture(type) {
    if ( $("#select-cultures-list").val() == 'Выберите культуру ...' )
    {
        $.stickr({note:'<p>Выберите культуру из списка!</p>',className:'sticker sticker-error', sticked:true, position:{right:320,top:98}});
    }
    else
    {
        var send = "ids="+idsString+"&cultureval="+$("#select-cultures-list").val()+"&type="+type+"&fid="+idFarmer;
        
        var currentLayer;
        if ( type == 'plan' )
        {
            // Удаляем старый слой.
            mapObject.removeLayer(layerCultures);
            // Чтобы слой не кэшировался, нужно инициализировать его еще раз и с другим параметром random.
            layerCultures = new GeoPortal.Layer.WMS("http://edu.ikit.sfu-kras.ru/service/wms", {
                service: 'WMS',
                request: 'GetMap',
                version: '1.1.1',
                layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
                styles: culturesPlanUniversalStyleName,
                format: 'image/png',
                transparent: true,
                cql_filter: filterCQLFarmer.filterString(),
                token: _currentToken,
                random: Math.random()
            });
            // Назначаем этот же слой, но с новыми параметрами.
            currentLayer = layerCultures;
        }
        else
        {
            // Удаляем старый слой.
            mapObject.removeLayer(layerCulturesFact);
            // Чтобы слой не кэшировался, нужно инициализировать его еще раз и с другим параметром random.
            layerCulturesFact = new GeoPortal.Layer.WMS("http://edu.ikit.sfu-kras.ru/service/wms", {
                service: 'WMS',
                request: 'GetMap',
                version: '1.1.1',
                layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
                styles: culturesFactUniversalStyleName,
                format: 'image/png',
                transparent: true,
                cql_filter: filterCQLFarmer.filterString(),
                token: _currentToken,
                random: Math.random()
            });
            // Назначаем этот же слой, но с новыми параметрами.
            currentLayer = layerCulturesFact;
        }
        
        $.ajax({
            url:"Modules/Popup/View/popup-view.php",
            data:send,
            success:function(result){

                $("#div-popup-form").replaceWith(result);
                $(".mapsurfer-popup-data p.hint").delay(5000).fadeOut(); // TODO: сообщения
                
                for ( var i=0; i<selectorLayer.length; i++ )
                {
                    mapObject.removeLayer(selectorLayer[i]);
                }
                // Очищаем строку ID'шников, чтобы они не участвовали в следующем запросе
                idsString = '';
                someString = '';
                // Ставим checkbox=true на "Показать плановые/фактические культуры".
                $("#my-" + type).prop("checked", true);
                // Делаем активной нужную вкладку таба.
                $.ionTabs.setTab("Tabs_Group_name", "Tab_4_name");
                // Включаем слой культур.
                mapObject.addLayer(currentLayer);
            }
        });
    }
}

/**
* @description Метод RemoveFieldFromFarmer() убирает выбранные контуры полей из хозяйства.
* @returns Слой хозяйства
*/
function RemoveFieldFromFarmer() {
    var send = "ids="+idsString+
               "&rm=1"+
               "&idFarmer="+idFarmer;
    
    $.ajax({
        url:"Modules/Popup/View/popup-view.php",
        data:send,
        success:function(result){
            $("#div-popup-form").replaceWith(result);
            $(".mapsurfer-popup-data p").delay(5000).fadeOut(); // TODO: сообщения
            
            for ( var i=0; i<selectorLayer.length; i++ )
            {
                mapObject.removeLayer(selectorLayer[i]);
            }
            
            // Очищаем строку ID'шников, чтобы они не участвовали в следующем запросе
            idsString = '';
            someString = '';            
            // Удаляем старый слой.
            mapObject.removeLayer(layerOfFarmer);
            // Чтобы слой не кэшировался, нужно инициализировать его еще раз и с другим параметром random.
            layerOfFarmer = new GeoPortal.Layer.WMS("http://edu.ikit.sfu-kras.ru/service/wms", {
                service: 'WMS',
                request: 'GetMap',
                version: '1.1.1',
                layers: 'workspace:contours_zshn_suchobuzimskoe_2015_vw',
                styles: farmerUniversalStyleName,
                format: 'image/png',
                transparent: true,
                cql_filter: filterCQLFarmer.filterString(),
                token: _currentToken,
                random: Math.random()
            });
            
            window.setTimeout(function(){
                // Включаем слой хозяйства
                mapObject.addLayer(layerOfFarmer);
            }, 3000);
            
            // Ставим checkbox=true на "Показать мои контуры"
            $("#my-farmer").prop("checked", true);
            // Делаем активной нужную вкладку таба.
            $.ionTabs.setTab("Tabs_Group_name", "Tab_4_name");
        }
    });
}

/**
* @description Метод ShowMyLayer() включает динамические слои текущего хозяйства.
* @param type string Тип слоя
* @param element Object checkbox
* @returns Слой хозяйства|плановые культуры|фактические культуры
*/
function ShowMyLayer(type, element) {
    var currentLayer;
    var currentElementId = element.id;
    
    if ( type == 'plan' ) 
    {
        currentLayer = layerCultures;
    }
    if ( type == 'fact' )
    {
        currentLayer = layerCulturesFact;
    }
    if ( type == 'farmer' )
    {
        currentLayer = layerOfFarmer;
    }
    /*if ( type == 'ndvi-mean' )
    {
        currentLayer = layerNDVIMean;
    }*/
    
    if ( document.getElementById(currentElementId).checked == true )
    {
        // Отображаем слой
        mapObject.addLayer(currentLayer);
        // Позиционируем карту
        if ( _minLat!=0 && _minLng!=0 && _maxLat!=0 && _maxLng!=0 ) {
            mapObject.fitBounds(_minLng, _minLat, _maxLng, _maxLat);
        }
        return true;
    }
    else
    {    
        mapObject.removeLayer(currentLayer);
        return false;
    }
}

/**
* @description GetBbox() вычисляет Bounding Box объекта
* @param object objGeom - объект, содержащий геометрию объекта слоя. Например, var objGeom = JSON.parse(features[0]._model._values.data.geom);
* @returns Bbox coordinates (minLat minLng maxLat maxLng)
*/
function GetBbox(objGeom) {
    
    // Алгоритм выдает minLat minLng maxLat maxLng
    var coordToStr = JSON.stringify(objGeom.coordinates);
    var delSubstring = coordToStr.replace(/\],\[/g,':');
    var delSubstring2 = delSubstring.replace(/\[/g,'');
    var delSubstring3 = delSubstring2.replace(/\]/g,'');
    var coordsToArray = delSubstring3.split(':');
    var coordArrayLength = coordsToArray.length;
    // Готовим массив из отдельно взятых координат
    var coordsArrayToStr = coordsToArray.toString();
    var coordsStrToMixArray = coordsArrayToStr.split(',');
    
    var LongitudeArray = new Array();
    var LatitudeArray = new Array();
    for ( var i=0; i<coordsStrToMixArray.length/2; i++ )
    {
        LongitudeArray[i] = coordsStrToMixArray[i*2];
        LatitudeArray[i] = coordsStrToMixArray[(i*2)+1];
    }
    
    var _current_bbox = new Array();
    _current_bbox['minLat'] = Math.min.apply(null, LatitudeArray);
    _current_bbox['maxLat'] = Math.max.apply(null, LatitudeArray);
    _current_bbox['minLng'] = Math.min.apply(null, LongitudeArray);
    _current_bbox['maxLng'] = Math.max.apply(null, LongitudeArray);
    
    return _current_bbox;
    
}

/**
* @description CompareCoordinatesOfContoursBbox() вычисляет максимальный Bounding Box, сравнивая BBox's двух объектов
* @param object bbox1 - BBox сравниваемого объекта
* @param object bbox2 - BBox эталонного объекта
* @returns Bbox coordinates (minLat minLng maxLat maxLng)
*/
function CompareCoordinatesOfContoursBbox(bbox1, bbox2) {
    
    //console.log("Compare=> ");
    //console.log(bbox2);
    if ( bbox1.minLat < bbox2.minLat ) {
        //console.log("minLat: "+_compare_bbox.minLat+" > "+dwnld_usgs_params.minLat+" => "+dwnld_usgs_params.minLat);
        bbox2.minLat = bbox1.minLat;
    }
    if ( bbox1.maxLat > bbox2.maxLat ) {
        //console.log("maxLat: "+_compare_bbox.maxLat+" < "+dwnld_usgs_params.maxLat+" => "+dwnld_usgs_params.maxLat);
        bbox2.maxLat = bbox1.maxLat;
    }
    if ( bbox1.minLng < bbox2.minLng ) {
        //console.log("minLng: "+_compare_bbox.minLng+" > "+dwnld_usgs_params.minLng+" => "+dwnld_usgs_params.minLng);
        bbox2.minLng = bbox1.minLng;
    }
    if ( bbox1.maxLng > bbox2.maxLng ) {
        //console.log("maxLng: "+_compare_bbox.maxLng+" < "+dwnld_usgs_params.maxLng+" => "+dwnld_usgs_params.maxLng);
        bbox2.maxLng = bbox1.maxLng;
    }
    
    return bbox2;
}

/**
* @description Метод Cancel() возвращает на предыдущий уровень меню.
* @returns Форма предыдущего уровеня меню
*/
function Cancel() {
    var send = "cancel=true";
    $.ajax({
        url:"Modules/Popup/View/popup-view.php",
        data:send,
        success:function(result){
            $("#div-popup-form").replaceWith(result);
        }
    });
}

function defineBrowser()
{
    var browserIs;
    var isIE11 = navigator.userAgent.match(/rv:11.0/);
    if( isIE11 != undefined && isIE11.length == 1 ) {
        browserIs = 'ie';
    }
    var isGoogleChrome = navigator.userAgent.match(/Chrome/);
    if( isGoogleChrome != undefined && isGoogleChrome.length == 1 ) {
        browserIs = 'chrome';
    }
    var isFirefox = navigator.userAgent.match(/Firefox/);
    if( isFirefox != undefined && isFirefox.length == 1 ) {
        browserIs = 'firefox';
    }
    return browserIs;
}

function BrowserIs(obj)
{
    var result;
    var browserIs = defineBrowser();
    $.each(obj, function(index, value) {
        if ( browserIs == index ) {
            result = value;
        }
    }); 
    
    return result;
}

/*function EnableWorkingLayer(layerObject)
{
    // Для упрощения использования программы при входе в нее нужно включить рабочий слой и перелететь к нему автоматически.
    // Для этого убедимся, что рабочий слой (он же первый в списке) имеет ID=302, так как в неавторизованном режиме ID первого слоя может быть каким угодно, 
    if ( layerObject.id() == Users.workingLayerId ) {
        // получим Bbox рабочего слоя, 
        layerObject.requestBbox( 
            function(bbox){ 
                // включим рабочий слой на карте, 
                mapObject.addLayer(layerObject);
                // в правой панеле поставим галочку на checkbox рабочего слоя, 
                $("#checkbox-layer-"+Users.workingLayerId).attr("checked", "checked");
                //$("#checkbox-layer-302").after('<span class="fly" id="fly-302"><img src="public/images/fly-to-layer.png" title="Перелет к слою" /></span>'); 
                // перелетим к рабочему слою.
                // Для перелета необходимо либо по событию onready указать карте Bbox,
                /*mapObject.on("ready",function(){
                        mapObject.fitBounds(bbox.minx, bbox.miny, bbox.maxx, bbox.maxy);
                },this);*/
                // либо установить задержку в 1 сек. перед выполнением функции перелета, чтобы карта успела спозиционироваться. Оба варианта работают.
/*                window.setTimeout(function(){
                    mapObject.fitBounds(bbox.minx, bbox.miny, bbox.maxx, bbox.maxy);
                }, 1000);
            }, 
            function(status, error){ 
                console.log(error); 
        });
    }
}*/

function GetShortLayerName(layerName)
{
    var layerNameLen = layerName.length; 
    var shortLayerName = '';
    var count = 22; 
    if ( layerNameLen > count ) 
    { 
        for ( i=0; i<=count; i++ ) 
        { 
            shortLayerName += layerName[i]; 
        } 
        return shortLayerName = shortLayerName + '...';
    } 
    else 
    { 
        return shortLayerName = layerName; 
    }
}

/**
* @description Метод ShowNDVIStats() передает идентификатор контура __gid для отображения графика статистики NDVI в dialog-box
* @returns График статистики NDVI
*/
function ShowNDVIStats()
{
    var selectedItems = new Array();
    $("#user-panel #object-info-form .checkboxes-list input[class='ndvi-src']:checked").each(function() {
        selectedItems.push($(this).val());
    });
    
    var ndviSrc = "";
    for (i=0; i<selectedItems.length; i++) {
        ndviSrc += selectedItems[i] + ",";
    }
    // alert(ndviSrc);

    var selectedYears = new Array();
    $(".select-ndvi-year").each(function(){
        selectedYears.push($(this).val());
    });
    
    var years = "";
    for (i=0; i<selectedYears.length; i++) {
        years += selectedYears[i];
    }
    // alert(years);
    
    if ( years == "null" ) {
        years = '';
    }
    
    var dialogHeight = $("#map").height() - 50;
    
    var send = "gid="+gid+"&ndvi_year="+years+"&ndvi_src="+ndviSrc;
    
    $.ajax({
        url:"Modules/Popup/View/popup-view.php",
        data:send,
        success:function(result){
            
            $("div[class='dialog-content']").replaceWith(result);
        }
    });
    // Задаем Заголовок модального окна
    $("#dialog-box").attr('title', 'Статистика динамики вегетации (NDVI)');
    // Изменяем Заголовок модального окна, если до этого были открыты окна с другими заголовками.
    $('span[class="ui-dialog-title"]').text('Статистика динамики вегетации (NDVI)');
    $("#dialog-box").dialog({
        minWidth: 980,
        height: dialogHeight,
        position: ['top',50],
        resizable: false,
        draggable: true
    });
}