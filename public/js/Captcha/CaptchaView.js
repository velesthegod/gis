CaptchaForm = {
    selectors: {
        "refreshCaptchaButton": "#refresh-captcha",
        "captchaImg": "#captcha-img",
    },
    
    elementsNames: {
        "refreshCaptchaButton": "refresh-captcha",
        "captchaImg": "captcha-img",
    },
    
    display: function() {
        var html = '<input id="captcha" type="text" placeholder="Введите код с картинки">' +
                   '<div id="captcha-wrap">' +
                        '<input type="button" alt="Обновить картинку" id="'+this.elementsNames['refreshCaptchaButton']+'"></button>' +
                        '<img src="'+Captcha.captchaGenPath+'" id="'+this.elementsNames['captchaImg']+'" />' +
                   '</div>';
        return html;
    },
}