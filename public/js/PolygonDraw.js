GeoPortal.Control.PolygonDraw = GeoPortal.Control.DirectButton.extend({
    
    //ls: [], 
    //lId: 0,
    
    mainOptions: {
        popups: true,
        buttonId: "polygondraw",
        title: "Добавить объект",
        image: "/public/images/plus.png",
        bindClick: true
    },

    //отрисовка контроллера
    _draw: function(){ 
        GeoPortal.Control.DirectButton.prototype._draw.call(this);
        this._polygon = new GeoPortal.Vector.Polygon([], {editable: true});
        this._polygon.on('edit', this._update, this);
        this._polygon.on('click', function(e) {});
        this._active = false;
        //вывод формы
        //this._distanceBox = undefined;
        this._map.addLayer(this._polygon);
        this._polygon_disable();
    },

    _remove: function(){
        this._polygon.off('edit', this._update, this);
        this._polygon.off('click', function(e) {});
        this._active = false;
        this._polygon_disable();
        this._polygon = undefined;
        GeoPortal.Control.DirectButton.prototype._remove.call(this);
    },
//
    _createJson: function(){
        var latLngs,
            len,
            i = 0,
            coordinates = new Array(),
            latLng;

            latLngs = this._polygon.latLngs();
            len = latLngs.length;

        for(i=0;i<len;i++){
            latLng = latLngs[i];
            coordinates.push(new Array(latLng.lng,latLng.lat));
        }
        coordinates.push(new Array(latLngs[0].lng,latLngs[0].lat));
        //console.log("coordinates",coordinates); //выводит координаты в консоль просто в виде массива координат
        var geoJson;
        if(this.options.type == "MULTIPOLYGON"){
            geoJson = {
                type: "MultiPolygon",
                coordinates: new Array(new Array(coordinates))
            }
        }
        else{
            geoJson = {
                type: "Polygon",
                coordinates: new Array(coordinates)
            }
        }
        // debug
        // console.log("geoJson",geoJson); //выводит координаты в консоль в виде типа объекта и массива координат (фортмат geojson)
        this._geojson = geoJson;
        this.fire("geometry:created",{geojson:geoJson});
    },
    
    geoJson: function(){
        this._createJson();
        return this._geojson;
    },

    //Активирование контрола
    _activateControl: function(){
        GeoPortal.Control.DirectButton.prototype._activateControl.call(this);
        if (this._active){
            this._disableControl();
        }
        else{
            this._element.children("div").addClass("current");
            this._polygon_enable();
            this.fire("control:DrawPolygon:enable");
        }
    },

    //Деактивирование контрола
    _disableControl: function(){
        GeoPortal.Control.DirectButton.prototype._disableControl.call(this);
        this._polygon_disable();
        this._reset();
        this.fire("control:DrawPolygon:disable");
    },

    //Возвращает линию
    getLine: function() { return this._polygon; },

    _polygon_enable: function() {
        this._map._map.on('click', this._add_points, this);
        this._map._map.container().style.cursor = 'crosshair';
        this._map.addLayer(this._polygon);
        this._active = true;
        this._polygon.editing.enable();
        if (!this._map._map.hasLayer(this._polygon))
            this._map.addLayer(this._polygon);

    },

    _polygon_disable: function() {
        this._map._map.off('click', this._add_points, this);
        this._map.removeLayer(this._polygon);
        this._map._map.container().style.cursor = 'default';
        this._active = false;
        this._polygon.editing.disable();
    },

    _reset: function(){
        this._polygon.setLatLngs([]);
        this._polygon.editing.updateMarkers();
    },

    _add_points: function (e) {
        var len = this._polygon.latLngs().length;
        this._polygon.addLatLng(e.latlng);
        this._polygon.editing.updateMarkers();
        this._polygon.fire('edit', {});
            if(this._polygon.latLngs().length == 1)
            this.fire("geometry:created");
    },
});