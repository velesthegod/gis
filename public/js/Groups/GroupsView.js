GroupsForm = {
    selectors: {
        divGroups: "#groups",
        divAccordionContent: "#layers-panel div.ui-accordion-content",
    },
    
    /**
    * @description Метод drawGroup() отображает одну группу.
    * @param Object group: Объект, содержащий данные о группе.
    * @param integer slice_number: Не обязательный параметр. Если группы разделены на tab вкладки, то задает номер tab.
    */
    displayGroupRow: function(group, slice_number) {

        if(typeof group == "undefined")
            return;

        var divGroups = $(this.selectors['divGroups']+slice_number),
        groupDiv, layersDiv, layers, key;
        
        divGroups.append('<h3>' + group.name() + '</h3>' + 
                '<div class="group"><div class="layers"></div></div>'); 
        groupDiv = divGroups.last(); 
        layersDiv = groupDiv.find(".layers:last"); 
        layers = group.layers();
        
        for(key in layers) {
            // layersStore объявляется в header, но заполняется тут при формировании строк слоев в правой панеле!
            layersStore[layers[key].id()] = layers[key]; 
            
            var layerName = layers[key].rusName(); 
            
            shortLayerName = GetShortLayerName(layerName); 
            
            layersDiv.append('<div class="layer">' +
                                '<div>' +
                                    '<span class="info" id="' + layers[key].id() + '">' +
                                        '<img src="public/images/info-icon.png" title="Информация о слое">' +
                                    '</span>' +
                                '</div>' +
                                '<div id="last-' + layers[key].id() + '">' +
                                    '<input type="checkbox" class="checkbox-layer" id="checkbox-layer-'+ layers[key].id() +'" value="'+ layers[key].id() +'">' +
                                    '<label for="checkbox-layer-'+ layers[key].id() +'"><span>' + shortLayerName + '</span>' +
                                '</div>' + 
                             '</div>');
        }
    },
    /**
    * @description Метод drawGroups() отображает список групп и оформляет их как jQuery widget - accordion.
    */
    display: function(){
        if ( Groups.groups.length >= 1 ) {
            var subgroups = [];
            
            for ( var num=0; num<Math.ceil(Groups.groups.length/Groups._elementsPerTab); num++ ) {
                subgroups[num] = Groups.groups.slice((num*Groups._elementsPerTab), (num*Groups._elementsPerTab) + Groups._elementsPerTab);
                
                if ( num == 0 ) {
                    $(LayersForm.selectors['divLayersPanel']+' ul.nav').after('<div class="tab-content"></div>');
                    var activeLi = 'class="active"',
                    activeTabPane = 'class="tab-pane active"';
                }
                else {
                    var activeLi = '',
                    activeTabPane = 'class="tab-pane"';
                }
                $(LayersForm.selectors['divLayersPanel']+' ul.nav').append('<li '+activeLi+'><a href="#tab'+num+'" data-toggle="tab">'+(num+1)+'</a></li>');
                $(LayersForm.selectors['divLayersPanel']+' div.tab-content').append('<div '+activeTabPane+' id="tab'+num+'"><div id="groups'+num+'"></div></div>');
                for ( var i=0; i<subgroups[num].length; i++ ) {
                    if ( subgroups[num][i]._model._pkValue != ErsData._ERS_GROUP_ID ) {
                        this.displayGroupRow(subgroups[num][i], num);
                    }
                }
            }
            
            // Changing DEFAULT Settings for jQuery accordion
            $("[id^='groups']").accordion();
            $(this.selectors['divAccordionContent']).height(Groups.calcContentHeight());
            $(".ui-accordion-header").click(function(){
                $(".ui-accordion-header").css({"color":"#FFFFFF", "background":"#9EB5BF", "border":"0px solid #6D8692"});
                $(".ui-accordion-header-active").css({"color":"#FFFFFF", "background":"#6D8692", "border":"0px solid #6D8692"});
            });
        }
    },
}