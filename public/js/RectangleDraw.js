GeoPortal.Control.RectangleDraw = GeoPortal.Control.DirectButton.extend({
    mainOptions: {
        buttonId: "rectangleDrawButton",
        title: GPMessages("rectangleDraw.select"),
        image: GeoPortal.basePath + "/public/images/icons/bbox-search.png",
        bindClick: true
    },

    _draw: function(){
        GeoPortal.Control.DirectButton.prototype._draw.call(this);

        this._enabled = false;
        this._turn = false;
        this._handler = new M.Rectangle.Draw(this._map._map, {initialLabelText: GPMessages("rectangleDraw.initialLabel"), releaseLabelText: GPMessages("rectangleDraw.releaseLabel")});
        this._polygon = undefined;
        this._map._map.on('draw:rectangle-created',this._rectangleCreated,this);
    },

    _remove: function(){
        this.disable();
        this._unbind(this._mainElement,"click",this._activateControl);
        this._handler = undefined;

        GeoPortal.Control.DirectButton.prototype._remove.call(this);
    },

    _activateControl: function(){
        if(this._turn){
            this.disable();
            return;
        }

        this._removePolygon();
        this._handler.enable();
        this._enabled = true;
        this._turn = true;
        this.fire("control:RectangleDraw:enable");
    },

    _removePolygon: function(){
        if(typeof this._polygon != 'undefined' && typeof this._map != 'undefined')
            this._map.removeLayer(this._polygon);
        this._polygon = undefined;
    },

    _rectangleCreated: function(e){
        if(!this._enabled)
            return;
        this._enabled = false;
        var latLngs = e.rect.latLngs();

        this._handler.disable();
        this._polygon = e.rect;
        this._map.addLayer(this._polygon);
        this.fire("control:RectangleDraw:created",{latLngs:latLngs});
    },

    disable: function(){
        this._removePolygon();
        this._handler.disable();
        this._enabled = false;
        this._turn = false;
        this.fire("control:RectangleDraw:disable");
    }
});
