LayersForm = {
    selectors: {
        divLayersPanel: "#layers-panel",
    },
    
    /**
    * @description Метод drawLayersPanel() формирует скелет панели для отображения списка ОБЩИХ СЛОЕВ.
    */
    display: function(){
        $(RightPanelForm.selectors['divRightPanel']+' div.tab-content').append('<div class="tab-pane active" id="rp-tab0"><div id="layers-panel"></div></div>');
        
        $(this.selectors['divLayersPanel']).addClass('tabbable');
        $(this.selectors['divLayersPanel']).prepend('<ul class="nav nav-tabs"></ul>');
    },
}