Layers = {
    
    enableWorkingLayer: function(layerID) {
        var layer = layersStore[layerID];
        // Включить рабочий слой на карте,
        mapObject.addLayer(layer);
        // в правой панеле поставить галочку на checkbox рабочего слоя.
        $("#checkbox-layer-"+layerID).attr("checked", "checked");
    },
};