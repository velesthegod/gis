
$(document).ready(function(){
    
    // Обрабатываем нажатие на кнопку редактирования
    $(ObjectInfo.selectors['divUserPanel']).on('click', 'button.changek', function(){
        if ( ObjectInfo._geom.coordinates.length > 0 )
        {
            // Преобразуем массив координат в массив чисел
            var newArr = [[]];

            for (var i = 0; i < ObjectInfo._geom.coordinates.length; i++) {
                newArr.push(ObjectInfo._geom.coordinates[0][i].reduce((a,b)=>a.concat(b)));
            }
            newArr.shift();
            // Результирующий массив чисел
            var NM = newArr[0];
            
            // Создаем массив объектов для создания копии объекта (каждая точка это объект)
            var LatLngP = new Array();
            for ( var i = 0; i < NM.length; i++ ){
                LatLngP.push(new GeoPortal.LatLng(NM[i+1], NM[i], true));
                i = i+1;
            }

            // Создаем слой-копию объекта и выводим ее на карту в редактируемом виде
            ObjectInfo._layerEditor = new GeoPortal.Vector.Polygon(LatLngP);
            mapObject.addLayer(ObjectInfo._layerEditor);
            ObjectInfo._layerEditor.editing.enable();
            
            editingLayerId = $('#user-panel button.changek').attr("layerid");
            
            ObjectInfo._layer = layersStore[editingLayerId];
            var editForm = ObjectInfo.EditForm();
            $("div[id='object-info-form']").replaceWith(editForm);
        }
    });
    
    // Обрабатываем нажатие на кнопку отмены редактирования
    $(ObjectInfo.selectors['divUserPanel']).on('click', 'button.change-object-cancel', function(){
        ObjectInfo._layerEditor.editing.disable();
        mapObject.removeLayer(ObjectInfo._layerEditor);
        ObjectInfo._layerEditor = null;
        ObjectInfo._layerSelector = null;
        ObjectInfo._layer = layersStore[editingLayerId];
        var infoForm = ObjectInfo.InfoForm();
        $("div[id='object-info-form']").replaceWith(infoForm);
    });
    
    // Обрабатываем нажатие на кнопку сохранения редактирования
    $(ObjectInfo.selectors['divUserPanel']).on('click', 'button.change-object-save', function(){
        
        console.log(_currentToken);
        
        var fieldID = $(this).attr('fieldid');
        var layerID = $(this).attr('layerid');
        
        // Получаем все input и формируем JSON атрибутов для отправки.
        var attrJson = ObjectInfo.FormToJSON($('#object-info-form input'));
        
        // Создаем массив координат, в котором только дырки в полигоне
        var holesCoords = ObjectInfo.holesCoordinates(ObjectInfo._geom.coordinates);
        
        // Измененные координаты для сохранения.
        // Поменять местами широту и долготу.
        ObjectInfo._layerEditor.editing.disable();
        var coordJson = ObjectInfo._layerEditor._latlngs.map(function(e){
              var lng = e.lng;
                e.lng = e.lat;
                e.lat = lng;
                return e;
        });
        
        // Координаты нового объекта.
        var coordJson2 = ObjectInfo._layerEditor._latlngs.map(function(e){
            return [e.lat, e.lng];
        });
        
        // Приводим координаты к единому виду для добавления в geoJson
        var coordJson3 = [coordJson2];
        var coordJson4 = coordJson3.concat(holesCoords);
        
        // Добавить новый объект
        var coordJson = ObjectInfo._layerEditor._latlngs;
        geoJson = {"type":"MultiPolygon", "coordinates": [coordJson4], };
        
        // СОХРАНЯЕТСЯ ГЕОМЕТРИЯ И АТРИБУТЫ
        if ( !confirm('Вы уверены, что хотите сохранить изменения?') )
            return;
        
        
        
        var saveObject = {};
            saveObject['attributes'] = attrJson;
            saveObject['token'] = _currentToken;
            saveObject['geometry'] = geoJson;
            saveObject['fieldid'] = fieldID;
            saveObject['layerid'] = layerID;
            // Выполняемое действие с объектом (изменить-1, создать-2, удалить-3)
            saveObject['_action'] = 1;
        
        ObjectInfo.Save(saveObject);
        
        // Перезагружаем слой для отображения изменений
        window.setTimeout(function(){
            AMLayers.reload(ObjectInfo._layer);
        }, 1500);
    });
    
    // Обрабатываем нажатие на кнопку сохраненния созданного объекта
    $(ObjectInfo.selectors['divUserPanel']).on('click', 'button.create-object-save', function(){
        var layerID = $(this).attr('layerid');
        
        var coordJson = PolygonDraw.geoJson();
        
        var geoJson = {"type":"MultiPolygon", "coordinates": [coordJson['coordinates']],};
        
        // Получаем все input и формируем JSON атрибутов для отправки.
        var attrJson = ObjectInfo.FormToJSON($('#object-info-form input'));
        
        // СОХРАНЯЕТСЯ ГЕОМЕТРИЯ И АТРИБУТЫ
        if ( !confirm('Вы уверены, что хотите сохранить объект?') )
            return;
        
        var saveObject = {};
            saveObject['attributes'] = attrJson;
            saveObject['token'] = _currentToken;
            saveObject['geometry'] = geoJson;
            saveObject['layerid'] = layerID;
            // Выполняемое действие с объектом (изменить-1, создать-2, удалить-3)
            saveObject['_action'] = 2;
        
        ObjectInfo.Save(saveObject);
        PolygonDraw._disableControl();
        // Перезагружаем слой для отображения изменений
        window.setTimeout(function(){
            AMLayers.reload(ObjectInfo._layer);
        }, 1500);
    });
    
    // Обрабатываем нажатие на кнопку удаляения объекта
    $(ObjectInfo.selectors['divUserPanel']).on('click', 'button.object-delete', function(){
        var fieldID = $(this).attr('fieldid');
        var layerID = $(this).attr('layerid');
        
        if ( !confirm('Вы уверены, что хотите удалить объект?') )
            return;
        
        var saveObject = {};
        saveObject['token'] = _currentToken;
        saveObject['fieldid'] = fieldID;
        saveObject['layerid'] = layerID;
        // Выполняемое действие с объектом (изменить-1, создать-2, удалить-3)
        saveObject['_action'] = 3;
        
        ObjectInfo.Save(saveObject);
        
        ObjectInfo._layer = layersStore[layerID];
        // Перезагружаем слой для отображения изменений
        window.setTimeout(function(){
            AMLayers.reload(ObjectInfo._layer);
        }, 1500);
    });
    
    // Обрабатываем нажатие на кнопку отмены создания объекта
    $(ObjectInfo.selectors['divUserPanel']).on('click', 'button.create-object-cancel', function(){
        console.log("Отмена создания объекта");
        PolygonDraw._disableControl();
        $('#user-panel #object-info-form').remove();

        $(ObjectInfo.selectors['divUserPanel']).slideUp(500);
        $(ObjectInfo.selectors['divUserPanel']).attr('class', 'up-show');
        $(ObjectInfo.selectors['divRollUserPanel']).css({"opacity":"0.5", "-webkit-opacity":"0.5", "-moz-opacity":"0.5", "-o-opacity":"0.5"});
        $(".zoom-control").removeAttr("style"); 
    });
    
});